# Notes

## How to have necessary user data for voice calls
When the gateway opens, the client receives a user cache in the READY payload. The user cache contains at least all the users the client user is friends with, has a private channel with, as well as users that are currently in a call in a guild.

The client will also receive a READY_SUPPLEMENTAL playload. This payload contains all the voice states of users in calls.

When the client receives a VOICE_STATE_UPDATE during runtime, this update will contain a `member` object **only** if the voice state update belongs to a guild. The member object contains a user object that contains the user's username and avatar. This is included because the user might not be in the user cache of the client yet. For private channel voice state updates, the username and avatar aren't required because they should already be in the user cache.

These are the ways that the client is informed about who is in what voice channel. When the client connects to a voice channel, it doesn't receive any information on who is in the channel. The only information it receives is a user ID and SSRC when a user speaks. If a user doesn't speak, there's no way to know that he is in a channel unless the READY_SUPPLEMENTAL and VOICE_STATE_UPDATE payloads are processed properly.

In order to start receiving audio, the client needs to create deocders for every user. The client won't know the SSRC of users until they speak, therefor a decoder can only be created once a user speaks. In order to know what SSRC belongs to what user, the speaking payloads receive from the voice gateway contain the user ID and SSRC.

## Private calls
Starting a private call is the same as starting a guild call except the guild_id parameter is null. The channel_id parameter is the channel ID of the private channel.

## Ringing
Ring and stop ringing by sending POST requests with the token in the Authorization header to these URLs:
- `https://discord.com/api/v9/channels/CHANNEL_ID/call/ring`
- `https://discord.com/api/v9/channels/CHANNEL_ID/call/stop-ringing`

The post data should be:
`{"recipients":null}` for ringing everybody and
`{"recipients":["USER_ID"]}` for ringing a specific user

## Undocumented gateway opcodes
CODE | NAME | SENT BY | DESCRIPTION
---|---|---|---
20 | Start watching stream | client | Communicates that the client wishes to watch a stream
19 | Stop watching stream | client | Communicates that the client stopped watching a stream

## Undocumented voice gateway opcodes
CODE | NAME | SENT BY | DESCRIPTION
---|---|---|---
12 | Video Streams | client and server | Communicate which streams are available.
14 | Codecs? | server | Communicate the preferred codecs
15 | Video Watching | client and server | Request start/stop transmission of a video stream.
16 | Version Probe | client and server | Requests and returns rtc_worker and voice version information from the voice gateway

## Transmitting video
In order to transmit video, when the client joins a voice channel, it needs to include an array of video streams in the identify payload (opcode 0) to the voice gateway. The array should be the `d.streams` parameter of the payload, and it is usually equal to this:
```json
[
  {
    "type": "video",
    "rid": "100",
    "quality": 100
  },
  {
    "type": "video",
    "rid": "50",
    "quality": 50
  }
]
```
When the voice gateway responds with a ready payload (opcode 2), this payload will also include the streams array. This array includes SSRCs for sending video data. It will look something like this:
```json
[
  {
    "type": "video",
    "ssrc": 189939,
    "rtx_ssrc": 189940,
    "rid": "50",
    "quality": 50,
    "active": false
  },
  {
    "type": "video",
    "ssrc": 189941,
    "rtx_ssrc": 189942,
    "rid": "100",
    "quality": 100,
    "active": false
  }
]
```
The client will then proceed by sending a protocol select payload (opcode 1). This payload should include a `d.codecs` attribute which is an array of objects that describe the codecs that the client is capable of transmitting. An example:
```json
[
  {
    "name": "opus",
    "type": "audio",
    "priority": 1000,
    "payload_type": 120
  },
  {
    "name": "H264",
    "type": "video",
    "priority": 1000,
    "payload_type": 101,
    "rtx_payload_type": 102
  },
  {
    "name": "VP8",
    "type": "video",
    "priority": 2000,
    "payload_type": 103,
    "rtx_payload_type": 104
  },
  {
    "name": "VP9",
    "type": "video",
    "priority": 3000,
    "payload_type": 105,
    "rtx_payload_type": 106
  },
  {
    "name": "AV1X",
    "type": "video",
    "priority": 4000,
    "payload_type": 107,
    "rtx_payload_type": 108
  }
]
```
When the client starts a video stream, it announces so using a video streams payload (opcode 12). The `d` object of the payload looks something like this:
```json
{
  "audio_ssrc": 262821,
  "video_ssrc": 262822,
  "rtx_ssrc": 262823,
  "streams": [
    {
      "type": "video",
      "rid": "50",
      "ssrc": 262822,
      "active": true,
      "quality": 50,
      "rtx_ssrc": 262823,
      "max_bitrate": 2500000,
      "max_framerate": 20,
      "max_resolution": {
        "type": "fixed",
        "width": 1280,
        "height": 720
      }
    },
    {
      "type": "video",
      "rid": "100",
      "ssrc": 262824,
      "active": true,
      "quality": 100,
      "rtx_ssrc": 262825,
      "max_bitrate": 2500000,
      "max_framerate": 20,
      "max_resolution": {
        "type": "fixed",
        "width": 1280,
        "height": 720
      }
    }
  ]
}
```
The client doesn't start transmitting the video stream until a user requests it using a video watching payload (opcode 15). This `d` object includes SSRCs as keys for streams that should be sent:
```json
{
  "262824": 0,
  "any": 100
}
```

## Watching streams
The client is informed about streams through voice state updates with `self_stream` set to `true`.

When the client wants to watch a stream it transmits an opcode 20 message over the gateway. The message's `d` object contains a stream key with the format `guild:GUILD_ID:CHANNEL_ID:USER_ID`:
```json
{
  "stream_key": "guild:543552981082046490:543552981992341506:228225091018686475"
}
```
Stopping watching a stream is similar
```json
{
  "stream_key": "guild:543552981082046490:543552981992341506:228225091018686475"
}
```
When the opcode 20 message is transmitted, the gateway will respond with a STREAM_CREATE dispatch with data like this:
```json
{
  "viewer_ids": [
    "544605157204164628"
  ],
  "stream_key": "guild:543552981082046490:543552981992341506:228225091018686475",
  "rtc_server_id": "990355390321557574",
  "region": "rotterdam",
  "paused": false
}
```
It will follow up with a STREAM_SERVER_UPDATE dispatch:
```json
{
  "token": "9e0634d8c5f106c9",
  "stream_key": "guild:543552981082046490:543552981992341506:228225091018686475",
  "guild_id": null,
  "endpoint": "rotterdam10058.discord.media:443"
}
```
This info is then used to open a WebSocket connection similar to the voice gateway. The identify payload (opcode 0) however, has some additional data attributes, and the server_id is the rtc_server_id from the STREAM_CREATE dispatch:
```json
{
  "server_id": "990355390321557574",
  "user_id": "544605157204164628",
  "session_id": "b3686bac019b72d300499e8d547f9880",
  "token": "9e0634d8c5f106c9",
  "video": true,
  "streams": [
    {
      "type": "screen",
      "rid": "100",
      "quality": 100
    }
  ]
}
```
The opcode 2 ready payload from the socket will look something like this:
```json
{
  "streams": [
    {
      "type": "video",
      "ssrc": 168809,
      "rtx_ssrc": 168810,
      "rid": "100",
      "quality": 100,
      "active": false
    }
  ],
  "ssrc": 168808,
  "port": 50010,
  "modes": [
    "aead_aes256_gcm_rtpsize",
    "aead_aes256_gcm",
    "xsalsa20_poly1305_lite_rtpsize",
    "xsalsa20_poly1305_lite",
    "xsalsa20_poly1305_suffix",
    "xsalsa20_poly1305"
  ],
  "ip": "35.214.250.200",
  "experiments": [
    "fixed_keyframe_interval"
  ]
}
```
The socket will also send an opcode 12 packet:
```json
{
  "video_ssrc": 168251,
  "user_id": "228225091018686475",
  "streams": [
    {
      "ssrc": 168251,
      "rtx_ssrc": 168252,
      "rid": "100",
      "quality": 100,
      "max_resolution": {
        "width": 1280,
        "type": "fixed",
        "height": 720
      },
      "max_framerate": 30,
      "active": true
    }
  ],
  "audio_ssrc": 168250
}
```

## Authentication

### Login

Endpoint:

```
https://discord.com/api/v9/auth/login
```

Method: `POST`

Headers:
- `User-Agent`
- `Content-Type: application/json`
- `Referer: https://discord.com/login`

Payload:

```json
{
  "login": "EMAIL",
  "password": "PASSWORD",
  "undelete": false,
  "captcha_key": null,
  "login_source": null,
  "gift_code_sku_id": null
}
```

### TOTP (2FA)

Endpoint:

```
https://discord.com/api/v9/auth/mfa/totp
```

Method: `POST`

Headers:
- `User-Agent`
- `Content-Type: application/json`
- `Referer: https://discord.com/login`

Payload:

```json
{
  "code": "CODE",
  "ticket": "TICKET",
  "login_source": null,
  "gift_code_sku_id": null
}
```

### Log out

Endpoint:

```
https://discord.com/api/v9/auth/logout
```

Method: `POST`

Headers:
- `User-Agent`
- `Content-Type: application/json`
- `Referer: https://discord.com/channels/@me`
- `Authorization: TOKEN`

Payload:

```json
{
  "provider": null,
  "voip_provider" :null
}
```

Has no response.

### Responses

Successful login:

```json
{
  "token": "TOKEN",
  "user_settings": {
    "locale": "en-US",
    "theme": "dark"
  },
  "user_id": "USER_ID"
}
```

Captcha required:

```json
{
  "captcha_key": [ "captcha-required" ],
  "captcha_sitekey": "f5561ba9-8f1e-40ca-9b5b-a0b3f719ef34",
  "captcha_service": "hcaptcha"
}
```

TOTP required:

```json
{
  "token": null,
  "mfa": true,
  "sms": false,
  "ticket": "TICKET"
}
```

Invalid login:

```json
{
  "code": 50035,
  "errors": {
    "login": {
      "_errors": [
        {
          "code": "INVALID_LOGIN",
          "message": "Login or password is invalid."
        }
      ]
    },
    "password": {
      "_errors": [
        {
          "code": "INVALID_LOGIN",
          "message": "Login or password is invalid."
        }
      ]
    }
  },
  "message": "Invalid Form Body"
}
```

Invalid TOTP code:

```json
{
  "message": "Invalid two-factor code",
  "code": 60008
}
```
