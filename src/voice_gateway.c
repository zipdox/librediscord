#include <json-glib/json-glib.h>
#include <string.h>
#include <stdlib.h>

#include "voice_gateway.h"

static gboolean VoiceGateway_heartbeat(gpointer user_data){
    VoiceGateway *voice_gateway = user_data;

    JsonObject *msg_obj = json_node_get_object(voice_gateway->heartbeat_node);
    json_object_set_int_member(msg_obj, "d", g_random_int());

    char *heartbeat_json = json_to_string(voice_gateway->heartbeat_node, FALSE);
    soup_websocket_connection_send_text(voice_gateway->connection, heartbeat_json);
    g_free(heartbeat_json);

    return TRUE;
}

static void VoiceGateway_hello(JsonObject *data_obj, VoiceGateway *voice_gateway){
    voice_gateway->heartbeat_interval = json_object_get_int_member(data_obj, "heartbeat_interval");
    voice_gateway->heartbeat_source = g_timeout_source_new(voice_gateway->heartbeat_interval);
    g_source_set_callback(voice_gateway->heartbeat_source, G_SOURCE_FUNC(VoiceGateway_heartbeat), (gpointer) voice_gateway, NULL);
    g_source_attach(voice_gateway->heartbeat_source, NULL);

    JsonNode *identify_node = json_from_string(VOICE_IDENTITY_TEMPLATE, NULL);
    JsonObject *identify_obj = json_node_get_object(identify_node);
    JsonObject *identify_data_obj = json_object_get_object_member(identify_obj, "d");

    json_object_set_string_member(identify_data_obj, "server_id", voice_gateway->server_id);
    json_object_set_string_member(identify_data_obj, "user_id", voice_gateway->user_id);
    json_object_set_string_member(identify_data_obj, "session_id", voice_gateway->session_id);
    json_object_set_string_member(identify_data_obj, "token", voice_gateway->token);

    char *identity_json = json_to_string(identify_node, FALSE);
    soup_websocket_connection_send_text(voice_gateway->connection, identity_json);


    g_free(identity_json);
    json_node_free(identify_node);
}

static void VoiceGateway_message_callback(SoupWebsocketConnection *self, int type, GBytes *message, gpointer user_data){
    VoiceGateway *voice_gateway = user_data;
    
    gsize size = g_bytes_get_size(message);
    const char *data = g_bytes_get_data(message, &size);

    GError *error;
    JsonNode *root = json_from_string(data, &error);
    if(voice_gateway->connection == NULL){
        g_print("Voice gateway message error: %s\n", error->message);
        g_error_free(error);
        return;
    }
    
    JsonObject *root_obj = json_node_get_object(root);

    gint64 opcode = json_object_get_int_member(root_obj, "op");
    if(opcode == 6) return json_node_free(root);

    JsonObject *data_obj = json_object_get_object_member(root_obj, "d");

    if(opcode == 8) VoiceGateway_hello(data_obj, voice_gateway);

    voice_gateway->message_cb(opcode, data_obj, voice_gateway->cb_data);

    json_node_free(root);
}

static void VoiceGateway_error_callback(SoupWebsocketConnection *self, GError *error, gpointer user_data){
    g_print("Voice gateway error: %s\n", error->message);
}

static void VoiceGateway_closed_callback(SoupWebsocketConnection *self, gpointer user_data){
    VoiceGateway *voice_gateway = user_data;
    if(voice_gateway->heartbeat_source) if(!g_source_is_destroyed(voice_gateway->heartbeat_source))
        g_source_destroy(voice_gateway->heartbeat_source);
    voice_gateway->closed_cb(voice_gateway->cb_data);
}

static void VoiceGateway_connected_cb(GObject *source_object, GAsyncResult *res, gpointer user_data){
    VoiceGateway *voice_gateway = user_data;

    GError *error;
    voice_gateway->connection = soup_session_websocket_connect_finish(voice_gateway->session, res, &error);
    if(voice_gateway->connection == NULL){
        g_print("Error connecting to voice gateway: %s\n", error->message);
        g_error_free(error);
        voice_gateway->closed_cb(voice_gateway->cb_data);
    }else{
        g_signal_connect(voice_gateway->connection, "message", G_CALLBACK(VoiceGateway_message_callback), (gpointer) voice_gateway);
        g_signal_connect(voice_gateway->connection, "error", G_CALLBACK(VoiceGateway_error_callback), (gpointer) voice_gateway);
        g_signal_connect(voice_gateway->connection, "closed", G_CALLBACK(VoiceGateway_closed_callback), (gpointer) voice_gateway);
    }
}

VoiceGateway *VoiceGateway_create(
    const char *endpoint, const char *server_id, const char *user_id, const char *session_id, const char *token,
    VoiceGateway_message_cb message_cb, VoiceGateway_closed_cb closed_cb, gpointer cb_data
){
    VoiceGateway *voice_gateway = g_new0(VoiceGateway, 1);
    voice_gateway->server_id = g_strdup(server_id);
    voice_gateway->user_id = g_strdup(user_id);
    voice_gateway->session_id = g_strdup(session_id);
    voice_gateway->token = g_strdup(token);
    voice_gateway->cb_data = cb_data;
    voice_gateway->voice_receivers = g_ptr_array_new();
    voice_gateway->message_cb = message_cb;
    voice_gateway->closed_cb = closed_cb;
    voice_gateway->heartbeat_node = json_from_string(VOICE_HEARTBEAT_TEMPLATE, NULL);

    char *ws_uri = g_strdup_printf(VOICE_GATEWAY_TEMPLATE, endpoint);
    voice_gateway->request = soup_message_new("GET", ws_uri);
    g_free(ws_uri);

    voice_gateway->session = soup_session_new();
    g_object_set(G_OBJECT(voice_gateway->session), "user-agent", USER_AGENT, NULL); 
    soup_session_websocket_connect_async(voice_gateway->session, voice_gateway->request, GATEWAY_ORIGIN, NULL, NULL, VoiceGateway_connected_cb, (gpointer) voice_gateway);

    return voice_gateway;
}

void VoiceGateway_send(VoiceGateway *voice_gateway, JsonNode *node){
    if(!voice_gateway->connection) return;
    if(soup_websocket_connection_get_state(voice_gateway->connection) != SOUP_WEBSOCKET_STATE_OPEN) return;

    char *json_text = json_to_string(node, FALSE);
    soup_websocket_connection_send_text(voice_gateway->connection, json_text);
    g_free(json_text);
}

void VoiceGateway_close(VoiceGateway *voice_gateway){
    if(voice_gateway->heartbeat_source) if(!g_source_is_destroyed(voice_gateway->heartbeat_source))
        g_source_destroy(voice_gateway->heartbeat_source);
    if(voice_gateway->connection) if(soup_websocket_connection_get_state(voice_gateway->connection) == SOUP_WEBSOCKET_STATE_OPEN)
        soup_websocket_connection_close(voice_gateway->connection, SOUP_WEBSOCKET_CLOSE_NORMAL, NULL);
}

void VoiceGateway_destroy(VoiceGateway *voice_gateway){
    g_free((void*) voice_gateway->server_id);
    g_free((void*) voice_gateway->user_id);
    g_free((void*) voice_gateway->session_id);
    g_free((void*) voice_gateway->token);
    g_object_unref(voice_gateway->session);
    g_object_unref(voice_gateway->request);
    g_object_unref(voice_gateway->connection);
    g_source_unref(voice_gateway->heartbeat_source);
    json_node_free(voice_gateway->heartbeat_node);
    g_ptr_array_free(voice_gateway->voice_receivers, TRUE);
}
