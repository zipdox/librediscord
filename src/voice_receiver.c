#include <math.h>

#include "voice_receiver.h"
#include "opus.h"

VoiceReceiver* VoiceReceiver_create(GMutex *mutex, VoiceReceiver_speaking_cb speaking_cb, gpointer cb_data){
    VoiceReceiver* voice_receiver = g_new0(VoiceReceiver, 1);
    voice_receiver->mutex = mutex;
    voice_receiver->speaking_cb = speaking_cb;
    voice_receiver->cb_data = cb_data;
    return voice_receiver;
}

static inline VoiceReceiverVoice *find_voice_by_user_id(VoiceReceiver *voice_receiver, guint64 user_id){
    VoiceReceiverVoice  *comp_voice;
    GList *voice = voice_receiver->voices;
    while(voice){
        comp_voice = voice->data;
        if(comp_voice->user_id == user_id) return comp_voice;
        voice = voice->next;
    }
    return NULL;
}

static inline VoiceReceiverVoice *find_voice_by_ssrc(VoiceReceiver *voice_receiver, guint32 ssrc){
    VoiceReceiverVoice  *comp_voice;
    GList *voice = voice_receiver->voices;
    while(voice){
        comp_voice = voice->data;
        if(comp_voice->ssrc == ssrc) return comp_voice;
        voice = voice->next;
    }
    return NULL;
}

// Checks if a voice exists and adds it if it doesn't
void VoiceReceiver_voice_speaking(VoiceReceiver *voice_receiver, guint32 ssrc, guint64 user_id){
    VoiceReceiverVoice *voice = find_voice_by_user_id(voice_receiver, user_id);

    voice = g_new0(VoiceReceiverVoice, 1);
    voice->ssrc = ssrc;
    voice->user_id = user_id;
    int opus_error;
    voice->decoder = opus_decoder_create(48000, 2, &opus_error);
    if(opus_error != OPUS_OK){
        g_print("Failed to create Opus decoder.\n");
        opus_decoder_destroy(voice->decoder);
        g_free(voice);
        return;
    }

    voice->buffer = Ringbuf_create(2, 48000*2);
    voice->volume = 1.0;
    g_mutex_lock(voice_receiver->mutex);
    voice_receiver->voices = g_list_append(voice_receiver->voices, (gpointer) voice);
    g_mutex_unlock(voice_receiver->mutex);
}

void VoiceReceiver_voice_remove(VoiceReceiver *voice_receiver, guint64 user_id){
    VoiceReceiverVoice *voice = find_voice_by_user_id(voice_receiver, user_id);
    g_mutex_lock(voice_receiver->mutex);
    voice_receiver->voices = g_list_remove(voice_receiver->voices, (gpointer) voice);
    g_mutex_unlock(voice_receiver->mutex);
    if(!voice) return;
    opus_decoder_destroy(voice->decoder);
    Ringbuf_destroy(voice->buffer);
    g_free(voice);
}

void VoiceReceiver_set_volume(VoiceReceiver *voice_receiver, guint64 user_id, float set_volume){
    VoiceReceiverVoice *voice = find_voice_by_user_id(voice_receiver, user_id);
    if(voice) voice->volume = set_volume;
}

#define amp2dB(A) (20.0f * log10f(A))
static inline void calc_amplitude(float *data, int samples, float volume[2]){
    float sum_l = .0f, sum_r = .0f;
    for(int i = 0; i < samples; i++){
        sum_l += data[i * 2] * data[i * 2];
        sum_r += data[i * 2 + 1] * data[i * 2 + 1];
    }
    float rms_l = sqrtf(sum_l / samples);
    float rms_r = sqrtf(sum_r / samples);
    // Prevent log of 0
    if(rms_l <= .0f) sum_l = FLT_EPSILON;
    if(rms_r <= .0f) sum_r = FLT_EPSILON;
    volume[0] = amp2dB(rms_l);
    volume[1] = amp2dB(rms_r);
}

void VoiceReceiver_decode(VoiceReceiver *voice_receiver, guint32 ssrc, unsigned char *data, gint32 len){
    VoiceReceiverVoice *voice = find_voice_by_ssrc(voice_receiver, ssrc);
    if(!voice) return;
    int samples = opus_decode_float(voice->decoder, data, len, voice_receiver->decoded_samples, DECODING_FRAME_SIZE, 0);
    if(samples <= 0) return;
    
    float volume[2];
    calc_amplitude(voice_receiver->decoded_samples, samples, volume);
    voice_receiver->speaking_cb(voice->user_id, volume, voice_receiver->cb_data);

    g_mutex_lock(voice_receiver->mutex);
    Ringbuf_write(voice->buffer, voice_receiver->decoded_samples, samples);
    g_mutex_unlock(voice_receiver->mutex);
}

void VoiceReceiver_read_samples(VoiceReceiver *voice_receiver, float *data, int samples, float volume){
    VoiceReceiverVoice *voice_rec;
    GList *voice = voice_receiver->voices;
    while(voice){
        voice_rec = voice->data;
        Ringbuf_read(voice_rec->buffer, data, samples, voice_rec->volume * volume);
        voice = voice->next;
    }
}

static void voice_free(gpointer data){
    VoiceReceiverVoice *voice = data;
    opus_decoder_destroy(voice->decoder);
    Ringbuf_destroy(voice->buffer);
    g_free(data);
}

static void silence_all_receivers(VoiceReceiver *receiver){
    GList *rec_voice = receiver->voices;
    while(rec_voice){
        VoiceReceiverVoice *voice = rec_voice->data;
        float volume[2] = {-INFINITY, -INFINITY};
        receiver->speaking_cb(voice->user_id, volume, receiver->cb_data);
        rec_voice = rec_voice->next;
    }
}

void VoiceReceiver_destroy(VoiceReceiver *voice_receiver){
    silence_all_receivers(voice_receiver);
    g_list_free_full(voice_receiver->voices, voice_free);
    g_free(voice_receiver);
}
