#include <leveldb/c.h>

#include "token.h"
#include "constants.h"


static void copy_recursive(GFile *src, GFile *dest, GFileCopyFlags flags, GCancellable *cancellable, GError **error){
    if(*error) return;
    GFileType src_type = g_file_query_file_type(src, G_FILE_QUERY_INFO_NONE, cancellable);
    if(src_type == G_FILE_TYPE_DIRECTORY){
        g_file_make_directory(dest, cancellable, error);
        g_file_copy_attributes(src, dest, flags, cancellable, error);

        GFileEnumerator *enumerator = g_file_enumerate_children(src, G_FILE_ATTRIBUTE_STANDARD_NAME, G_FILE_QUERY_INFO_NONE, cancellable, error);
        for (GFileInfo *info = g_file_enumerator_next_file(enumerator, cancellable, error); info != NULL; info = g_file_enumerator_next_file(enumerator, cancellable, error)) {
            const char *relative_path = g_file_info_get_name(info);
            copy_recursive(
                g_file_resolve_relative_path(src, relative_path),
                g_file_resolve_relative_path(dest, relative_path),
                flags, cancellable, error);
        }
    }else if(src_type == G_FILE_TYPE_REGULAR){
        g_file_copy(src, dest, flags, cancellable, NULL, NULL, error);
    }
}

static void delete_recursive(GFile *file, GCancellable *cancellable, GError **error){
    if(*error) return;
    GFileType file_type = g_file_query_file_type(file, G_FILE_QUERY_INFO_NONE, cancellable);
    if(file_type == G_FILE_TYPE_DIRECTORY){
        GFileEnumerator *enumerator = g_file_enumerate_children(file, G_FILE_ATTRIBUTE_STANDARD_NAME, G_FILE_QUERY_INFO_NONE, cancellable, error);
        for (GFileInfo *info = g_file_enumerator_next_file(enumerator, cancellable, error); info != NULL; info = g_file_enumerator_next_file(enumerator, cancellable, error)) {
            delete_recursive(
                g_file_resolve_relative_path(file, g_file_info_get_name(info)),
                cancellable, error);
        }
        g_file_delete(file, cancellable, error);
    }else if(file_type == G_FILE_TYPE_REGULAR){
        g_file_delete(file, cancellable, error);
    }
}

static void find_token_thread(GTask *task, gpointer source_object, gpointer task_data, GCancellable *cancellable){
    char *token = NULL;

    const char *config_dir = g_get_user_config_dir();
    GFile *discord_dir = g_file_new_build_filename(config_dir, "discord", "Local Storage", "leveldb", NULL);
    if(!g_file_query_exists(discord_dir, NULL)){
        g_object_unref(discord_dir);
        return g_task_return_pointer(task, token, g_object_unref);
    }

    const char *tmpdir = g_get_tmp_dir();
    const char *temp_copy_dir = g_build_filename(tmpdir, "discordfindtoken", NULL);
    GFile *temp_copy = g_file_new_for_path(temp_copy_dir);

    GError *error = NULL;
    copy_recursive(discord_dir, temp_copy, G_FILE_COPY_OVERWRITE, NULL, &error);
    g_object_unref(discord_dir);
    if(error){
        g_print("Error finding token: %s\n", error->message);
        g_error_free(error);
        g_object_unref(temp_copy);
        g_free((gpointer) temp_copy_dir);
        return g_task_return_pointer(task, token, g_object_unref);
    }


    leveldb_options_t *options = leveldb_options_create();
    leveldb_options_set_create_if_missing(options, 1);

    char *err = NULL;
    leveldb_t *db = leveldb_open(options, temp_copy_dir, &err);
    leveldb_readoptions_t *roptions = leveldb_readoptions_create();

    if(err != NULL){
        goto cleanup_and_return;
    }

    size_t token_len;
    char *temp_token = leveldb_get(db, roptions, TOKEN_KEY, TOKEN_KEY_LENGTH, &token_len, &err);

    if(err || temp_token == NULL){
        goto cleanup_and_return;
    }

    token = calloc(1, token_len - 2);

    // there are two characters before the token and one after it, so this adjusts the length to account for that
    token_len -= 3;

    // token + 2 because there are two characters before the token
    memcpy(token, temp_token + 2, token_len);

    cleanup_and_return:
    if (err)
        leveldb_free(err);
    if (token)
        leveldb_free(temp_token);

    leveldb_options_destroy(options);
    leveldb_readoptions_destroy(roptions);
    leveldb_close(db);

    delete_recursive(temp_copy, NULL, &error);
    if(error){
        g_print("Error deleting token DB copy: %s\n", error->message);
        g_error_free(error);
    }

    g_free((gpointer) temp_copy_dir);
    g_object_unref(temp_copy);

    g_task_return_pointer(task, token, g_object_unref);
}

void find_token_async(GAsyncReadyCallback callback, gpointer user_data){
    GTask *task = g_task_new(NULL, NULL, callback, user_data);
    g_task_run_in_thread(task, find_token_thread);
    g_object_unref(task);
}

char *find_token_finish(GAsyncResult *result, GError **error){
    g_return_val_if_fail(g_task_is_valid(result, NULL), NULL);
    return g_task_propagate_pointer(G_TASK(result), error);
}
