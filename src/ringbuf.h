#define RINGBUF_SAMPLES 96000

typedef struct {
    int write_pos;
    int read_pos;
    float buffer[2*RINGBUF_SAMPLES];
} Ringbuf;

Ringbuf *Ringbuf_create();

void Ringbuf_destroy(Ringbuf *buffer);

void Ringbuf_write(Ringbuf *buffer, float *data, int samples);

void Ringbuf_read(Ringbuf *buffer, float *data, int samples, float volume);
