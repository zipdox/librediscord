#include "state_manager.h"
#include "constants.h"

StateManager *StateManager_create(StateManager_mute_icon mute_icon, StateManager_deafen_icon deafen_icon, gpointer cb_data){
    StateManager *manager = g_new0(StateManager, 1);
    manager->guilds = g_ptr_array_new();
    manager->private_channels = g_ptr_array_new();
    manager->user_cache = g_ptr_array_new();
    manager->private_voice_states = g_ptr_array_new();
    manager->mute_icon = mute_icon;
    manager->deafen_icon = deafen_icon;
    manager->cb_data = cb_data;
    manager->vu_pattern = cairo_pattern_create_linear(0.0, 0.0, 180.0, 0.0);
    cairo_pattern_add_color_stop_rgb(manager->vu_pattern, 0.0, 0.0, 0.5, 0.0);
    cairo_pattern_add_color_stop_rgb(manager->vu_pattern, 0.6, 0.0, 0.5, 0.0);
    cairo_pattern_add_color_stop_rgb(manager->vu_pattern, 0.8, 0.5, 0.5, 0.0);
    cairo_pattern_add_color_stop_rgb(manager->vu_pattern, 1.0, 0.5, 0.0, 0.0);
    return manager;
}

static DiscordUser *StateManager_get_user(StateManager *manager, const char *user_id){
    DiscordUser *user;
    guint len = manager->user_cache->len;
    for(guint i = 0; i < len; i++){
        user = g_ptr_array_index(manager->user_cache, i);
        if(g_strcmp0(user_id, user->id) == 0) return user;
    }
    return NULL;
}

DiscordGuild *StateManager_get_guild(StateManager *manager, const char *id){
    DiscordGuild *guild;
    guint len = manager->guilds->len;
    for(guint i = 0; i < len; i++){
        guild = g_ptr_array_index(manager->guilds, i);
        if(g_strcmp0(id, guild->id) == 0) return guild;
    }
    return NULL;
}

static DiscordGuild *StateManager_get_guild_int(StateManager *manager, guint64 id){
    DiscordGuild *guild;
    guint len = manager->guilds->len;
    for(guint i = 0; i < len; i++){
        guild = g_ptr_array_index(manager->guilds, i);
        if(id == guild->id_int) return guild;
    }
    return NULL;
}

DiscordUser *StateManager_add_user(StateManager *manager, const char *username, const char *discriminator, const char *id, const char *avatar){
    DiscordUser *user = StateManager_get_user(manager, id);
    if(user) return user;
    user = g_new0(DiscordUser, 1);
    user->username = g_strdup(username);
    user->discriminator = g_strdup(discriminator);
    user->id = g_strdup(id);
    user->id_int = g_ascii_strtoull(id, NULL, 10);
    user->avatar = g_strdup(avatar);
    g_ptr_array_add(manager->user_cache, user);
    return user;
}

DiscordGuild *StateManager_add_guild(StateManager *manager, const char *name, const char *id, const char *icon){
    DiscordGuild *guild = g_new0(DiscordGuild, 1);
    guild->name = g_strdup(name);
    guild->id = g_strdup(id);
    guild->id_int = g_ascii_strtoull(id, NULL, 10);
    guild->icon = g_strdup(icon);
    guild->channels = g_ptr_array_new();
    guild->voice_states = g_ptr_array_new();
    g_ptr_array_add(manager->guilds, guild);
    return guild;
}

DiscordGuildVoiceChannel *StateManager_guild_add_channel(StateManager *manager, const char *guild_id, const char *name, const char *id, gboolean can_join){
    DiscordGuild *guild = StateManager_get_guild(manager, guild_id);
    if(!guild) return NULL;
    DiscordGuildVoiceChannel *channel = g_new0(DiscordGuildVoiceChannel, 1);
    channel->name = g_strdup(name);
    channel->id = g_strdup(id);
    channel->id_int = g_ascii_strtoull(id, NULL, 10);
    channel->can_join = can_join;
    channel->guild = guild;
    g_ptr_array_add(guild->channels, channel);
    return channel;
}

DiscordGuildVoiceChannel *StateManager_guild_get_channel(DiscordGuild *guild, const char *channel_id){
    DiscordGuildVoiceChannel *channel;
    guint len = guild->channels->len;
    for(guint i = 0; i < len; i++){
        channel = g_ptr_array_index(guild->channels, i);
        if(g_strcmp0(channel->id, channel_id) == 0) return channel;
    }
    return NULL;
}

DiscordPrivateChannel *StateManager_add_private_channel(StateManager *manager, const char *name, const char *id, char type, const char *icon){
    DiscordPrivateChannel *channel = g_new0(DiscordPrivateChannel, 1);
    channel->name = g_strdup(name);
    channel->id = g_strdup(id);
    channel->id_int = g_ascii_strtoull(id, NULL, 10);
    channel->type = type;
    channel->icon = g_strdup(icon);
    g_ptr_array_add(manager->private_channels, channel);
    return channel;
}

DiscordPrivateChannel *StateManager_get_private_channel(StateManager *manager, const char *id){
    DiscordPrivateChannel *channel;
    guint len = manager->private_channels->len;
    for(guint i = 0; i < len; i++){
        channel = g_ptr_array_index(manager->private_channels, i);
        if(g_strcmp0(id, channel->id) == 0) return channel;
    }
    return NULL;
}

static inline DiscordGuildVoiceState *StateManager_get_guild_voice_state(DiscordGuild *guild, const char *user_id){
    guint voice_states_len = guild->voice_states->len;
    DiscordGuildVoiceState *state;
    for(guint i = 0; i < voice_states_len; i++){
        state = g_ptr_array_index(guild->voice_states, i);
        if(g_strcmp0(state->user->id, user_id) == 0) return state;
    }
    return NULL;
}

static inline DiscordGuildVoiceState *StateManager_get_guild_voice_state_int(DiscordGuild *guild, guint64 user_id){
    guint voice_states_len = guild->voice_states->len;
    DiscordGuildVoiceState *state;
    for(guint i = 0; i < voice_states_len; i++){
        state = g_ptr_array_index(guild->voice_states, i);
        if(state->user->id_int == user_id) return state;
    }
    return NULL;
}

static inline void StateManager_delete_guild_voice_state(GPtrArray *voice_state_arr, DiscordGuildVoiceState *state){
    gtk_widget_destroy(gtk_widget_get_parent(state->row));
    g_free(state);
    g_ptr_array_remove(voice_state_arr, state);
}

static inline DiscordPrivateVoiceState *StateManager_get_private_voice_state(GPtrArray *states, const char *user_id){
    guint voice_states_len = states->len;
    DiscordPrivateVoiceState *state;
    for(guint i = 0; i < voice_states_len; i++){
        state = g_ptr_array_index(states, i);
        if(g_strcmp0(state->user->id, user_id) == 0) return state;
    }
    return NULL;
}

static inline DiscordPrivateVoiceState *StateManager_get_private_voice_state_int(GPtrArray *states, guint64 user_id){
    guint voice_states_len = states->len;
    DiscordPrivateVoiceState *state;
    for(guint i = 0; i < voice_states_len; i++){
        state = g_ptr_array_index(states, i);
        if(state->user->id_int ==  user_id) return state;
    }
    return NULL;
}

static inline void StateManager_delete_private_voice_state(GPtrArray *voice_state_arr, DiscordPrivateVoiceState *state){
    gtk_widget_destroy(gtk_widget_get_parent(state->row));
    g_free(state);
    g_ptr_array_remove(voice_state_arr, state);
}

static void StateManager_update_guild_voice_state_imgs(StateManager *manager, DiscordGuildVoiceState *state){
    manager->mute_icon(GTK_IMAGE(state->mute_img), (state->self_mute || state->mute), manager->cb_data);
    manager->deafen_icon(GTK_IMAGE(state->deafen_img), (state->self_deaf || state->deaf), manager->cb_data);
}

static void StateManager_update_private_voice_state_imgs(StateManager *manager, DiscordPrivateVoiceState *state){
    manager->mute_icon(GTK_IMAGE(state->mute_img), (state->self_mute || state->mute), manager->cb_data);
    manager->deafen_icon(GTK_IMAGE(state->deafen_img), (state->self_deaf || state->deaf), manager->cb_data);
}

static void volume_adjusted(GtkAdjustment *adjustment, gpointer user_data){
    DiscordGuildVoiceState *state = user_data;
    state->volume_adjust(state->user->id, gtk_adjustment_get_value(state->volume_adjustment), state->data);
}

static void vu_draw(GtkWidget *widget, cairo_t *cr, cairo_pattern_t *pattern, double l, double r){
    guint width = gtk_widget_get_allocated_width(widget);
    guint height = gtk_widget_get_allocated_height(widget);
    double bar_height = height * 0.4;

    cairo_set_source(cr, pattern);
    cairo_rectangle(cr, 0.0, 0.0, width * l, bar_height);
    cairo_rectangle(cr, 0.0, height - bar_height, width * r, bar_height);
    cairo_fill(cr);
}

static gboolean vu_draw_guild(GtkWidget *widget, cairo_t *cr, gpointer user_data){
    DiscordGuildVoiceState *state = user_data;
    vu_draw(widget, cr, state->vu_pattern, state->vu_vol[0], state->vu_vol[1]);
    return TRUE;
}

static gboolean vu_draw_private(GtkWidget *widget, cairo_t *cr, gpointer user_data){
    DiscordPrivateVoiceState *state = user_data;
    vu_draw(widget, cr, state->vu_pattern, state->vu_vol[0], state->vu_vol[1]);
    return TRUE;
}

static gboolean show_ctx_menu(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    if(gdk_event_get_event_type(event) != GDK_BUTTON_PRESS) return FALSE;
    if(((GdkEventButton*)event)->button != 3) return FALSE;
    gtk_widget_show_all(GTK_WIDGET(user_data));
    gtk_popover_popup(GTK_POPOVER(user_data));
    return TRUE;
}

void StateManager_user_speaking(StateManager *manager, gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2]){
    if(private_channel){
        DiscordPrivateVoiceState *state =  StateManager_get_private_voice_state_int(manager->private_voice_states, user_id);
        if(!state) return;
        state->vu_vol[0] = (volume[0] + 60.0f) / 60.0;
        state->vu_vol[1] = (volume[1] + 60.0f) / 60.0;
        gtk_widget_queue_draw(state->vu_meter);
    }else{
        DiscordGuild *guild = StateManager_get_guild_int(manager, server_id);
        if(!guild) return;
        DiscordGuildVoiceState *state = StateManager_get_guild_voice_state_int(guild, user_id);
        if(!state) return;
        state->vu_vol[0] = (volume[0] + 60.0f) / 60.0;
        state->vu_vol[1] = (volume[1] + 60.0f) / 60.0;
        gtk_widget_queue_draw(state->vu_meter);
    }
}

void StateManager_voice_state_update(
    StateManager *manager, ImgCache *cache,
    const char *username, const char *discriminator, const char *avatar,
    const char *user_id, const char *session_id, const char *guild_id, const char *channel_id,
    gboolean self_video, gboolean self_mute, gboolean self_deaf, gboolean mute, gboolean deaf,
    gboolean is_self,
    StateManager_volume_adjust_cb volume_adjust, gpointer cb_data
){
    DiscordUser *user;
    DiscordGuild *guild;
    DiscordGuildVoiceState *state;
    DiscordPrivateVoiceState *private_state;
    DiscordGuildVoiceChannel *channel;
    DiscordPrivateChannel *private_channel;
    GtkWidget *username_label, *parent;

    if(guild_id){
        guild = StateManager_get_guild(manager, guild_id);
        if(!guild) return;

        state = StateManager_get_guild_voice_state(guild, user_id);
        if(state){ // user is in channel
            if(channel_id){ // user moving channel and/or changing state
                state->self_mute = self_mute;
                state->self_deaf = self_deaf;
                state->self_video = self_video;
                state->mute = mute;
                state->deaf = deaf;
                StateManager_update_guild_voice_state_imgs(manager, state);
                if(g_strcmp0(state->channel->id, channel_id) != 0){ // user changing channel
                    channel = StateManager_guild_get_channel(guild, channel_id);
                    if(!channel) return;
                    state->channel = channel;
                    parent = gtk_widget_get_parent(state->row);
                    g_object_ref(parent);
                    gtk_container_remove(GTK_CONTAINER(gtk_widget_get_parent(parent)), parent);
                    gtk_container_add(GTK_CONTAINER(channel->user_list), parent);
                }
            }else // user leaving channel
                StateManager_delete_guild_voice_state(guild->voice_states, state);
        }else{ // user joining channel
            if(!channel_id) return; // It should never occur that we get a voice state update to a state that we don't know

            channel = StateManager_guild_get_channel(guild, channel_id);
            if(!channel) return;
            user = StateManager_add_user(manager, username, discriminator, user_id, avatar);
            
            state = g_new0(DiscordGuildVoiceState, 1);
            state->user = user;
            state->channel = channel;
            state->self_mute = self_mute;
            state->self_deaf = self_deaf;
            state->self_video = self_video;
            state->mute = mute;
            state->deaf = deaf;
            state->volume_adjust = volume_adjust;
            state->data = cb_data;

            state->row = gtk_event_box_new();
            GtkWidget *rowbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4);
            gtk_container_add(GTK_CONTAINER(state->row), rowbox);
            state->pfp = gtk_image_new();
            set_img(cache, GTK_IMAGE(state->pfp), 'a', user->id, user->avatar, AVATAR_SIZE);
            gtk_container_add(GTK_CONTAINER(rowbox), state->pfp);
            username_label = gtk_label_new(user->username);
            gtk_container_add(GTK_CONTAINER(rowbox), username_label);
            state->deafen_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), state->deafen_img, FALSE, TRUE, 2);
            state->mute_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), state->mute_img, FALSE, TRUE, 2);
            state->video_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), state->video_img, FALSE, TRUE, 2);

            state->vu_meter = gtk_drawing_area_new();
            state->vu_pattern = manager->vu_pattern;
            g_signal_connect(G_OBJECT(state->vu_meter), "draw", G_CALLBACK(vu_draw_guild), state);
            g_object_set(G_OBJECT(state->vu_meter), "width-request", 180, NULL);
            gtk_widget_set_valign(state->vu_meter, GTK_ALIGN_FILL);
            gtk_box_pack_end(GTK_BOX(rowbox), state->vu_meter, FALSE, TRUE, 0);

            if(!is_self){
                state->volume_adjustment = gtk_adjustment_new(100.0, 0.0, 200.0, 1.0, 0.0, 0.0);
                g_signal_connect(G_OBJECT(state->volume_adjustment), "value-changed", G_CALLBACK(volume_adjusted), state);
                GtkWidget *volume_scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, state->volume_adjustment);
                g_object_set(G_OBJECT(volume_scale), "width-request", 200, NULL);
                gtk_scale_set_digits(GTK_SCALE(volume_scale), 0);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 0.0, GTK_POS_BOTTOM, NULL);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 100.0, GTK_POS_BOTTOM, NULL);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 200.0, GTK_POS_BOTTOM, NULL);

                state->ctx_menu = gtk_popover_new(state->row);
                gtk_container_add(GTK_CONTAINER(state->ctx_menu), volume_scale);
                gtk_widget_hide(state->ctx_menu);

                g_signal_connect(G_OBJECT(state->row), "button-press-event", G_CALLBACK(show_ctx_menu), state->ctx_menu);
            }

            StateManager_update_guild_voice_state_imgs(manager, state);
            gtk_container_add(GTK_CONTAINER(channel->user_list), state->row);
            gtk_widget_show_all(state->row);

            g_ptr_array_add(guild->voice_states, state);
        }
    }else{
        private_state = StateManager_get_private_voice_state(manager->private_voice_states, user_id);
        if(private_state){ // user is in channel
            if(channel_id){ // user changing state
                private_state->self_mute = self_mute;
                private_state->self_deaf = self_deaf;
                private_state->self_video = self_video;
                private_state->mute = mute;
                private_state->deaf = deaf;
                StateManager_update_private_voice_state_imgs(manager, private_state);
            }else // user leaving channel
                StateManager_delete_private_voice_state(manager->private_voice_states, private_state);
        }else{ // user joining channel
            if(!channel_id) return; // It should never occur that we get a voice state update to a state that we don't know

            private_channel = StateManager_get_private_channel(manager, channel_id);
            if(!private_channel) return;
            user = StateManager_get_user(manager, user_id);
            if(!user) return;

            private_state = g_new0(DiscordPrivateVoiceState, 1);
            private_state->user = user;
            private_state->channel = private_channel;
            private_state->self_mute = self_mute;
            private_state->self_deaf = self_deaf;
            private_state->self_video = self_video;
            private_state->mute = mute;
            private_state->deaf = deaf;
            private_state->volume_adjust = volume_adjust;
            private_state->data = cb_data;

            private_state->row = gtk_event_box_new();
            GtkWidget *rowbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4);
            gtk_container_add(GTK_CONTAINER(private_state->row), rowbox);
            private_state->pfp = gtk_image_new();
            set_img(cache, GTK_IMAGE(private_state->pfp), 'a', user->id, user->avatar, AVATAR_SIZE);
            gtk_container_add(GTK_CONTAINER(rowbox), private_state->pfp);
            username_label = gtk_label_new(user->username);
            gtk_container_add(GTK_CONTAINER(rowbox), username_label);
            private_state->deafen_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), private_state->deafen_img, FALSE, TRUE, 2);
            private_state->mute_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), private_state->mute_img, FALSE, TRUE, 2);
            private_state->video_img = gtk_image_new();
            gtk_box_pack_end(GTK_BOX(rowbox), private_state->video_img, FALSE, TRUE, 2);

            private_state->vu_meter = gtk_drawing_area_new();
            private_state->vu_pattern = manager->vu_pattern;
            g_signal_connect(G_OBJECT(private_state->vu_meter), "draw", G_CALLBACK(vu_draw_private), private_state);
            g_object_set(G_OBJECT(private_state->vu_meter), "width-request", 180, NULL);
            gtk_widget_set_valign(private_state->vu_meter, GTK_ALIGN_FILL);
            gtk_box_pack_end(GTK_BOX(rowbox), private_state->vu_meter, FALSE, TRUE, 0);

            if(!is_self){
                private_state->volume_adjustment = gtk_adjustment_new(100.0, 0.0, 200.0, 1.0, 0.0, 0.0);
                g_signal_connect(G_OBJECT(private_state->volume_adjustment), "value-changed", G_CALLBACK(volume_adjusted), private_state);
                GtkWidget *volume_scale = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, private_state->volume_adjustment);
                g_object_set(G_OBJECT(volume_scale), "width-request", 200, NULL);
                gtk_scale_set_digits(GTK_SCALE(volume_scale), 0);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 0.0, GTK_POS_BOTTOM, NULL);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 100.0, GTK_POS_BOTTOM, NULL);
                gtk_scale_add_mark (GTK_SCALE(volume_scale), 200.0, GTK_POS_BOTTOM, NULL);

                private_state->ctx_menu = gtk_popover_new(private_state->row);
                gtk_container_add(GTK_CONTAINER(private_state->ctx_menu), volume_scale);
                gtk_widget_hide(private_state->ctx_menu);
            }

            StateManager_update_private_voice_state_imgs(manager, private_state);
            gtk_container_add(GTK_CONTAINER(private_channel->user_list), private_state->row);
            gtk_widget_show_all(private_state->row);

            g_ptr_array_add(manager->private_voice_states, private_state);
        }
    }
}

void StateManager_destroy(StateManager *manager){
    DiscordUser *user;
    guint users_len = manager->user_cache->len;
    // guint channels_len;
    for(guint i = 0; i < users_len; i++){
        user = g_ptr_array_index(manager->user_cache, i);
        g_free((gpointer) user->username);
        g_free((gpointer) user->discriminator);
        g_free((gpointer) user->id);
        g_free((gpointer) user->avatar);
        g_free(user);
    }
    g_ptr_array_free(manager->user_cache, TRUE);

    DiscordGuild *guild;
    DiscordGuildVoiceChannel *channel;
    DiscordGuildVoiceState *guild_voice_state;
    GtkWidget *parent;
    guint guilds_len = manager->guilds->len;
    guint channels_len, voice_states_len;
    for(guint i = 0; i < guilds_len; i++){
        guild = g_ptr_array_index(manager->guilds, i);
        g_free((gpointer) guild->name);
        g_free((gpointer) guild->id);
        g_free((gpointer) guild->icon);

        voice_states_len = guild->voice_states->len;
        for(guint j = 0; j < voice_states_len; j++){
            guild_voice_state = g_ptr_array_index(guild->voice_states, j);
            gtk_widget_destroy(guild_voice_state->row);
            g_free(guild_voice_state);
        }

        channels_len = guild->channels->len;
        for(guint j = 0; j < channels_len; j++){
            channel = g_ptr_array_index(guild->channels, j);
            g_free((gpointer) channel->id);
            g_free((gpointer) channel->name);
            parent = gtk_widget_get_parent(channel->channel_label);
            g_object_steal_data(G_OBJECT(parent), "guild");
            g_object_steal_data(G_OBJECT(channel->join_button), "channel");
            gtk_widget_destroy(channel->channel_box);
            gtk_widget_destroy(parent);
            g_free(channel);
        }

        gtk_widget_destroy(guild->popover);
        parent = gtk_widget_get_parent(guild->icon_widget);
        g_object_steal_data(G_OBJECT(parent), "guild");
        gtk_widget_destroy(parent);
        gtk_widget_destroy(guild->channels_list);
        g_ptr_array_free(guild->channels, TRUE);
        g_ptr_array_free(guild->voice_states, TRUE);
        g_free(guild);
    }
    g_ptr_array_free(manager->guilds, TRUE);

    DiscordPrivateChannel *private_channel;
    guint private_channels_len = manager->private_channels->len;
    for(guint i = 0; i < private_channels_len; i++){
        private_channel = g_ptr_array_index(manager->private_channels, i);
        g_free((gpointer) private_channel->name);
        g_free((gpointer) private_channel->id);
        g_free((gpointer) private_channel->icon);
        GtkWidget *parent = gtk_widget_get_parent(private_channel->item_box);
        g_object_steal_data(G_OBJECT(parent), "channel");
        g_object_steal_data(G_OBJECT(private_channel->join_button), "channel");
        gtk_widget_destroy(private_channel->channel_box);
        gtk_widget_destroy(parent);
        g_free(private_channel);
    }
    g_ptr_array_free(manager->private_channels, TRUE);

    DiscordPrivateVoiceState *private_voice_state;
    voice_states_len = manager->private_voice_states->len;
    for(guint i = 0; i < voice_states_len; i++){
        private_voice_state = g_ptr_array_index(manager->private_voice_states, i);
        gtk_widget_destroy(private_voice_state->row);
        g_free(private_voice_state);
    }
    g_ptr_array_free(manager->private_voice_states, TRUE);

    cairo_pattern_destroy(manager->vu_pattern);

    g_free(manager);
}
