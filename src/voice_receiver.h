#include <glib.h>
#include <opus/opus.h>

#include "ringbuf.h"

typedef struct {
    guint32 ssrc;
    guint64 user_id;
    float volume;
    OpusDecoder *decoder;
    Ringbuf *buffer;
} VoiceReceiverVoice;

#define DECODING_FRAME_SIZE 2880

typedef void (*VoiceReceiver_speaking_cb)(guint64 user_id, float volume[2], gpointer cb_data);

typedef struct {
    GList *voices;
    GMutex *mutex;
    float decoded_samples[2*DECODING_FRAME_SIZE];
    VoiceReceiver_speaking_cb speaking_cb;
    gpointer cb_data;
} VoiceReceiver;

VoiceReceiver* VoiceReceiver_create(GMutex *mutex, VoiceReceiver_speaking_cb speaking_cb, gpointer cb_data);

void VoiceReceiver_voice_speaking(VoiceReceiver *voice_receiver, guint32 ssrc, guint64 user_id);

void VoiceReceiver_voice_remove(VoiceReceiver *voice_receiver, guint64 user_id);

void VoiceReceiver_set_volume(VoiceReceiver *voice_receiver, guint64 user_id, float set_volume);

void VoiceReceiver_decode(VoiceReceiver *voice_receiver, guint32 ssrc, unsigned char *data, gint32 len);

void VoiceReceiver_read_samples(VoiceReceiver *voice_receiver, float *data, int samples, float volume);

void VoiceReceiver_destroy(VoiceReceiver *voice_receiver);
