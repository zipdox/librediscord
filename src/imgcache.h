#include <libsoup/soup.h>

#include "constants.h"

typedef struct {
    SoupSession *session;
    SoupCache *cache;
} ImgCache;

typedef void (*ImgCache_cb)(GInputStream *img_stream, gboolean animated, gpointer user_data);

ImgCache *ImgCache_create(const char *cache_dir);

void ImgCache_fetch(ImgCache *cache, char type, const char *id, const char *image, unsigned int size, ImgCache_cb callback, gpointer user_data);

void ImgCache_destroy(ImgCache *cache);
