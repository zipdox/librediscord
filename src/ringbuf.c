#include <glib.h>

#include "ringbuf.h"

Ringbuf *Ringbuf_create(int channels, int samples){
    Ringbuf *buffer = g_new0(Ringbuf, 1);
    return buffer;
}

void Ringbuf_destroy(Ringbuf *buffer){
    g_free(buffer);
}

static inline int Ringbuf_length(Ringbuf *buffer){
    if(buffer->write_pos > buffer->read_pos) return buffer->write_pos - buffer->read_pos;
    if(buffer->read_pos > buffer->write_pos) return RINGBUF_SAMPLES - buffer->read_pos + buffer->write_pos;
    return 0;
}

void Ringbuf_write(Ringbuf *buffer, float *data, int samples){
    for(int i = 0; i < samples; i++){
        if(buffer->write_pos >= RINGBUF_SAMPLES) buffer->write_pos = 0;
        buffer->buffer[buffer->write_pos * 2] = data[i * 2];
        buffer->buffer[buffer->write_pos * 2 + 1] = data[i * 2 + 1];
        buffer->write_pos++;
    }
}

void Ringbuf_read(Ringbuf *buffer, float *data, int samples, float volume){
    int length = Ringbuf_length(buffer);
    if(length > samples) length = samples;
    for(int i = 0; i < length; i++){
        if(buffer->read_pos >= RINGBUF_SAMPLES) buffer->read_pos = 0;
        data[i * 2] += buffer->buffer[buffer->read_pos * 2] * volume;
        data[i * 2 + 1] += buffer->buffer[buffer->read_pos * 2 + 1] * volume;
        buffer->read_pos++;
    }
}
