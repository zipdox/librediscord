#include <glib.h>
#include <gtk/gtk.h>

#include "setimg.h"

// typedef struct {
//     const char *user_id;
//     const char *session_id;
//     gboolean self_mute;
//     gboolean self_deaf;
//     gboolean self_video;
//     gboolean mute;
//     gboolean deaf;
//     const char *channel_id;
// } DiscordUserVoiceState;

// typedef struct {
//     const char *token;
//     const char *guild_id;
//     const char *endpoint;
//     const char *channel_id;
// } DiscordVoiceServerUpdate;

typedef struct {
    const char *name;
    const char *id;
    guint64 id_int;
    const char *icon;
    GPtrArray *channels;
    GPtrArray *voice_states;
    GtkWidget *icon_widget;
    GtkWidget *popover;
    GtkWidget *channels_list;
} DiscordGuild;

typedef struct {
    const char *name;
    const char *id;
    guint64 id_int;
    DiscordGuild *guild;
    gboolean can_join;
    GtkWidget *channel_label;
    GtkWidget *channel_box;
    GtkWidget *join_button;
    GtkWidget *user_list;
} DiscordGuildVoiceChannel;

typedef struct {
    const char *name;
    const char *id;
    guint64 id_int;
    const char *icon;
    char type;
    GtkWidget *channel_box;
    GtkWidget *item_box;
    GtkWidget *join_button;
    GtkWidget *user_list;
} DiscordPrivateChannel;

typedef struct {
    const char *username;
    const char *discriminator;
    const char *id;
    guint64 id_int;
    const char *avatar;
} DiscordUser;

typedef void (*StateManager_volume_adjust_cb)(const char *user_id, float volume, gpointer cb_data);

typedef void (*StateManager_mute_icon)(GtkImage *icon, gboolean mute, gpointer cb_data);

typedef void (*StateManager_deafen_icon)(GtkImage *icon, gboolean deaf, gpointer cb_data);

typedef struct {
    DiscordUser *user;
    DiscordGuildVoiceChannel *channel;
    gboolean self_mute;
    gboolean self_deaf;
    gboolean self_video;
    gboolean mute;
    gboolean deaf;
    GtkWidget *row;
    GtkWidget *pfp;
    GtkWidget *video_img;
    GtkWidget *mute_img;
    GtkWidget *deafen_img;
    GtkAdjustment *volume_adjustment;
    GtkWidget *ctx_menu;
    StateManager_volume_adjust_cb volume_adjust;
    GtkWidget *vu_meter;
    cairo_pattern_t *vu_pattern;
    float vu_vol[2];
    gpointer data;
} DiscordGuildVoiceState;

typedef struct {
    DiscordUser *user;
    DiscordPrivateChannel *channel;
    gboolean self_mute;
    gboolean self_deaf;
    gboolean self_video;
    gboolean mute;
    gboolean deaf;
    GtkWidget *row;
    GtkWidget *pfp;
    GtkWidget *video_img;
    GtkWidget *mute_img;
    GtkWidget *deafen_img;
    GtkAdjustment *volume_adjustment;
    GtkWidget *ctx_menu;
    StateManager_volume_adjust_cb volume_adjust;
    GtkWidget *vu_meter;
    cairo_pattern_t *vu_pattern;
    float vu_vol[2];
    gpointer data;
} DiscordPrivateVoiceState;

typedef struct {
    GPtrArray *guilds;
    GPtrArray *private_channels;
    GPtrArray *user_cache;
    GPtrArray *private_voice_states;
    StateManager_mute_icon mute_icon;
    StateManager_deafen_icon deafen_icon;
    cairo_pattern_t *vu_pattern;
    gpointer cb_data;
} StateManager;

StateManager *StateManager_create(StateManager_mute_icon mute_icon, StateManager_deafen_icon deafen_icon, gpointer cb_data);

DiscordGuild *StateManager_get_guild(StateManager *manager, const char *id);

DiscordUser *StateManager_add_user(StateManager *manager, const char *username, const char *discriminator, const char *id, const char *avatar);

DiscordGuild *StateManager_add_guild(StateManager *manager, const char *name, const char *id, const char *icon);

DiscordGuildVoiceChannel *StateManager_guild_add_channel(StateManager *manager, const char *guild_id, const char *name, const char *id, gboolean can_join);

DiscordGuildVoiceChannel *StateManager_guild_get_channel(DiscordGuild *guild, const char *channel_id);

DiscordPrivateChannel *StateManager_add_private_channel(StateManager *manager, const char *name, const char *id, char type, const char *icon);

DiscordPrivateChannel *StateManager_get_private_channel(StateManager *manager, const char *id);

void StateManager_user_speaking(StateManager *manager, gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2]);

void StateManager_voice_state_update(
    StateManager *manager, ImgCache *cache,
    const char *username, const char *discriminator, const char *avatar,
    const char *user_id, const char *session_id, const char *guild_id, const char *channel_id,
    gboolean self_video, gboolean self_mute, gboolean self_deaf, gboolean mute, gboolean deaf,
    gboolean is_self,
    StateManager_volume_adjust_cb volume_adjust, gpointer cb_data
);

void StateManager_destroy(StateManager *manager);
