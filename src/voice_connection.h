#include <sodium.h>

#include "voice_gateway.h"
#include "voice_socket.h"
#include "voice_receiver.h"

#define SPEAKING_TEMPLATE "{ \"op\": 5, \"d\": { \"delay\": 0 }}"

typedef void (*VoiceConnection_closed_cb)(gpointer voice_connection, gpointer cb_data);
typedef void (*VoiceConnection_stage_cb)(int stage, gpointer cb_data);
typedef void (*VoiceConnection_speaking_cb)(gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2], gpointer cb_data);

#define MAX_PACKET_SIZE 65536
#define VOICE_PAYLOAD_TYPE 120

typedef struct {
    gboolean private_channel;
    guint64 server_id;
    VoiceGateway *gateway;
    VoiceSocket *socket;
    VoiceReceiver *receiver;
    guint32 ssrc;
    gboolean ready;
    unsigned char secret_key[32];
    unsigned char nonce[crypto_secretbox_NONCEBYTES];
    unsigned char decryption_buffer[MAX_PACKET_SIZE];
    unsigned char send_buffer[MAX_PACKET_SIZE];
    unsigned char encoded_buffer[MAX_PACKET_SIZE];
    unsigned char send_nonce[crypto_secretbox_NONCEBYTES];
    GMutex *mutex;
    VoiceConnection_stage_cb stage_cb;
    VoiceConnection_closed_cb closed_cb;
    VoiceConnection_speaking_cb speaking_cb;
    OpusEncoder *encoder;
    guint16 sequence;
    guint32 timestamp; 
    float encoding_buffer[2*960];
    guint frame_size;
    guint frame_pos;
    gpointer cb_data;
    JsonNode *speaking_node;
} VoiceConnection;

VoiceConnection *VoiceConnection_create(
    const char *endpoint, const char *server_id, const char *user_id, const char *session_id, const char *token,
    VoiceConnection_stage_cb stage_cb, VoiceConnection_closed_cb closed_cb,
    VoiceConnection_speaking_cb speaking_cb,
    gboolean private_channel,
    GMutex *mutex,
    guint bitrate, guint frame_size,
    gpointer cb_data
);

void VoiceConnection_get_samples(VoiceConnection *voice_connection, float *data, int n_samples, float volume);

void VoiceConnection_speaking(VoiceConnection *voice_connection, gboolean speaking);

void VoiceConnection_send_samples(VoiceConnection *voice_connection, float *data, unsigned int n_samples);

void VoiceConnection_flush_samples(VoiceConnection *voice_connection);

void VoiceConnection_set_birate_frame_size(VoiceConnection *voice_connection, guint bitrate, guint frame_size);

void VoiceConnection_close(VoiceConnection *voice_connection);
