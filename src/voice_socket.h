#include <gio/gio.h>

typedef void (*VoiceSocket_data_cb)(unsigned char *data, gssize size, gpointer cb_data);

typedef struct {
    GSocketAddress *address;
    GSocketFamily family;
    GSocket *socket;
    GSource *source;
    VoiceSocket_data_cb data_cb;
    gpointer cb_data;
    unsigned char buffer[65536];
} VoiceSocket;

VoiceSocket* VoiceSocket_create(const char *address, guint port, VoiceSocket_data_cb data_cb, gpointer cb_data);

void VoiceSocket_send(VoiceSocket* voice_socket, unsigned char *data, gsize size);

void VoiceSocket_destroy(VoiceSocket* voice_socket);
