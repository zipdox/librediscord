#include "gui.h"
#include "credentials_manager.h"
#include "token.h"
#include "auth.h"

static void message_dialog(GUI *gui, const char *message){
    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(gui->window), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", message);
    g_signal_connect(dialog, "response", G_CALLBACK(gtk_widget_destroy), NULL);
    gtk_widget_show(dialog);
}

static void ready_cb(const char *username, const char *discriminator, const char *user_id, const char *avatar, gpointer cb_data){
    GUI *gui = cb_data;
    if(gui->add_account){
        DiscordCredentials_add(username, discriminator, user_id, avatar, gui->token);
        gui->add_account = FALSE;
    }
    g_settings_set_string(gui->settings, "selected-account", user_id);
    set_img(gui->cache, gui->profile_picture, 'a', user_id, avatar, ICON_SIZE);
    
    char *status_text;
    if(g_strcmp0("0", discriminator) == 0){
        status_text = g_markup_printf_escaped("%s", username);
    }else{
        status_text = g_markup_printf_escaped("%s<span alpha='50%%'>#%s</span>", username, discriminator);
    }
    gtk_label_set_markup(gui->username_label, status_text);
    g_free(status_text);
}

static void create_client(GUI *gui);
static void close_client(GUI *gui){
    DiscordClient_set_channel(gui->client, NULL, NULL);
    gtk_widget_hide(gui->voice_status_box);
    DiscordClient_close(gui->client);
    StateManager_destroy(gui->state_manager);
    gui->state_manager = NULL;
    gtk_label_set_text(gui->username_label, "");
}

static void set_token(GUI *gui, const char *token){
    if(gui->token) g_free((gpointer) gui->token);
    gui->token = g_strdup(token);
}

static void shutdown(GUI *gui){
    ImgCache_destroy(gui->cache);
    gtk_main_quit();
}

static void closed_cb(gboolean auth_failed, gpointer cb_data){
    GUI *gui = cb_data;
    if(gui->close){
        shutdown(gui);
    }else{
        if(gui->state_manager) StateManager_destroy(gui->state_manager);
        gui->state_manager = NULL;
        gtk_paned_add2(gui->master_pane, gui->default_channel_box);
    }

    if(auth_failed){
        message_dialog(gui, "Authentication failed");
        gui->client = NULL;
        gtk_label_set_text(gui->username_label, "");
    }
    if(gui->switch_token){
        set_token(gui, gui->switch_token);
        g_free((gpointer) gui->switch_token);
        gui->switch_token = NULL;
        create_client(gui);
        gtk_label_set_text(gui->username_label, "Logging in...");
    }
    gtk_image_set_from_icon_name(gui->profile_picture, "contact-new", GTK_ICON_SIZE_DIALOG);
}

static void user_cb(const char *username, const char *discriminator, const char *id, const char *avatar, gpointer cb_data){
    GUI *gui = cb_data;
    StateManager_add_user(gui->state_manager, username, discriminator, id, avatar);
}

gboolean popover_enter(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    g_assert(GTK_IS_POPOVER(user_data));
    gtk_popover_popup(GTK_POPOVER(user_data));
    gtk_widget_show_all(GTK_WIDGET(user_data));
    return FALSE;
}

gboolean popover_leave(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    gtk_popover_popdown(GTK_POPOVER(user_data));
    return FALSE;
}

static void guild_channel_select(GtkListBox *box, gpointer user_data){
    GUI *gui = user_data;
    GtkListBoxRow *row = gtk_list_box_get_selected_row(box);
    if(!row) return;
    DiscordGuildVoiceChannel *channel = g_object_get_data(G_OBJECT(row), "channel");
    // g_print("%s\n", channel->name);

    GtkWidget *child = gtk_paned_get_child2(gui->master_pane);
    if(child){
        g_object_ref(G_OBJECT(child));
        gtk_container_remove(GTK_CONTAINER(gui->master_pane), child);
    }

    gtk_paned_add2(gui->master_pane, channel->channel_box);
    gtk_widget_show_all(channel->channel_box);

    const char *window_title = g_strdup_printf("#%s | %s - LibreDiscord", channel->name, channel->guild->name);
    gtk_window_set_title(GTK_WINDOW(gui->window), window_title);
    g_free((gpointer) window_title);
}

static void guild_cb(const char *name, const char *id, const char *icon, gpointer cb_data){
    GUI *gui = cb_data;

    DiscordGuild *guild = StateManager_add_guild(gui->state_manager, name, id, icon);
    guild->icon_widget = gtk_event_box_new();
    GtkWidget *icon_widget_img = gtk_image_new();
    gtk_container_add(GTK_CONTAINER(guild->icon_widget), icon_widget_img);
    gtk_container_add(GTK_CONTAINER(gui->guilds_list), guild->icon_widget);
    gtk_widget_show_all(guild->icon_widget);

    GdkWindow *window = gtk_widget_get_window(guild->icon_widget);
    gdk_window_set_events(window, GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

    guild->popover = gtk_popover_new(guild->icon_widget);
    gtk_popover_set_modal(GTK_POPOVER(guild->popover), FALSE);
    gtk_popover_set_position(GTK_POPOVER(guild->popover), GTK_POS_RIGHT);
    GtkWidget *popover_label = gtk_label_new(name);
    gtk_widget_set_margin_start(GTK_WIDGET(popover_label), 8);
    gtk_widget_set_margin_end(GTK_WIDGET(popover_label), 8);
    gtk_container_add(GTK_CONTAINER(guild->popover), popover_label);
    g_signal_connect(guild->icon_widget, "enter-notify-event", G_CALLBACK(popover_enter), guild->popover);
    g_signal_connect(guild->icon_widget, "leave-notify-event", G_CALLBACK(popover_leave), guild->popover);

    guild->channels_list = gtk_list_box_new();
    g_signal_connect(guild->channels_list, "selected-rows-changed", G_CALLBACK(guild_channel_select), gui);

    GtkWidget *parent = gtk_widget_get_parent(guild->icon_widget);
    // gtk_widget_set_tooltip_text(parent, name);
    g_object_set_data(G_OBJECT(parent), "guild", guild);
    set_img(gui->cache, GTK_IMAGE(icon_widget_img), 'i', guild->id, guild->icon, ICON_SIZE);
}

static void guild_channel_join(GtkButton *button, gpointer user_data){
    GUI *gui = user_data;

    DiscordGuildVoiceChannel *channel = g_object_get_data(G_OBJECT(button), "channel");

    if(gui->client) DiscordClient_set_channel(gui->client, channel->guild->id, channel->id);
}

static void guild_channel_cb(const char *guild_id, const char *id, const char *name, gpointer cb_data){
    GUI *gui = cb_data;
    DiscordGuildVoiceChannel *channel = StateManager_guild_add_channel(gui->state_manager, guild_id, name, id, TRUE);
    if(!channel) return;
    channel->channel_label = gtk_label_new(name);
    gtk_widget_set_halign(channel->channel_label, GTK_ALIGN_START);
    gtk_container_add(GTK_CONTAINER(channel->guild->channels_list), channel->channel_label);
    GtkWidget *parent = gtk_widget_get_parent(channel->channel_label);
    g_object_set_data(G_OBJECT(parent), "channel", channel);

    channel->channel_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4);
    GtkWidget *guild_title = gtk_label_new(NULL);
    const char *guild_title_text = g_markup_printf_escaped("<span size='16000'>%s</span>", channel->guild->name);
    gtk_label_set_markup(GTK_LABEL(guild_title), guild_title_text);
    g_free((gpointer) guild_title_text);
    gtk_container_add(GTK_CONTAINER(channel->channel_box), guild_title);
    GtkWidget *channel_title = gtk_label_new(channel->name);
    gtk_container_add(GTK_CONTAINER(channel->channel_box), channel_title);
    channel->join_button = gtk_button_new_with_label("Join");
    g_object_set_data(G_OBJECT(channel->join_button), "channel", channel);
    g_signal_connect(channel->join_button, "clicked", G_CALLBACK(guild_channel_join), cb_data);
    gtk_container_add(GTK_CONTAINER(channel->channel_box), channel->join_button);
    channel->user_list = gtk_list_box_new();
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(channel->user_list), GTK_SELECTION_NONE);
    gtk_container_add(GTK_CONTAINER(channel->channel_box), channel->user_list);
}

static void private_channel_join(GtkButton *button, gpointer user_data){
    GUI *gui = user_data;

    DiscordPrivateChannel *channel = g_object_get_data(G_OBJECT(button), "channel");

    if(gui->client) DiscordClient_set_channel(gui->client, NULL, channel->id);
}

static void private_cb(const char *name, const char *id, const char *icon_id, const char *icon, char type, gpointer cb_data){
    GUI *gui = cb_data;

    DiscordPrivateChannel *private_channel = StateManager_add_private_channel(gui->state_manager, name, id, type, icon);
    g_free((gpointer) name);
    private_channel->item_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 4);
    GtkWidget *channel_icon = gtk_image_new();
    gtk_widget_show(channel_icon);
    gtk_container_add(GTK_CONTAINER(private_channel->item_box), channel_icon);
    GtkWidget *channel_label = gtk_label_new(private_channel->name);
    gtk_label_set_line_wrap(GTK_LABEL(channel_label), TRUE);
    gtk_widget_show(channel_label);
    gtk_container_add(GTK_CONTAINER(private_channel->item_box), channel_label);
    gtk_container_add(GTK_CONTAINER(gui->directs_list), private_channel->item_box);
    GtkWidget *parent = gtk_widget_get_parent(private_channel->item_box);
    g_object_set_data(G_OBJECT(parent), "channel", private_channel);
    gtk_widget_show(private_channel->item_box);
    set_img(gui->cache, GTK_IMAGE(channel_icon), type, icon_id, icon, AVATAR_SIZE);

    private_channel->channel_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4);
    GtkWidget *channel_title = gtk_label_new(NULL);
    const char *channel_title_text = g_markup_printf_escaped("<span size='16000'>%s</span>", private_channel->name);
    gtk_label_set_markup(GTK_LABEL(channel_title), channel_title_text);
    g_free((gpointer) channel_title_text);
    gtk_container_add(GTK_CONTAINER(private_channel->channel_box), channel_title);
    private_channel->join_button = gtk_button_new_with_label("Join");
    g_object_set_data(G_OBJECT(private_channel->join_button), "channel", private_channel);
    g_signal_connect(private_channel->join_button, "clicked", G_CALLBACK(private_channel_join), cb_data);
    gtk_container_add(GTK_CONTAINER(private_channel->channel_box), private_channel->join_button);
    private_channel->user_list = gtk_list_box_new();
    gtk_list_box_set_selection_mode(GTK_LIST_BOX(private_channel->user_list), GTK_SELECTION_NONE);
    gtk_container_add(GTK_CONTAINER(private_channel->channel_box), private_channel->user_list);
}

static void state_volume_adjust(const char *user_id, float volume, gpointer cb_data){
    GUI *gui = cb_data;
    if(gui->client) DiscordClient_set_user_volume(gui->client, user_id, volume);
}

static void voice_state_cb(
    const char *username, const char *discriminator, const char *avatar,
    const char *user_id, const char *session_id, const char *guild_id, const char *channel_id,
    gboolean self_video, gboolean self_mute, gboolean self_deaf, gboolean mute, gboolean deaf,
    gpointer cb_data
){
    GUI *gui = cb_data;

    StateManager_voice_state_update(
        gui->state_manager, gui->cache,
        username, discriminator, avatar,
        user_id, session_id, guild_id, channel_id,
        self_video, self_mute, self_deaf, mute, deaf,
        g_strcmp0(user_id, gui->client->user_id) == 0,
        state_volume_adjust, gui
    );

    const char *channel_label;
    DiscordGuild *guild;
    DiscordGuildVoiceChannel *guild_voice_channel;
    DiscordPrivateChannel *private_channel;
    if(g_strcmp0(user_id, gui->client->user_id) == 0){
        if(g_strcmp0(session_id, gui->client->session_id) == 0){
            if(channel_id){
                if(guild_id){
                    guild = StateManager_get_guild(gui->state_manager, guild_id);
                    if(!guild) return;
                    guild_voice_channel = StateManager_guild_get_channel(guild, channel_id);
                    if(!guild_voice_channel) return;
                    channel_label = g_strdup_printf("%s / %s", guild_voice_channel->name, guild->name);
                    gtk_label_set_text(gui->voice_channel_label, channel_label);
                    g_free((gpointer) channel_label);
                }else{
                    private_channel = StateManager_get_private_channel(gui->state_manager, channel_id);
                    if(!private_channel) return;
                    gtk_label_set_text(gui->voice_channel_label, private_channel->name);
                }
                gtk_widget_show_all(gui->voice_status_box);
            }else{
                gtk_widget_hide(gui->voice_status_box);
            }
        }else{
            gtk_widget_hide(gui->voice_status_box);
        }
    }
}

static void credentials_cb(GPtrArray *accounts, gpointer cb_data){
    GUI *gui = cb_data;
    gui->accounts = accounts;
    gui->loading_accounts = FALSE;
    DiscordCredentials *creds;
    GtkWidget *account;
    if(accounts) for(guint i = 0; i < accounts->len; i++){
        creds = g_ptr_array_index(accounts, i);
        account = gtk_label_new(NULL);
        char *account_text = g_markup_printf_escaped("%s<span alpha='50%%'>#%s</span>", creds->username, creds->discriminator);
        gtk_label_set_markup(GTK_LABEL(account), account_text);
        g_free(account_text);
        gtk_widget_show(account);
        gtk_container_add(GTK_CONTAINER(gui->accounts_list), account);
    }
    gtk_widget_show(gui->accounts_dialog);
}

static void initial_credentials_cb(GPtrArray *accounts, gpointer cb_data){
    GUI *gui = cb_data;

    gui->accounts = accounts;
    gui->loading_accounts = FALSE;
    DiscordCredentials *creds;
    if(accounts) for(guint i = 0; i < accounts->len; i++){
        creds = g_ptr_array_index(accounts, i);
        if(g_strcmp0(gui->initial_user_id, creds->user_id) == 0){
            set_token(gui, creds->token);
            create_client(gui);
            gtk_label_set_text(gui->username_label, "Logging in...");
            break;
        }
    }
    g_free((gpointer) gui->initial_user_id);
}

static void initial_login(GUI *gui, const char *user_id){
    gui->initial_user_id = user_id;
    DiscordCredentials_list(initial_credentials_cb, gui);
}

static gboolean window_delete(GtkWidget *window, GdkEvent *event, gpointer user_data){
    GUI *gui = user_data;
    if(gui->client){
        gui->close = TRUE;
        close_client(gui);
    }else
        shutdown(gui);
    return FALSE;
}

static void accounts(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    if(gui->loading_accounts) return;
    gui->loading_accounts = TRUE;
    if(gui->accounts){
        DiscordCredentials_list_clear(gui->accounts);
        gui->accounts = NULL;
    }
    GtkListBoxRow *row;
    while(1){
        row = gtk_list_box_get_row_at_index(gui->accounts_list, 0);
        if(!row) break;
        gtk_widget_destroy(GTK_WIDGET(row));
    }
    DiscordCredentials_list(credentials_cb, user_data);
}

static void accounts_find(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_entry_set_text(gui->token_entry, "");
    gtk_widget_show(gui->account_find_dialog);
}

static void accounts_add(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_entry_set_text(gui->email_entry, "");
    gtk_entry_set_text(gui->password_entry, "");
    gtk_widget_show(gui->login_dialog);
}

static gboolean logout_delete(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GUI *gui = user_data;
    g_free((gpointer) gui->logout_token);
    gui->logout_token = NULL;
    gtk_widget_hide(gui->logout_dialog);
    return TRUE;
}

static void logout_yes(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    Auth_logout(gui->logout_token);
    g_free((gpointer) gui->logout_token);
    gui->logout_token = NULL;
    gtk_widget_hide(gui->logout_dialog);
}

static void logout_no(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    g_free((gpointer) gui->logout_token);
    gui->logout_token = NULL;
    gtk_widget_hide(gui->logout_dialog);
}

static void accounts_remove(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;

    gui->loading_accounts = TRUE;
    GtkListBoxRow *row = gtk_list_box_get_row_at_index(gui->accounts_list, gui->selected_account);
    if(!row){
        gui->loading_accounts = FALSE;
        return;
    }
    gtk_widget_destroy(GTK_WIDGET(row));

    DiscordCredentials *account_to_delete = g_ptr_array_index(gui->accounts, gui->selected_account);
    if(gui->client) if(g_strcmp0(gui->client->user_id, account_to_delete->user_id) == 0) close_client(gui);
    gui->logout_token = g_strdup(account_to_delete->token);

    gtk_widget_show(gui->logout_dialog);

    DiscordCredentials_remove(gui->accounts, gui->selected_account);
    gui->loading_accounts = FALSE;
}

static void accounts_apply(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;

    if(!gui->accounts) return;
    DiscordCredentials *creds = g_ptr_array_index(gui->accounts, gui->selected_account);
    if(!creds) return;
    if(gui->client) if(g_strcmp0(creds->user_id, gui->client->user_id) == 0) return gtk_widget_hide(gui->accounts_dialog);

    if(gui->client){
        gui->switch_token = g_strdup(creds->token);
        close_client(gui);
    }else{
        set_token(gui, creds->token);
        create_client(gui);
    }
    gtk_widget_hide(gui->accounts_dialog);
}

static void account_selected(GtkListBox *box, GtkListBoxRow *row, gpointer user_data){
    GUI *gui = user_data;
    if(gui->loading_accounts) return;
    gui->selected_account = gtk_list_box_row_get_index(row);
}

static inline void token_cb(GObject *object, GAsyncResult *res, gpointer user_data){
    gpointer *data = user_data;
    GtkWidget *find_button = data[0];
    GUI *gui = data[1];
    g_free(data);
    gtk_widget_set_sensitive(find_button, TRUE);
    GError *error;
    char *token = find_token_finish(res, &error);
    if(!token) return;
    gtk_entry_set_text(gui->token_entry, token);
    g_free((gpointer) token);
}

static void find_token(GtkWidget *widget, gpointer user_data){
    gpointer *data = g_new0(gpointer, 2);
    data[0] = widget;
    data[1] = user_data;
    gtk_widget_set_sensitive(widget, FALSE);
    find_token_async(token_cb, data);
}

static void find_login(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    const char *get_token = gtk_entry_get_text(gui->token_entry);
    if(!get_token) return;
    if(strlen(get_token) == 0) return;

    if(gui->client){
        gui->switch_token = g_strdup(get_token);
        close_client(gui);
    }else{
        set_token(gui, get_token);
        create_client(gui);
    }
    gui->add_account = TRUE;
    gtk_entry_set_text(gui->token_entry, "");
    gtk_widget_hide(gui->account_find_dialog);
    gtk_widget_hide(gui->accounts_dialog);
}

static void mfa_login_cb(const char *token, const char *error_msg, gpointer user_data){
    GUI *gui = user_data;

    if(error_msg) return message_dialog(gui, error_msg);

    if(token){
        gui->add_account = TRUE;
        if(gui->client){
            gui->switch_token = g_strdup(token);
            close_client(gui);
        }else{
            set_token(gui, token);
            create_client(gui);
        }
    }

    gtk_widget_hide(gui->mfa_dialog);
}

static void email_login_cb(const char *token, const char *ticket, const char *error_msg, gpointer user_data){
    GUI *gui = user_data;

    if(error_msg) return message_dialog(gui, error_msg);

    if(ticket){
        gui->mfa_ticket = g_strdup(ticket);
        gtk_entry_set_text(gui->mfa_entry, "");
        gtk_widget_show(gui->mfa_dialog);
    }else if(token){
        gui->add_account = TRUE;
        if(gui->client){
            gui->switch_token = g_strdup(token);
            close_client(gui);
        }else{
            set_token(gui, token);
            create_client(gui);
        }
    }

    gtk_widget_hide(gui->login_dialog);
    gtk_widget_hide(gui->accounts_dialog);
}

static void login_ok(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;

    const char *email = gtk_entry_get_text(gui->email_entry);
    const char *password = gtk_entry_get_text(gui->password_entry);

    Auth_email_password_async(email, password, email_login_cb, user_data);
}

static void login_cancel(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_widget_hide(gui->login_dialog);
}

static void mfa_ok(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;

    const char *mfa_code = gtk_entry_get_text(gui->mfa_entry);
    Auth_mfa_async(gui->mfa_ticket, mfa_code, mfa_login_cb, user_data);
}

static void mfa_cancel(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_widget_hide(gui->mfa_dialog);
    g_free((gpointer) gui->mfa_ticket);
    gui->mfa_ticket = NULL;
}

static gboolean mfa_delete(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GUI *gui = user_data;

    g_free((gpointer) gui->mfa_ticket);
    gui->mfa_ticket = NULL;
    gtk_widget_hide(gui->mfa_dialog);
    return TRUE;
}

static void alert_cb(const char *alert, gpointer cb_data){
    message_dialog(cb_data, alert);
}

static const char *status_colors[] = {"#cc0000", "#ffbf00", "#00cc00"};
static void voice_status_cb(const char *text, int progress, gpointer cb_data){
    GUI *gui = cb_data;

    const char *status_text = g_markup_printf_escaped("<span foreground='%s'>%s</span>", status_colors[progress], text);
    gtk_label_set_markup(gui->voice_status_label, status_text);
    g_free((gpointer) status_text);
}

static void speaking_cb(gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2], gpointer cb_data){
    GUI *gui = cb_data;
    StateManager_user_speaking(gui->state_manager, private_channel, server_id, user_id, volume);
}

static void mute_icon(GtkImage *icon, gboolean mute, gpointer cb_data){
    GUI *gui = cb_data;
    gtk_image_set_from_pixbuf(icon, gui->icon_bufs[mute ? ICON_MICROPHONE_MUTED : ICON_MICROPHONE]);
}

static void deafen_icon(GtkImage *icon, gboolean mute, gpointer cb_data){
    GUI *gui = cb_data;
    gtk_image_set_from_pixbuf(icon, gui->icon_bufs[mute ? ICON_HEADPHONES_MUTED : ICON_HEADPHONES]);
}

static void create_client(GUI *gui){
    gui->state_manager = StateManager_create(mute_icon, deafen_icon, gui);
    gui->client = DiscordClient_create(gui->token, ready_cb, closed_cb, user_cb, guild_cb, guild_channel_cb, private_cb, voice_state_cb, alert_cb, voice_status_cb, speaking_cb, gui->settings, gui);
}

static void lock_buffer_size(GtkComboBox *widget, const char *audio_api){
    if(g_strcmp0("jack", audio_api) == 0){
        gtk_combo_box_set_active_id(widget, "0");
        gtk_widget_set_sensitive(GTK_WIDGET(widget), FALSE);
    }else{
        gtk_widget_set_sensitive(GTK_WIDGET(widget), TRUE);
    }
}

static void audio_api_changed(GtkComboBox *widget, gpointer user_data){
    GUI *gui = user_data;

    gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(gui->settings_widgets->audio_output_device));
    gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(gui->settings_widgets->audio_input_device));

    const char *selected_api = gtk_combo_box_get_active_id(widget);
    lock_buffer_size(gui->settings_widgets->buffer_size, selected_api);
    if(!selected_api) return;
    AudioDevices *devices = AudioClient_get_devices_for_api(selected_api);
    for(int i = 0; i < devices->n_outputs; i++){
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(gui->settings_widgets->audio_output_device), devices->outputs[i], devices->outputs[i]);
    }
    for(int i = 0; i < devices->n_inputs; i++){
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(gui->settings_widgets->audio_input_device), devices->inputs[i], devices->inputs[i]);
    }
    AudioDevices_clear(devices);
}

static void settings(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;

    const char *audio_api = g_settings_get_string(gui->settings, "audio-api");
    if(gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_api), audio_api))
        audio_api_changed(GTK_COMBO_BOX(gui->settings_widgets->audio_api), gui);

    guint buffer_size = g_settings_get_uint(gui->settings, "buffer-size");
    const char *buffer_size_str = g_strdup_printf("%u", buffer_size);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->buffer_size), buffer_size_str);
    lock_buffer_size(gui->settings_widgets->buffer_size, audio_api);
    g_free((gpointer) buffer_size_str);
    g_free((gpointer) audio_api);

    const char *output_device = g_settings_get_string(gui->settings, "audio-output-device");
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_output_device), output_device);
    g_free((gpointer) output_device);

    const char *input_device = g_settings_get_string(gui->settings, "audio-input-device");
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_input_device), input_device);
    g_free((gpointer) input_device);

    const char *stereo_mode = g_settings_get_string(gui->settings, "stereo-mode");
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->stereo_mode), stereo_mode);
    g_free((gpointer) stereo_mode);

    gboolean monitor = g_settings_get_boolean(gui->settings, "monitor");
    gtk_switch_set_active(GTK_SWITCH(gui->settings_widgets->monitor_enable), monitor);

    gboolean noisegate_enable = g_settings_get_boolean(gui->settings, "noisegate-enable");
    gtk_switch_set_active(GTK_SWITCH(gui->settings_widgets->noisegate_enable), noisegate_enable);

    double gate_threshold = g_settings_get_double(gui->settings, "noisegate-threshold");
    gtk_adjustment_set_value(gui->settings_widgets->gate_threshold_adjustment, gate_threshold);

    double gate_hold = g_settings_get_double(gui->settings, "noisegate-hold");
    gtk_adjustment_set_value(gui->settings_widgets->gate_hold_adjustment, gate_hold);

    double gate_decay = g_settings_get_double(gui->settings, "noisegate-decay");
    gtk_adjustment_set_value(gui->settings_widgets->gate_decay_adjustment, gate_decay);

    gboolean compressor_enable = g_settings_get_boolean(gui->settings, "compressor-enable");
    gtk_switch_set_active(GTK_SWITCH(gui->settings_widgets->compressor_enable), compressor_enable);

    double compressor_gain = g_settings_get_double(gui->settings, "compressor-gain");
    gtk_adjustment_set_value(gui->settings_widgets->compressor_gain_adjustment, compressor_gain);

    double compressor_threshold = g_settings_get_double(gui->settings, "compressor-threshold");
    gtk_adjustment_set_value(gui->settings_widgets->compressor_threshold_adjustment, compressor_threshold);

    double compressor_ratio = g_settings_get_double(gui->settings, "compressor-ratio");
    gtk_adjustment_set_value(gui->settings_widgets->compressor_ratio_adjustment, compressor_ratio);

    double compressor_attack = g_settings_get_double(gui->settings, "compressor-attack");
    gtk_adjustment_set_value(gui->settings_widgets->compressor_attack_adjustment, compressor_attack);

    double compressor_release = g_settings_get_double(gui->settings, "compressor-release");
    gtk_adjustment_set_value(gui->settings_widgets->compressor_release_adjustment, compressor_release);

    guint bitrate = g_settings_get_uint(gui->settings, "bitrate");
    const char *bitrate_str = g_strdup_printf("%u", bitrate);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->bitrate), bitrate_str);
    g_free((gpointer) bitrate_str);

    guint frame_size = g_settings_get_uint(gui->settings, "frame-size");
    const char *frame_size_str = g_strdup_printf("%u", frame_size);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(gui->settings_widgets->frame_size), frame_size_str);
    g_free((gpointer) frame_size_str);

    gtk_widget_show(gui->settings_dialog);
}

static void settings_apply(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    
    const char *audio_api = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_api));
    g_settings_set_string(gui->settings, "audio-api", audio_api);

    const char *output_device = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_output_device));
    if(output_device) g_settings_set_string(gui->settings, "audio-output-device", output_device);

    const char *input_device = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->audio_input_device));
    if(input_device) g_settings_set_string(gui->settings, "audio-input-device", input_device);

    const char *buffer_size_str = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->buffer_size));
    guint buffer_size = g_ascii_strtoull(buffer_size_str, NULL, 10);
    g_settings_set_uint(gui->settings, "buffer-size", buffer_size);

    const char *stereo_mode = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->stereo_mode));
    g_settings_set_string(gui->settings, "stereo-mode", stereo_mode);

    gboolean monitor = gtk_switch_get_active(GTK_SWITCH(gui->settings_widgets->monitor_enable));
    g_settings_set_boolean(gui->settings, "monitor", monitor);

    gboolean noisegate_enable = gtk_switch_get_active(GTK_SWITCH(gui->settings_widgets->noisegate_enable));
    g_settings_set_boolean(gui->settings, "noisegate-enable", noisegate_enable);

    double gate_threshold = gtk_adjustment_get_value(gui->settings_widgets->gate_threshold_adjustment);
    g_settings_set_double(gui->settings, "noisegate-threshold", gate_threshold);

    double gate_hold = gtk_adjustment_get_value(gui->settings_widgets->gate_hold_adjustment);
    g_settings_set_double(gui->settings, "noisegate-hold", gate_hold);

    double gate_decay = gtk_adjustment_get_value(gui->settings_widgets->gate_decay_adjustment);
    g_settings_set_double(gui->settings, "noisegate-decay", gate_decay);

    gboolean compressor_enable = gtk_switch_get_active(GTK_SWITCH(gui->settings_widgets->compressor_enable));
    g_settings_set_boolean(gui->settings, "compressor-enable", compressor_enable);

    double compressor_gain = gtk_adjustment_get_value(gui->settings_widgets->compressor_gain_adjustment);
    g_settings_set_double(gui->settings, "compressor-gain", compressor_gain);

    double compressor_threshold = gtk_adjustment_get_value(gui->settings_widgets->compressor_threshold_adjustment);
    g_settings_set_double(gui->settings, "compressor-threshold", compressor_threshold);

    double compressor_ratio = gtk_adjustment_get_value(gui->settings_widgets->compressor_ratio_adjustment);
    g_settings_set_double(gui->settings, "compressor-ratio", compressor_ratio);

    double compressor_attack = gtk_adjustment_get_value(gui->settings_widgets->compressor_attack_adjustment);
    g_settings_set_double(gui->settings, "compressor-attack", compressor_attack);

    double compressor_release = gtk_adjustment_get_value(gui->settings_widgets->compressor_release_adjustment);
    g_settings_set_double(gui->settings, "compressor-release", compressor_release);

    const char *bitrate_str = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->bitrate));
    guint bitrate = g_ascii_strtoull(bitrate_str, NULL, 10);
    g_settings_set_uint(gui->settings, "bitrate", bitrate);

    const char *frame_size_str = gtk_combo_box_get_active_id(GTK_COMBO_BOX(gui->settings_widgets->frame_size));
    guint frame_size = g_ascii_strtoull(frame_size_str, NULL, 10);
    g_settings_set_uint(gui->settings, "frame-size", frame_size);

    DiscordClient_update_audio(gui->client);

    gtk_widget_hide(gui->settings_dialog);
}

static void settings_cancel(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_widget_hide(gui->settings_dialog);
}

static void about(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_widget_show(gui->about_dialog);
}

static void deafen(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gui->deafened ^= 1;
    gtk_image_set_from_pixbuf(gui->deafen_button_icon, gui->icon_bufs[gui->deafened ? ICON_HEADPHONES_MUTED : ICON_HEADPHONES]);
    if(gui->client) DiscordClient_mute_deafen_video(gui->client, gui->muted, gui->deafened, gui->video);
}

static void mute(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gui->muted ^= 1;
    gtk_image_set_from_pixbuf(gui->mute_button_icon, gui->icon_bufs[gui->muted ? ICON_MICROPHONE_MUTED : ICON_MICROPHONE]);
    if(gui->client) DiscordClient_mute_deafen_video(gui->client, gui->muted, gui->deafened, gui->video);
}

static void camera(GtkWidget *widget, gpointer user_data){
    // GUI *gui = user_data;
    g_print("video\n");
}

static void disconnect(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    if(gui->client) DiscordClient_set_channel(gui->client, NULL, NULL);
}

static void private_channels(GtkWidget *widget, gpointer user_data){
    GUI *gui = user_data;
    gtk_list_box_unselect_all(gui->guilds_list);

    GtkWidget *child = gtk_bin_get_child(GTK_BIN(gui->channels_viewport));
    g_object_ref(G_OBJECT(child));
    gtk_container_remove(GTK_CONTAINER(gui->channels_viewport), child);

    gtk_container_add(GTK_CONTAINER(gui->channels_viewport), GTK_WIDGET(gui->directs_list));
}

static void guild_select(GtkListBox *box, gpointer user_data){
    GUI *gui = user_data;
    GtkListBoxRow *row = gtk_list_box_get_selected_row(box);
    if(!row) return;
    DiscordGuild *guild = g_object_get_data(G_OBJECT(row), "guild");
    // g_print("%s\n", guild->name);

    GtkWidget *child = gtk_bin_get_child(GTK_BIN(gui->channels_viewport));
    g_object_ref(G_OBJECT(child));
    gtk_container_remove(GTK_CONTAINER(gui->channels_viewport), child);

    gtk_container_add(GTK_CONTAINER(gui->channels_viewport), guild->channels_list);
    gtk_widget_show_all(guild->channels_list);
}

static void private_channel_select(GtkListBox *box, gpointer user_data){
    GUI *gui = user_data;
    GtkListBoxRow *row = gtk_list_box_get_selected_row(box);
    if(!row) return;
    DiscordPrivateChannel *private_channel = g_object_get_data(G_OBJECT(row), "channel");
    DiscordClient_load_private_channel(gui->client, private_channel->id);
    // g_print("%s\n", private_channel->name);

    GtkWidget *child = gtk_paned_get_child2(gui->master_pane);
    if(child){
        g_object_ref(G_OBJECT(child));
        gtk_container_remove(GTK_CONTAINER(gui->master_pane), child);
    }

    gtk_paned_add2(gui->master_pane, private_channel->channel_box);
    gtk_widget_show_all(private_channel->channel_box);

    const char *window_title = g_strdup_printf(private_channel->type == 'c' ? "%s - LibreDiscord" : "@%s - LibreDiscord" , private_channel->name);
    gtk_window_set_title(GTK_WINDOW(gui->window), window_title);
    g_free((gpointer) window_title);
}

gboolean channel_status(GtkWidget *widget, GdkEvent *event, gpointer user_data){
    GUI *gui = user_data;
    
    if(!gui->client) return FALSE; // should never happen

    GtkWidget *selected_channel_box;
    DiscordGuild *guild;
    DiscordGuildVoiceChannel *guild_channel;
    DiscordPrivateChannel *private_channel;
    const char *window_title;
    if(gui->client->voice_guild_id && gui->client->voice_channel_id){
        guild = StateManager_get_guild(gui->state_manager, gui->client->voice_guild_id);
        if(!guild) return FALSE;
        guild_channel = StateManager_guild_get_channel(guild, gui->client->voice_channel_id);
        if(!guild_channel) return FALSE;
        selected_channel_box = guild_channel->channel_box;
        window_title = g_strdup_printf("#%s | %s - LibreDiscord", guild_channel->name, guild->name);
    }else if(gui->client->voice_channel_id){
        private_channel = StateManager_get_private_channel(gui->state_manager, gui->client->voice_channel_id);
        if(!private_channel) return FALSE;
        selected_channel_box = private_channel->channel_box;
        window_title = g_strdup_printf(private_channel->type == 'c' ? "%s - LibreDiscord" : "@%s - LibreDiscord" , private_channel->name);
    }else{
        return FALSE;
    }
    
    GtkWidget *child = gtk_paned_get_child2(gui->master_pane);
    if(child){
        g_object_ref(G_OBJECT(child));
        gtk_container_remove(GTK_CONTAINER(gui->master_pane), child);
    }

    gtk_paned_add2(gui->master_pane, selected_channel_box);
    gtk_widget_show_all(selected_channel_box);

    gtk_window_set_title(GTK_WINDOW(gui->window), window_title);
    g_free((gpointer) window_title);

    return FALSE;
}

static const char *event_names[] = {
    "window_delete",
    "accounts",
    "accounts_find",
    "accounts_add",
    "accounts_remove",
    "accounts_apply",
    "accounts_delete",
    "account_selected",
    "find_token",
    "find_login",
    "login_ok",
    "login_cancel",
    "login_delete",
    "mfa_ok",
    "mfa_cancel",
    "mfa_delete",
    "logout_delete",
    "logout_yes",
    "logout_no",

    "settings",
    "settings_apply",
    "settings_cancel",
    "settings_delete",

    "audio_api_changed",

    "about",
    "about_delete",
    "about_close",

    "deafen",
    "mute",
    "camera",

    "disconnect",

    "private_channels",
    "guild_select",
    "private_channel_select",

    "channel_status"
};

static void (*event_funcs[])() = {
    (void*) window_delete,
    accounts,
    accounts_find,
    accounts_add,
    accounts_remove,
    accounts_apply,
    (void*) gtk_widget_hide_on_delete,
    account_selected,
    find_token,
    find_login,
    login_ok,
    login_cancel,
    (void*) gtk_widget_hide_on_delete,
    mfa_ok,
    mfa_cancel,
    (void*) mfa_delete,
    (void*) logout_delete,
    logout_yes,
    logout_no,

    settings,
    settings_apply,
    settings_cancel,
    (void*) gtk_widget_hide_on_delete,

    audio_api_changed,

    about,
    (void*) gtk_widget_hide_on_delete,
    gtk_widget_hide,

    deafen,
    mute,
    camera,

    disconnect,

    private_channels,
    guild_select,
    private_channel_select,

    (void*) channel_status
};

static void connect_signal(GtkBuilder *builder,
                          GObject *object,
                          const char *signal_name,
                          const char *handler_name,
                          GObject *connect_object,
                          GConnectFlags flags,
                          gpointer user_data)
{
    for(int i = 0; i < (sizeof(event_names)/sizeof(*event_names)); i++){
        if(g_strcmp0(event_names[i], handler_name) == 0) g_signal_connect(object, signal_name, G_CALLBACK(event_funcs[i]), user_data);
    }
}

static inline GUISettings *get_gui_settings(GtkBuilder *builder){
    GUISettings *settings = g_new0(GUISettings, 1);

    settings->audio_api = GTK_COMBO_BOX(gtk_builder_get_object(builder, "settings_audio_api"));
    const char **audio_apis = AudioClient_get_supported_apis();
    const char *audio_api;
    for(unsigned int i = 0; 1; i++){
        audio_api = audio_apis[i];
        if(!audio_api) break;
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(settings->audio_api), audio_api, audio_api);
    }
    g_free(audio_apis);

    settings->audio_input_device = GTK_WIDGET(gtk_builder_get_object(builder, "settings_audio_input_device"));
    settings->audio_output_device = GTK_WIDGET(gtk_builder_get_object(builder, "settings_audio_output_device"));
    settings->stereo_mode = GTK_COMBO_BOX(gtk_builder_get_object(builder, "settings_stereo_mode"));
    settings->monitor_enable = GTK_WIDGET(gtk_builder_get_object(builder, "settings_monitor_enable"));
    settings->buffer_size = GTK_COMBO_BOX(gtk_builder_get_object(builder, "settings_buffer_size"));

    settings->noisegate_enable = GTK_WIDGET(gtk_builder_get_object(builder, "settings_noisegate_enable"));
    settings->gate_threshold_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gate_threshold_adjustment"));
    settings->gate_hold_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gate_hold_adjustment"));
    settings->gate_decay_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gate_decay_adjustment"));

    settings->compressor_enable = GTK_WIDGET(gtk_builder_get_object(builder, "settings_compressor_enable"));
    settings->compressor_gain_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "gain_adjustment"));
    settings->compressor_threshold_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "threshold_adjustment"));
    settings->compressor_ratio_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "ratio_adjustment"));
    settings->compressor_attack_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "attack_adjustment"));
    settings->compressor_release_adjustment = GTK_ADJUSTMENT(gtk_builder_get_object(builder, "release_adjustment"));

    settings->bitrate = GTK_COMBO_BOX(gtk_builder_get_object(builder, "settings_bitrate"));
    settings->frame_size = GTK_COMBO_BOX(gtk_builder_get_object(builder, "settings_frame_size"));

    return settings;
}

const char *icon_name_str[] = {
    "camera-symbolic",
    "camera-off-symbolic",
    "screen-symbolic",
    "headphones-symbolic",
    "headphones-muted-symbolic",
    "microphone-symbolic",
    "microphone-muted-symbolic",
    "hang-up-symbolic",
    "settings-symbolic",
    "info-symbolic"
};

static void load_icons(GtkWidget *window, GdkPixbuf *bufs[]){
    GtkIconTheme *icon_theme = gtk_icon_theme_new();
    gtk_icon_theme_add_resource_path(icon_theme, "/icons");

    GtkStyleContext *ctx = gtk_widget_get_style_context(window);

    for(int i = 0; i < ICON_TOTAL; i++){
        const gchar *icon_name[] = {icon_name_str[i], NULL};
        GtkIconInfo *icon_info = gtk_icon_theme_choose_icon(icon_theme, icon_name, 24, 0);
        bufs[i] = gtk_icon_info_load_symbolic_for_context(icon_info, ctx, NULL, NULL);
        g_object_unref(icon_info);
    }

    g_object_unref(icon_theme);
}

typedef struct {GdkPixbuf *buf; const char *name;} IconCombo;
static void apply_icons(GtkBuilder *builder, IconCombo *combo, int n_icons){
    for(int i = 0; i < n_icons; i++){
        gtk_image_set_from_pixbuf(GTK_IMAGE(gtk_builder_get_object(builder, combo[i].name)), combo[i].buf);
    }
}

GUI *GUI_create(GSettings *settings, int *argc, char ***argv, const char *cache_dir){
    GUI *gui = g_new0(GUI, 1);
    gui->settings = settings;
    gui->cache = ImgCache_create(cache_dir);
    gtk_init(argc, argv);

    gui->builder = gtk_builder_new_from_resource("/librediscord.glade");
    gui->window = GTK_WIDGET(gtk_builder_get_object(gui->builder, "window_main"));

    load_icons(gui->window, gui->icon_bufs);

    gui->master_pane = GTK_PANED(gtk_builder_get_object(gui->builder, "master_pane"));
    gui->default_channel_box = GTK_WIDGET(gtk_builder_get_object(gui->builder, "default_channel_box"));

    gui->channels_viewport = GTK_WIDGET(gtk_builder_get_object(gui->builder, "channels_viewport"));
    gui->directs_list = GTK_LIST_BOX(gtk_builder_get_object(gui->builder, "directs_list"));
    gui->guilds_list = GTK_LIST_BOX(gtk_builder_get_object(gui->builder, "guilds_list"));

    gui->accounts_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "accounts_dialog"));
    gui->accounts_list = GTK_LIST_BOX(gtk_builder_get_object(gui->builder, "accounts_list"));
    gui->account_find_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "account_find_dialog"));
    gui->token_entry = GTK_ENTRY(gtk_builder_get_object(gui->builder, "token_entry"));
    gui->login_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "login_dialog"));
    gui->email_entry = GTK_ENTRY(gtk_builder_get_object(gui->builder, "email_entry"));
    gui->password_entry = GTK_ENTRY(gtk_builder_get_object(gui->builder, "password_entry"));
    gui->logout_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "logout_dialog"));
    gui->mfa_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "mfa_dialog"));
    gui->mfa_entry = GTK_ENTRY(gtk_builder_get_object(gui->builder, "mfa_entry"));

    gui->settings_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "settings_dialog"));

    gui->about_dialog = GTK_WIDGET(gtk_builder_get_object(gui->builder, "about_dialog"));
    GdkPixbuf *librediscord_logo = gdk_pixbuf_new_from_resource_at_scale("/icon.svg", 128, 128, FALSE, NULL);
    gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(gui->about_dialog), librediscord_logo);
    g_object_unref(librediscord_logo);

    gui->deafen_button_icon = GTK_IMAGE(gtk_builder_get_object(gui->builder, "deafen_button_icon"));
    gui->mute_button_icon = GTK_IMAGE(gtk_builder_get_object(gui->builder, "mute_button_icon"));
    gui->camera_button_icon = GTK_IMAGE(gtk_builder_get_object(gui->builder, "camera_button_icon"));
    
    IconCombo icons_to_apply[] = {
        {gui->icon_bufs[ICON_HEADPHONES], "deafen_button_icon"},
        {gui->icon_bufs[ICON_MICROPHONE], "mute_button_icon"},
        {gui->icon_bufs[ICON_CAMERA_OFF], "camera_button_icon"},
        {gui->icon_bufs[ICON_SCREEN], "screen_button_icon"},
        {gui->icon_bufs[ICON_HANG_UP], "hang_up_icon"},
        {gui->icon_bufs[ICON_SETTINGS], "settings_button_icon"},
        {gui->icon_bufs[ICON_INFO], "info_button_icon"}
    };
    apply_icons(gui->builder, icons_to_apply, sizeof(icons_to_apply) / sizeof(IconCombo));

    gui->voice_status_box = GTK_WIDGET(gtk_builder_get_object(gui->builder, "voice_status_box"));
    gtk_widget_hide(gui->voice_status_box);
    gui->voice_status_label = GTK_LABEL(gtk_builder_get_object(gui->builder, "voice_status_label"));
    gui->voice_channel_label = GTK_LABEL(gtk_builder_get_object(gui->builder, "voice_channel_label"));

    gui->profile_picture = GTK_IMAGE(gtk_builder_get_object(gui->builder, "profile_picture"));
    gui->username_label = GTK_LABEL(gtk_builder_get_object(gui->builder, "username_label"));

    gtk_builder_connect_signals_full(gui->builder, connect_signal, gui);

    gui->settings_widgets = get_gui_settings(gui->builder);

    const char *selected_account = g_settings_get_string(gui->settings, "selected-account");

    if(strlen(selected_account) > 0)
        initial_login(gui, selected_account);

    return gui;
}

void GUI_run(GUI *gui){
    gtk_widget_show(gui->window);
    gtk_main();
}
