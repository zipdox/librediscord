#include <glib.h>

typedef struct {
    float threshold;
    int hold;
    int decay;

    unsigned int holding;
    unsigned int decaying;
} Noisegate;

Noisegate *Noisegate_create(float threshold, float hold, float decay);

void Noisegate_set_params(Noisegate *gate, float threshold, float hold, float decay);

gboolean Noisegate_process(Noisegate *gate, float *out, float *in, unsigned int n_frames);

void Noisegate_destroy(Noisegate *gate);
