#include <math.h>

#include "noisegate.h"

#define SAMPLE_RATE 48000.0

#define dB2amplitude(dB) powf(10, dB/20.0)

Noisegate *Noisegate_create(float threshold, float hold, float decay){
    Noisegate *gate = g_new0(Noisegate, 1);
    gate->threshold = dB2amplitude(threshold);
    gate->hold = hold * SAMPLE_RATE;
    gate->decay = decay * SAMPLE_RATE;

    return gate;
}

void Noisegate_set_params(Noisegate *gate, float threshold, float hold, float decay){
    gate->threshold = dB2amplitude(threshold);
    gate->hold = hold * SAMPLE_RATE;
    gate->decay = decay * SAMPLE_RATE;
}

gboolean Noisegate_process(Noisegate *gate, float *out, float *in, unsigned int n_frames){
    gboolean sound = FALSE;
    float amplitude;

    for(unsigned int i = 0; i < n_frames; i++){
        amplitude = 1.0;
        if(fmaxf(fabsf(in[i * 2]), fabsf(in[i * 2 + 1])) > gate->threshold){
            // active
            gate->holding = gate->decaying = 0;
            sound = TRUE;
        }else{
            if(gate->holding < gate->hold){
                // hold
                sound = TRUE;
                gate->holding++;
            }else{
                if(gate->decaying < gate->decay){
                    // decay
                    amplitude = 1.0 - ((float) gate->decaying) / ((float) gate->decay);
                    sound = TRUE;
                    gate->decaying++;
                }else{
                    // after decayed
                    amplitude = 0.0;
                }
            }
        }
        out[i * 2] = in[i * 2] * amplitude;
        out[i * 2 + 1] = in[i * 2 + 1] * amplitude;
    }

    return sound;
}

void Noisegate_destroy(Noisegate *gate){
    g_free(gate);
}
