#include <libsoup/soup.h>
#include <json-glib/json-glib.h>

#include "auth.h"
#include "constants.h"

#define LOGOUT_URL "https://discord.com/api/v9/auth/logout"
#define LOGIN_URL "https://discord.com/api/v9/auth/login"
#define MFA_URL "https://discord.com/api/v9/auth/mfa/totp"

#define LOGOUT_PAYLOAD "{\"provider\":null,\"voip_provider\":null}"
#define LOGIN_TEMPLATE "{\"undelete\":false,\"captcha_key\":null,\"login_source\":null,\"gift_code_sku_id\":null}"
#define MFA_TEMPLATE "{\"login_source\":null,\"gift_code_sku_id\":null}"
#define LOGOUT_REFERER "https://discord.com/channels/@me"
#define LOGIN_REFERER "https://discord.com/login"

#define CAPTCHA_REQUIRED "Captcha required. Usually this means one of these things:\n• You're logging in on a new IP-address\n• Invalid credentials\n• You're using a VPN or proxy or other public network\n\nLog in using a browser first and try again, or import a token directly."

void Auth_logout(const char *token){
    SoupSession *session = soup_session_new();
    SoupMessage *msg = soup_message_new("POST", LOGOUT_URL);

    soup_message_set_request(msg, "application/json", SOUP_MEMORY_COPY, LOGOUT_PAYLOAD, strlen (LOGOUT_PAYLOAD));

    soup_message_headers_append(msg->request_headers, "Authorization", token);
    soup_message_headers_append(msg->request_headers, "Referer", LOGOUT_REFERER);
    soup_message_headers_replace(msg->request_headers, "User-Agent", USER_AGENT);

    soup_session_queue_message(session, msg, NULL, NULL);
    g_object_unref(session);
}

typedef struct {
    Auth_email_password_cb callback;
    gpointer user_data;
} AuthStruct;

static void Auth_email_password_result(SoupSession *session, SoupMessage *msg, gpointer user_data){
    AuthStruct *auth = user_data;

    SoupMessageBody *body = msg->response_body;

    GError *error = NULL;
    JsonNode *response_node = json_from_string(body->data, &error);
    g_object_unref(session);
    if(!response_node){
        auth->callback(NULL, NULL, error->message, auth->user_data);
        g_error_free(error);
        g_free(user_data);
        return;
    }

    JsonObject *response_obj =json_node_get_object(response_node);

    const char *token;
    if(json_object_has_member(response_obj, "token")) if(!json_object_get_null_member(response_obj, "token")){
        token = json_object_get_string_member(response_obj, "token");
        auth->callback(token, NULL, NULL, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    if(json_object_has_member(response_obj, "captcha_key")){
        auth->callback(NULL, NULL, CAPTCHA_REQUIRED, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    const char *login_message;
    if(json_object_has_member(response_obj, "message")){
        login_message = json_object_get_string_member(response_obj, "message");
        auth->callback(NULL, NULL, login_message, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    const char *ticket;
    if(json_object_has_member(response_obj, "ticket")) if(json_object_get_boolean_member(response_obj, "mfa")){
        ticket = json_object_get_string_member(response_obj, "ticket");
        auth->callback(NULL, ticket, NULL, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    auth->callback(NULL, NULL, "Unknown error loggin in.", auth->user_data);

    json_node_free(response_node);

    g_free(user_data);
}

typedef struct {
    Auth_mfa_cb callback;
    gpointer user_data;
} MFAStruct;

void Auth_email_password_async(const char *email, const char *password, Auth_email_password_cb callback, gpointer user_data){
    AuthStruct *auth = g_new0(AuthStruct, 1);
    auth->callback = callback;
    auth->user_data = user_data;

    SoupSession *session = soup_session_new();
    SoupMessage *msg = soup_message_new("POST", LOGIN_URL);

    JsonNode *login_node = json_from_string(LOGIN_TEMPLATE, NULL);
    JsonObject *login_object = json_node_get_object(login_node);
    json_object_set_string_member(login_object, "login", email);
    json_object_set_string_member(login_object, "password", password);
    const char *login_str = json_to_string(login_node, FALSE);
    soup_message_set_request(msg, "application/json", SOUP_MEMORY_COPY, login_str, strlen (login_str));
    g_free((gpointer) login_str);
    json_node_free(login_node);

    soup_message_headers_append(msg->request_headers, "Referer", LOGIN_REFERER);
    soup_message_headers_replace(msg->request_headers, "User-Agent", USER_AGENT);

    soup_session_queue_message(session, msg, Auth_email_password_result, auth);
}

static void Auth_mfa_result(SoupSession *session, SoupMessage *msg, gpointer user_data){
    MFAStruct *auth = user_data;

    SoupMessageBody *body = msg->response_body;

    GError *error = NULL;
    JsonNode *response_node = json_from_string(body->data, &error);
    g_object_unref(session);
    if(!response_node){
        auth->callback(NULL, error->message, auth->user_data);
        g_error_free(error);
        g_free(user_data);
        return;
    }
    JsonObject *response_obj =json_node_get_object(response_node);

    const char *token;
    if(json_object_has_member(response_obj, "token")) if(!json_object_get_null_member(response_obj, "token")){
        token = json_object_get_string_member(response_obj, "token");
        auth->callback(token, NULL, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    if(json_object_has_member(response_obj, "captcha_key")){
        auth->callback(NULL, CAPTCHA_REQUIRED, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    const char *login_message;
    if(json_object_has_member(response_obj, "message")){
        login_message = json_object_get_string_member(response_obj, "message");
        auth->callback(NULL,  login_message, auth->user_data);
        json_node_free(response_node);
        g_free(user_data);
        return;
    }

    auth->callback(NULL, "Unknown error with 2FA.", auth->user_data);

    json_node_free(response_node);

    g_free(user_data);

}

void Auth_mfa_async(const char *ticket, const char *code, Auth_mfa_cb callback, gpointer user_data){
    MFAStruct *mfa = g_new0(MFAStruct, 1);
    mfa->callback = callback;
    mfa->user_data = user_data;

    SoupSession *session = soup_session_new();
    SoupMessage *msg = soup_message_new("POST", MFA_URL);

    JsonNode *mfa_node = json_from_string(MFA_TEMPLATE, NULL);
    JsonObject *mfa_object = json_node_get_object(mfa_node);
    json_object_set_string_member(mfa_object, "code", code);
    json_object_set_string_member(mfa_object, "ticket", ticket);
    const char *mfa_str = json_to_string(mfa_node, FALSE);
    soup_message_set_request(msg, "application/json", SOUP_MEMORY_COPY, mfa_str, strlen (mfa_str));
    g_free((gpointer) mfa_str);
    json_node_free(mfa_node);

    soup_message_headers_append(msg->request_headers, "Referer", LOGIN_REFERER);
    soup_message_headers_replace(msg->request_headers, "User-Agent", USER_AGENT);

    soup_session_queue_message(session, msg, Auth_mfa_result, mfa);
}
