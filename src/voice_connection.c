#include <json-glib/json-glib.h>

#include "voice_connection.h"
#include "rtp.h"

static inline void ip_discovered(VoiceConnection *voice_connection, const char *my_ip, guint my_port){
    voice_connection->stage_cb(2, voice_connection->cb_data);

    JsonNode *root_node = json_from_string(SELECT_PROTOCOL_TEMPLATE, NULL);
    JsonObject *root_obj = json_node_get_object(root_node);
    JsonObject *d_obj = json_object_get_object_member(root_obj, "d");
    JsonObject *data_obj = json_object_get_object_member(d_obj, "data");

    json_object_set_string_member(data_obj, "address", my_ip);
    json_object_set_int_member(data_obj, "port", my_port);

    VoiceGateway_send(voice_connection->gateway, root_node);

    json_node_free(root_node);
}

static inline int decrypt_packet(VoiceConnection *voice_connection, unsigned char *data, gssize rtp_size, gssize size){
    memcpy(voice_connection->nonce, data, 12);
    int decrypted = crypto_secretbox_open_easy(voice_connection->decryption_buffer, data + rtp_size, size - rtp_size, voice_connection->nonce, voice_connection->secret_key);
    if(decrypted != 0) return 0;
    return size - 12 - crypto_secretbox_MACBYTES;
}

static void handle_socket_data(unsigned char *data, gssize size, gpointer cb_data){
    VoiceConnection *voice_connection = cb_data;
    if(size == 74 && data[0] == 0 && data[1] == 2){
        data[71] = 0;
        const char *my_ip = (const char*) data + 8;
        guint my_port = (data[72] << 8) | data[73];
        ip_discovered(voice_connection, my_ip, my_port);
        return;
    }
    if(!voice_connection->ready) return;

    gboolean padding, extension, marker;
    guint8 cc, pt;
    guint16 sequence;
    guint32 timestamp, ssrc;
    guint32 csrcs[15];

    gssize rtp_size = read_rtp_header(data, size, &padding, &extension, &cc, &marker, &pt, &sequence, &timestamp, &ssrc, csrcs);
    if(rtp_size == 0) return;
    if(pt != VOICE_PAYLOAD_TYPE) return;

    int decrypted_len = decrypt_packet(voice_connection, data, rtp_size, size);
    if(decrypted_len == 0) return;

    int packet_offset = cc * 4;
    if (extension != 0 && decrypted_len > 4){
        //The first "4" is in order to ignore profile-specific extension header ID
        //and the the extension header id lenght, 16 bits each
        //The second "4" is because the length of extension is measured in units of 32bits
        //If length is 3, then there are 32*3 bits, or 4*3 in this case where each buffer position holds 8 bits
        packet_offset += 4 + 4*((voice_connection->decryption_buffer[packet_offset + 2] << 8) | voice_connection->decryption_buffer[packet_offset + 3]);
    }
    if(packet_offset >= decrypted_len) return;

    VoiceReceiver_decode(voice_connection->receiver, ssrc, voice_connection->decryption_buffer + packet_offset, decrypted_len - packet_offset);
}

static inline void discover_ip(VoiceConnection *voice_connection){
    unsigned char *buffer = voice_connection->send_buffer;
    memset((void*) buffer, 0, 74);
    buffer[1] = 1;
    buffer[3] = 70;
    guint32 ssrc = voice_connection->ssrc;
    buffer[4] = ssrc >> 24  & 0xFF;
    buffer[5] = ssrc >> 16 & 0xFF;
    buffer[6] = ssrc >> 8 & 0xFF;
    buffer[7] = ssrc & 0xFF;
    VoiceSocket_send(voice_connection->socket, buffer, 74);
}

static inline void handle_ready(VoiceConnection *voice_connection, JsonObject *data_obj){
    const char *ip = json_object_get_string_member(data_obj, "ip");
    guint port = json_object_get_int_member(data_obj, "port");
    voice_connection->ssrc = json_object_get_int_member(data_obj, "ssrc");
    voice_connection->stage_cb(1, voice_connection->cb_data);

    voice_connection->socket = VoiceSocket_create(ip, port, handle_socket_data, (gpointer) voice_connection);
    discover_ip(voice_connection);
}

static inline void handle_session_description(VoiceConnection *voice_connection, JsonObject *data_obj){
    JsonArray* key_array = json_object_get_array_member(data_obj, "secret_key");
    for(int i = 0; i < 32; i++)
        voice_connection->secret_key[i] = json_array_get_int_element(key_array, i);
    voice_connection->ready = TRUE;
}

static inline void handle_speaking(VoiceConnection *voice_connection, JsonObject *data_obj){
    const char *user_id_str = json_object_get_string_member(data_obj, "user_id");
    guint64 user_id = g_ascii_strtoull(user_id_str, NULL, 10);
    guint32 ssrc = json_object_get_int_member(data_obj, "ssrc");
    VoiceReceiver_voice_speaking(voice_connection->receiver, ssrc, user_id);
}

static inline void handle_user_left(VoiceConnection *voice_connection, JsonObject *data_obj){
    const char *user_id_str = json_object_get_string_member(data_obj, "user_id");
    guint64 user_id = g_ascii_strtoull(user_id_str, NULL, 10);
    VoiceReceiver_voice_remove(voice_connection->receiver, user_id);
}

static void gateway_message_cb(gint64 opcode, JsonObject *data_obj, gpointer cb_data){
    VoiceConnection *voice_connection = cb_data;
    switch(opcode){
        case 2:
            handle_ready(voice_connection, data_obj);
            break;
        case 4:
            handle_session_description(voice_connection, data_obj);
            break;
        case 5:
            handle_speaking(voice_connection, data_obj);
            break;
        case 13:
            handle_user_left(voice_connection, data_obj);
        default:
            break;
    }
}

static void gateway_closed_cb(gpointer cb_data){
    VoiceConnection *voice_connection = cb_data;
    g_mutex_lock(voice_connection->mutex);
    voice_connection->ready = FALSE;

    VoiceGateway_destroy(voice_connection->gateway);
    if(voice_connection->socket) VoiceSocket_destroy(voice_connection->socket);
    voice_connection->socket = NULL;

    if(voice_connection->receiver) VoiceReceiver_destroy(voice_connection->receiver);
    voice_connection->receiver = NULL;
    
    voice_connection->closed_cb((gpointer) voice_connection, voice_connection->cb_data);

    opus_encoder_destroy(voice_connection->encoder);

    g_mutex_unlock(voice_connection->mutex);

    json_node_free(voice_connection->speaking_node);
    g_free(voice_connection);
}

static void receiver_speaking_cb(guint64 user_id, float volume[2], gpointer cb_data){
    VoiceConnection *voice_connection = cb_data;
    voice_connection->speaking_cb(voice_connection->private_channel, voice_connection->server_id, user_id, volume, voice_connection->cb_data);
}

VoiceConnection *VoiceConnection_create(
    const char *endpoint, const char *server_id, const char *user_id, const char *session_id, const char *token,
    VoiceConnection_stage_cb stage_cb, VoiceConnection_closed_cb closed_cb,
    VoiceConnection_speaking_cb speaking_cb,
    gboolean private_channel,
    GMutex *mutex,
    guint bitrate, guint frame_size,
    gpointer cb_data
){
    VoiceConnection *voice_connection = g_new0(VoiceConnection, 1);
    voice_connection->gateway = VoiceGateway_create(endpoint, server_id, user_id, session_id, token, gateway_message_cb, gateway_closed_cb, (gpointer) voice_connection);
    voice_connection->server_id = g_ascii_strtoull(server_id, NULL, 10);
    voice_connection->socket = NULL;
    voice_connection->receiver = VoiceReceiver_create(mutex, receiver_speaking_cb, voice_connection);
    voice_connection->mutex = mutex;
    voice_connection->stage_cb = stage_cb;
    voice_connection->closed_cb = closed_cb;
    voice_connection->speaking_cb = speaking_cb;
    voice_connection->cb_data = cb_data;
    voice_connection->speaking_node = json_from_string(SPEAKING_TEMPLATE, NULL);
    voice_connection->private_channel = private_channel;

    int opus_error;
    voice_connection->encoder = opus_encoder_create(48000, 2, OPUS_APPLICATION_AUDIO, &opus_error);
    if(opus_error != OPUS_OK)
        g_print("Failed to create Opus encoder!\n");
    opus_encoder_ctl(voice_connection->encoder, OPUS_SET_BITRATE(bitrate));
    voice_connection->frame_size = frame_size;

    return voice_connection;
}

void VoiceConnection_get_samples(VoiceConnection *voice_connection, float *data, int samples, float volume){
    VoiceReceiver_read_samples(voice_connection->receiver, data, samples, volume);
}

void VoiceConnection_speaking(VoiceConnection *voice_connection, gboolean speaking){
    JsonObject *speaking_obj = json_node_get_object(voice_connection->speaking_node);
    JsonObject *data_obj = json_object_get_object_member(speaking_obj, "d");

    json_object_set_boolean_member(data_obj, "speaking", speaking);
    json_object_set_int_member(data_obj, "ssrc", voice_connection->ssrc);

    VoiceGateway_send(voice_connection->gateway, voice_connection->speaking_node);
}

static inline void encrypt_and_send_encoded(VoiceConnection *voice_connection, gssize header_len, opus_int32 encoded_size){
    memcpy(voice_connection->send_nonce, voice_connection->send_buffer, 12);

    crypto_secretbox_easy(voice_connection->send_buffer + header_len, voice_connection->encoded_buffer, encoded_size, voice_connection->send_nonce, voice_connection->secret_key);

    gsize packet_size = header_len + encoded_size + crypto_secretbox_MACBYTES;

    VoiceSocket_send(voice_connection->socket, voice_connection->send_buffer, packet_size);
}

static inline void encode_samples(VoiceConnection *voice_connection){
    gssize header_len = write_rtp_header(voice_connection->send_buffer, FALSE, FALSE, 0, FALSE, VOICE_PAYLOAD_TYPE, voice_connection->sequence, voice_connection->timestamp, voice_connection->ssrc, NULL);

    opus_int32 encoded_size = opus_encode_float(voice_connection->encoder, voice_connection->encoding_buffer, voice_connection->frame_size, voice_connection->encoded_buffer, MAX_PACKET_SIZE - header_len - crypto_secretbox_MACBYTES);

    encrypt_and_send_encoded(voice_connection, header_len, encoded_size);

    voice_connection->sequence++;
    voice_connection->timestamp += voice_connection->frame_size;
}

void VoiceConnection_send_samples(VoiceConnection *voice_connection, float *data, unsigned int n_samples){
    for(unsigned int i = 0; i < n_samples; i++){
        voice_connection->encoding_buffer[voice_connection->frame_pos * 2] = data[i * 2];
        voice_connection->encoding_buffer[voice_connection->frame_pos * 2 + 1] = data[i * 2 + 1];
        voice_connection->frame_pos++;

        if(voice_connection->frame_pos >= voice_connection->frame_size){
            encode_samples(voice_connection);
            voice_connection->frame_pos = 0;
        }
    }
}

void VoiceConnection_flush_samples(VoiceConnection *voice_connection){
    if(voice_connection->frame_pos == 0) return;
    for(; voice_connection->frame_pos < voice_connection->frame_size; voice_connection->frame_pos++){
        voice_connection->encoding_buffer[voice_connection->frame_pos * 2] = 0.0;
        voice_connection->encoding_buffer[voice_connection->frame_pos * 2 + 1] = 0.0;
    }
    encode_samples(voice_connection);
    voice_connection->frame_pos = 0;
}

void VoiceConnection_set_birate_frame_size(VoiceConnection *voice_connection, guint bitrate, guint frame_size){
    g_mutex_lock(voice_connection->mutex);
    opus_encoder_ctl(voice_connection->encoder, OPUS_SET_BITRATE(bitrate));
    voice_connection->frame_size = frame_size;
    g_mutex_unlock(voice_connection->mutex);
}

void VoiceConnection_close(VoiceConnection *voice_connection){
    voice_connection->ready = FALSE;
    VoiceGateway_close(voice_connection->gateway);

    if(voice_connection->socket) VoiceSocket_destroy(voice_connection->socket);
    voice_connection->socket = NULL;

    if(voice_connection->receiver) VoiceReceiver_destroy(voice_connection->receiver);
    voice_connection->receiver = NULL;
}
