#include "credentials_manager.h"

typedef struct {
    guint len;
    GPtrArray *accounts;
    DiscordCredentials_list_cb callback;
    gpointer cb_data;
} DiscordCredentials_search_struct;

static void DiscordCredentials_delete(DiscordCredentials *creds){
    g_free((gpointer) creds->username);
    g_free((gpointer) creds->discriminator);
    g_free((gpointer) creds->user_id);
    g_free((gpointer) creds->avatar);
    g_free((gpointer) creds->token);
    g_free(creds);
}

void DiscordCredentials_list_clear(GPtrArray *accounts){
    DiscordCredentials *creds;
    for(guint i = 0; i < accounts->len; i++){
        creds = g_ptr_array_index(accounts, i);
        DiscordCredentials_delete(creds);
    }
    g_ptr_array_free(accounts, TRUE);
}

#if defined _WIN32 || __CYGWIN__ 

#include <windows.h>
#include <wincred.h>
#include <wchar.h>
#include <json-glib/json-glib.h>
#include <glib/gprintf.h>

void DiscordCredentials_list(DiscordCredentials_list_cb callback, gpointer cb_data){
    DWORD count;
    PCREDENTIALW *creds;
    BOOL ok = CredEnumerateW(L"LibreDiscord-*", 0, &count, &creds);
    if(!ok){
        g_print("Error getting credentials from vault\n");
        callback(NULL, cb_data);
        return;
    }

    GPtrArray *accounts = g_ptr_array_new();

    for(DWORD i = 0; i < count; i++){
        PCREDENTIALW cred = creds[i];

        size_t comment_len = wcstombs(NULL, cred->Comment, 0);
        char comment[comment_len + 1];
        wcstombs(comment, cred->Comment, comment_len + 1);

        size_t user_id_len = wcstombs(NULL, cred->UserName, 0);
        char user_id[user_id_len + 1];
        wcstombs(user_id, cred->UserName, user_id_len + 1);

        DiscordCredentials *account_creds = g_new0(DiscordCredentials, 1);
        account_creds->user_id = g_strdup(user_id);

        JsonNode *meta_node = json_from_string(comment, NULL);
        JsonObject *meta_obj = json_node_get_object(meta_node);
        account_creds->username = g_strdup(json_object_get_string_member(meta_obj, "username"));
        account_creds->discriminator = g_strdup(json_object_get_string_member(meta_obj, "discriminator"));
        account_creds->avatar = g_strdup(json_object_get_string_member(meta_obj, "avatar"));
        json_node_free(meta_node);

        cred->CredentialBlob[cred->CredentialBlobSize - 1] = '\0';
        account_creds->token = g_strdup((char*)cred->CredentialBlob);

        g_ptr_array_add(accounts, (gpointer)account_creds);
    }

    CredFree(creds);

    callback(accounts, cb_data);
}

void DiscordCredentials_add(const char *username, const char *discriminator, const char *user_id, const char *avatar, const char *token){
    JsonNode *meta_node = json_from_string("{}", NULL);
    JsonObject *meta_obj = json_node_get_object(meta_node);
    json_object_set_string_member(meta_obj, "username", username);
    json_object_set_string_member(meta_obj, "discriminator", discriminator);
    json_object_set_string_member(meta_obj, "avatar", avatar);
    const char *comment_str = json_to_string(meta_node, FALSE);
    json_node_free(meta_node);

    CREDENTIALW cred = {0};
    cred.Type = CRED_TYPE_GENERIC;
    DWORD cbCreds = 1 + strlen(token);
    cred.CredentialBlobSize = cbCreds;
    cred.CredentialBlob = (LPBYTE) token;
    cred.Persist = CRED_PERSIST_LOCAL_MACHINE;

    char cred_name[strlen(user_id) + 14];
    g_sprintf(cred_name, "LibreDiscord-%s", user_id);
    size_t wide_cred_name_len = mbstowcs(NULL, cred_name, 0);
    wchar_t cred_name_w[wide_cred_name_len + 1];
    mbstowcs(cred_name_w, cred_name, wide_cred_name_len + 1);
    cred.TargetName = cred_name_w;

    size_t wide_user_id_len = mbstowcs(NULL, user_id, 0);
    wchar_t user_id_w[wide_user_id_len + 1];
    mbstowcs(user_id_w, user_id, wide_user_id_len + 1);
    cred.UserName = user_id_w;

    size_t wide_comment_len = mbstowcs(NULL, comment_str, 0);
    wchar_t comment_w[wide_comment_len + 1];
    mbstowcs(comment_w, comment_str, wide_comment_len + 1);
    g_free((gpointer)comment_str);
    cred.Comment = comment_w;

    BOOL ok = CredWriteW (&cred, 0);
    if(!ok) g_print("Error saving credentials to vault\n");
}

void DiscordCredentials_remove(GPtrArray *accounts, guint index){
    DiscordCredentials *account = g_ptr_array_index(accounts, index);
    const char *user_id = account->user_id;

    char cred_name[strlen(user_id) + 14];
    g_sprintf(cred_name, "LibreDiscord-%s", user_id);
    size_t wide_cred_name_len = mbstowcs(NULL, cred_name, 0);
    wchar_t cred_name_w[wide_cred_name_len + 1];
    mbstowcs(cred_name_w, cred_name, wide_cred_name_len + 1);
    
    BOOL ok = CredDeleteW(cred_name_w, CRED_TYPE_GENERIC, 0);
    if(!ok){
        g_print("Failed to delete credential from vault\n");
        return;
    }

    DiscordCredentials_delete(account);
    g_ptr_array_remove_index(accounts, index);
}

#else

#include <libsecret/secret.h>

static const SecretSchema discord_credentials_schema = {
    "net.librediscord.credentials", SECRET_SCHEMA_NONE,
    {
        {  "username", SECRET_SCHEMA_ATTRIBUTE_STRING },
        {  "discriminator", SECRET_SCHEMA_ATTRIBUTE_STRING },
        {  "user_id", SECRET_SCHEMA_ATTRIBUTE_STRING },
        {  "avatar", SECRET_SCHEMA_ATTRIBUTE_STRING },
        {  NULL, 0 },
    }
};

static void secret_callback(GObject *obj, GAsyncResult* res, gpointer user_data){
    DiscordCredentials_search_struct *search_struct = user_data;
    SecretRetrievable *secret = (SecretRetrievable*) obj;

    GHashTable *attr = secret_retrievable_get_attributes(secret);
    DiscordCredentials *account_creds = g_new0(DiscordCredentials, 1);
    account_creds->username = g_strdup(g_hash_table_lookup(attr, "username"));
    account_creds->discriminator = g_strdup(g_hash_table_lookup(attr, "discriminator"));
    account_creds->user_id = g_strdup(g_hash_table_lookup(attr, "user_id"));
    account_creds->avatar = g_strdup(g_hash_table_lookup(attr, "avatar"));
    g_hash_table_unref(attr);

    SecretValue *value = secret_retrievable_retrieve_secret_finish(secret, res, NULL);
    gsize value_len;
    account_creds->token = g_strdup(secret_value_get(value, &value_len));
    secret_value_unref(value);

    g_ptr_array_add(search_struct->accounts, account_creds);

    if(search_struct->accounts->len >= search_struct->len){
        search_struct->callback(search_struct->accounts, search_struct->cb_data);
        g_free(search_struct);
    }
}

static void list_callback(GObject* source_object, GAsyncResult* res, gpointer user_data){
    DiscordCredentials_search_struct *search_struct = user_data;
    GError *error = NULL;
    GList *list = secret_password_search_finish(res, &error);
    if(error){
        g_print("Err: %s\n", error->message);
        g_error_free(error);
        
        search_struct->callback(NULL, search_struct->cb_data);
        g_free(search_struct);
        return;
    }
    search_struct->len = g_list_length(list);
    if(search_struct->len == 0){
        search_struct->callback(NULL, search_struct->cb_data);
        g_free(search_struct);
        return;
    }
    search_struct->accounts = g_ptr_array_new();

    SecretRetrievable *secret;
    for(guint i = 0; i < search_struct->len; i++){
        secret = g_list_nth_data(list, i);
        secret_retrievable_retrieve_secret(secret, NULL, secret_callback, search_struct);
        g_object_unref(secret);
    }
    g_list_free(list);
}

void DiscordCredentials_list(DiscordCredentials_list_cb callback, gpointer cb_data){
    DiscordCredentials_search_struct *search_struct = g_new0(DiscordCredentials_search_struct, 1);
    search_struct->callback = callback;
    search_struct->cb_data = cb_data;
    secret_password_search(&discord_credentials_schema, SECRET_SEARCH_ALL, NULL, list_callback, search_struct, NULL);
}

typedef struct {
    DiscordCredentials_add_cb callback;
    gpointer cb_data;
} DiscordCredentials_add_struct;

static void add_callback(GObject* source_object, GAsyncResult* res, gpointer user_data){
    DiscordCredentials_add_struct *add_struct = user_data;

    GError *error = NULL;
    gboolean success = secret_password_store_finish(res, &error);
    if(!success){
        g_print("Err: %s\n", error->message);
        g_error_free(error);
    }
    // add_struct->callback(success, add_struct->cb_data);
    g_free(add_struct);
}

// void DiscordCredentials_add(char *username, char *user_id, char *avatar, char *token, DiscordCredentials_add_cb callback, gpointer cb_data){
void DiscordCredentials_add(const char *username, const char *discriminator, const char *user_id, const char *avatar, const char *token){
    DiscordCredentials_add_struct *add_struct = g_new(DiscordCredentials_add_struct, 1);
    // add_struct->callback = callback;
    // add_struct->cb_data = cb_data;
    secret_password_store(&discord_credentials_schema, SECRET_COLLECTION_DEFAULT, "LibreDiscord Account", token, NULL, add_callback, add_struct, "username", username, "discriminator", discriminator, "user_id", user_id, "avatar", avatar ? avatar : "", NULL);
}

static void remove_callback(GObject* source_object, GAsyncResult* res, gpointer user_data){
    GError *error = NULL;
    GList *list = secret_password_search_finish(res, &error);
    if(error){
        g_print("Err: %s\n", error->message);
        g_error_free(error);
        return;
    }
    guint len = g_list_length(list);
    if(len != 1) return g_print("Can't remove account, not found\n");
    SecretRetrievable *secret = g_list_nth_data(list, 0);
    secret_item_delete(SECRET_ITEM(secret), NULL, NULL, NULL);
}

void DiscordCredentials_remove(GPtrArray *accounts, guint index){
    DiscordCredentials *account = g_ptr_array_index(accounts, index);
    const char *user_id = account->user_id;
    secret_password_search(&discord_credentials_schema, SECRET_SEARCH_NONE, NULL, remove_callback, NULL, "user_id", user_id, NULL);
    DiscordCredentials_delete(account);
    g_ptr_array_remove_index(accounts, index);
}

#endif
