#define TOKEN_KEY "_https://discord.com\0\x01token"
#define TOKEN_KEY_LENGTH 27

#define GATEWAY_ADDRESS "wss://gateway.discord.gg/?encoding=json&v=10"
#define GATEWAY_ORIGIN "https://discord.com"
#define MAX_WS_PAYLOAD 16777216
#define USER_AGENT "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) discord/0.0.20 Chrome/91.0.4472.164 Electron/13.6.6 Safari/537.36"
#define VOICE_GATEWAY_TEMPLATE "wss://%s?v=6"

#define ICON_SIZE 48
#define AVATAR_SIZE 32
