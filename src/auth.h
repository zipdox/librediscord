#include <gio/gio.h>

void Auth_logout(const char *token);

typedef void (*Auth_email_password_cb)(const char *token, const char *ticket, const char *error_msg, gpointer user_data);

typedef void (*Auth_mfa_cb)(const char *token, const char *error_msg, gpointer user_data);

void Auth_email_password_async(const char *email, const char *password, Auth_email_password_cb callback, gpointer user_data);

void Auth_mfa_async(const char *ticket, const char *code, Auth_mfa_cb callback, gpointer user_data);
