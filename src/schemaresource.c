// stolen from GLib untill they merge my PR

#include <string.h>
#include <gio/gio.h>

#include "schemaresource.h"

typedef struct { guint16 value; } guint16_le;
typedef struct { guint32 value; } guint32_le;

struct gvdb_pointer {
  guint32_le start;
  guint32_le end;
};

struct gvdb_hash_header {
  guint32_le n_bloom_words;
  guint32_le n_buckets;
};

struct gvdb_hash_item {
  guint32_le hash_value;
  guint32_le parent;

  guint32_le key_start;
  guint16_le key_size;
  gchar type;
  gchar unused;

  union
  {
    struct gvdb_pointer pointer;
    gchar direct[8];
  } value;
};

struct gvdb_header {
  guint32 signature[2];
  guint32_le version;
  guint32_le options;

  struct gvdb_pointer root;
};

static inline guint32 guint32_from_le (guint32_le value) {
  return GUINT32_FROM_LE (value.value);
}


static inline guint16 guint16_from_le (guint16_le value) {
  return GUINT16_FROM_LE (value.value);
}

#define GVDB_SIGNATURE0 1918981703
#define GVDB_SIGNATURE1 1953390953
#define GVDB_SWAPPED_SIGNATURE0 GUINT32_SWAP_LE_BE (GVDB_SIGNATURE0)
#define GVDB_SWAPPED_SIGNATURE1 GUINT32_SWAP_LE_BE (GVDB_SIGNATURE1)

struct _GvdbTable {
  GBytes *bytes;

  const gchar *data;
  gsize size;

  gboolean byteswapped;
  gboolean trusted;

  const guint32_le *bloom_words;
  guint32 n_bloom_words;
  guint bloom_shift;

  const guint32_le *hash_buckets;
  guint32 n_buckets;

  struct gvdb_hash_item *hash_items;
  guint32 n_hash_items;
};

struct _GSettingsSchemaSource
{
  GSettingsSchemaSource *parent;
  gchar *directory;
  GvdbTable *table;
  GHashTable **text_tables;

  gint ref_count;
};

static gconstpointer
gvdb_table_dereference (GvdbTable                 *file,
                        const struct gvdb_pointer *pointer,
                        gint                       alignment,
                        gsize                     *size)
{
  guint32 start, end;

  start = guint32_from_le (pointer->start);
  end = guint32_from_le (pointer->end);

  if G_UNLIKELY (start > end || end > file->size || start & (alignment - 1))
    return NULL;

  *size = end - start;

  return file->data + start;
}

static void
gvdb_table_setup_root (GvdbTable                 *file,
                       const struct gvdb_pointer *pointer)
{
  const struct gvdb_hash_header *header;
  guint32 n_bloom_words;
  guint32 n_buckets;
  gsize size;

  header = gvdb_table_dereference (file, pointer, 4, &size);

  if G_UNLIKELY (header == NULL || size < sizeof *header)
    return;

  size -= sizeof *header;

  n_bloom_words = guint32_from_le (header->n_bloom_words);
  n_buckets = guint32_from_le (header->n_buckets);
  n_bloom_words &= (1u << 27) - 1;

  if G_UNLIKELY (n_bloom_words * sizeof (guint32_le) > size)
    return;

  file->bloom_words = (gpointer) (header + 1);
  size -= n_bloom_words * sizeof (guint32_le);
  file->n_bloom_words = n_bloom_words;

  if G_UNLIKELY (n_buckets > G_MAXUINT / sizeof (guint32_le) ||
                 n_buckets * sizeof (guint32_le) > size)
    return;

  file->hash_buckets = file->bloom_words + file->n_bloom_words;
  size -= n_buckets * sizeof (guint32_le);
  file->n_buckets = n_buckets;

  if G_UNLIKELY (size % sizeof (struct gvdb_hash_item))
    return;

  file->hash_items = (gpointer) (file->hash_buckets + n_buckets);
  file->n_hash_items = size / sizeof (struct gvdb_hash_item);
}

static GvdbTable *
gvdb_table_new_from_bytes (GBytes    *bytes,
                           gboolean   trusted,
                           GError   **error)
{
  const struct gvdb_header *header;
  GvdbTable *file;

  file = g_slice_new0 (GvdbTable);
  file->bytes = g_bytes_ref (bytes);
  file->data = g_bytes_get_data (bytes, &file->size);
  file->trusted = trusted;

  if (file->size < sizeof (struct gvdb_header))
    goto invalid;

  header = (gpointer) file->data;

  if (header->signature[0] == GVDB_SIGNATURE0 &&
      header->signature[1] == GVDB_SIGNATURE1 &&
      guint32_from_le (header->version) == 0)
    file->byteswapped = FALSE;

  else if (header->signature[0] == GVDB_SWAPPED_SIGNATURE0 &&
           header->signature[1] == GVDB_SWAPPED_SIGNATURE1 &&
           guint32_from_le (header->version) == 0)
    file->byteswapped = TRUE;

  else
    goto invalid;

  gvdb_table_setup_root (file, &header->root);

  return file;

invalid:
  g_set_error_literal (error, G_FILE_ERROR, G_FILE_ERROR_INVAL, "invalid gvdb header");

  g_bytes_unref (file->bytes);

  g_slice_free (GvdbTable, file);

  return NULL;
}

GSettingsSchemaSource *
g_settings_schema_source_from_resource ( const char             *resource,
                                         GSettingsSchemaSource  *parent,
                                         gboolean                trusted,
                                         GError                **error)
{
  GSettingsSchemaSource *source;
  GvdbTable *table;
  GBytes *bytes;

  bytes = g_resources_lookup_data(resource, G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  if (bytes == NULL)
    return NULL;

  g_return_val_if_fail (bytes != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  table = gvdb_table_new_from_bytes (bytes, trusted, error);
  if (table == NULL)
    return NULL;

  source = g_slice_new0 (GSettingsSchemaSource);
  source->parent = parent ? g_settings_schema_source_ref (parent) : NULL;
  source->table = table;
  source->ref_count = 1;

  return source;
}
