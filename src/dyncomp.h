#include <stdbool.h>
#include <stdint.h>

typedef struct {
	float sample_rate;

	uint32_t n_channels;
	float    norm_input;

	float ratio;
	float p_rat;

	bool hold;

	float igain;
	float p_ign;
	float l_ign;

	float p_thr;
	float l_thr;

	float w_att;
	float w_rel;
	float t_att;
	float t_rel;

	float za1;
	float zr1;
	float zr2;

	bool  newg;
	float gmax;
	float gmin;

	float rms;
	float w_rms;
	float w_lpf;

} Dyncomp;

void Dyncomp_reset (Dyncomp* self);

void Dyncomp_set_ratio (Dyncomp* self, float r);

void Dyncomp_set_inputgain (Dyncomp* self, float g);

void Dyncomp_set_threshold (Dyncomp* self, float t);

void Dyncomp_set_hold (Dyncomp* self, bool hold);

void Dyncomp_set_attack (Dyncomp* self, float a);

void Dyncomp_set_release (Dyncomp* self, float r);

void Dyncomp_get_gain (Dyncomp* self, float* gmin, float* gmax, float* rms);

void Dyncomp_init (Dyncomp* self, float sample_rate, uint32_t n_channels);

void Dyncomp_process (Dyncomp* self, uint32_t n_samples, float *samples);
