#include "gateway.h"

static gboolean Gateway_heartbeat(gpointer user_data){
    Gateway *gateway = user_data;

    JsonObject *msg_obj = json_node_get_object(gateway->heartbeat_node);
    json_object_set_int_member(msg_obj, "d", gateway->seq);

    char *heartbeat_json = json_to_string(gateway->heartbeat_node, FALSE);
    soup_websocket_connection_send_text(gateway->connection, heartbeat_json);
    g_free(heartbeat_json);

    return TRUE;
}

static void Gateway_hello(JsonObject *data_obj, Gateway *gateway){
    gateway->heartbeat_interval = json_object_get_int_member(data_obj, "heartbeat_interval");
    gateway->heartbeat_source = g_timeout_source_new(gateway->heartbeat_interval);
    g_source_set_callback(gateway->heartbeat_source, G_SOURCE_FUNC(Gateway_heartbeat), (gpointer) gateway, NULL);
    g_source_attach(gateway->heartbeat_source, NULL);

    JsonNode *identify_node = json_from_string(IDENTITY_TEMPLATE, NULL);
    JsonObject *identify_obj = json_node_get_object(identify_node);
    JsonObject *identify_data_obj = json_object_get_object_member(identify_obj, "d");

    json_object_set_string_member(identify_data_obj, "token", gateway->token);

    char *identity_json = json_to_string(identify_node, FALSE);
    soup_websocket_connection_send_text(gateway->connection, identity_json);

    g_free(identity_json);
    json_node_free(identify_node);
}

static void Gateway_dispatch(JsonObject *data_obj, const char *t, Gateway *gateway){
    if(g_strcmp0(t, "READY") == 0){
        const char *session_id = json_object_get_string_member(data_obj, "session_id");
        gateway->session_id = g_strdup(session_id);
    }
}

static void Gateway_message_callback(SoupWebsocketConnection *self, int type, GBytes *message, gpointer user_data){
    Gateway *gateway = user_data;
    
    gsize size = g_bytes_get_size(message);
    const char *data = g_bytes_get_data(message, &size);

    GError *error;
    JsonNode *root = json_from_string(data, &error);
    if(gateway->connection == NULL){
        g_print("Gateway error: %s\n", error->message);
        g_error_free(error);
        return;
    }
    
    JsonObject *root_obj = json_node_get_object(root);

    if(json_object_has_member(root_obj, "seq"))
        gateway->seq = json_object_get_int_member(root_obj, "seq");

    gint64 opcode = json_object_get_int_member(root_obj, "op");
    if(opcode == 1 || opcode == 11) return json_node_free(root);

    JsonNode* data_node = json_object_get_member(root_obj, "d");
    if(!data_node) return json_node_free(root);
    if(json_node_get_node_type(data_node) != JSON_NODE_OBJECT) return json_node_free(root);
    JsonObject *data_obj = json_object_get_object_member(root_obj, "d");
    const char *t = json_object_get_string_member(root_obj, "t");

    if(opcode == 10) Gateway_hello(data_obj, gateway);
    if(opcode == 0) Gateway_dispatch(data_obj, t, gateway);

    gateway->message_cb(opcode, data_obj, t, gateway->cb_data);

    json_node_free(root);
}

static void Gateway_error_callback(SoupWebsocketConnection *self, GError *error, gpointer user_data){
    g_print("Gateway error: %s\n", error->message);
}

static void Gateway_closed_callback(SoupWebsocketConnection *self, gpointer user_data){
    Gateway *gateway = user_data;
    gateway->closed_cb(soup_websocket_connection_get_close_code(gateway->connection), gateway->cb_data);
}

static void Gateway_connected_cb(GObject *source_object, GAsyncResult *res, gpointer user_data){
    Gateway *gateway = user_data;

    GError *error;
    gateway->connection = soup_session_websocket_connect_finish(gateway->session, res, &error);
    if(gateway->connection == NULL){
        g_print("Error connecting to gateway: %s\n", error->message);
        g_error_free(error);
        gateway->closed_cb(-1, gateway->cb_data);
    }else{
        g_object_set(G_OBJECT(gateway->connection), "max-incoming-payload-size", MAX_WS_PAYLOAD, NULL); 
        g_signal_connect(gateway->connection, "message", G_CALLBACK(Gateway_message_callback), (gpointer) gateway);
        g_signal_connect(gateway->connection, "error", G_CALLBACK(Gateway_error_callback), (gpointer) gateway);
        g_signal_connect(gateway->connection, "closed", G_CALLBACK(Gateway_closed_callback), (gpointer) gateway);
    }
}

Gateway *Gateway_create(
    const char *token,
    Gateway_message_cb message_cb, Gateway_closed_cb closed_cb, gpointer cb_data
){
    Gateway *gateway = g_new0(Gateway, 1);
    gateway->token = g_strdup(token);
    gateway->cb_data = cb_data;
    gateway->message_cb = message_cb;
    gateway->closed_cb = closed_cb;
    gateway->heartbeat_node = json_from_string(HEARTBEAT_TEMPLATE, NULL);
    
    gateway->request = soup_message_new("GET", GATEWAY_ADDRESS);

    gateway->session = soup_session_new();
    g_object_set(G_OBJECT(gateway->session), "user-agent", USER_AGENT, NULL); 
    soup_session_websocket_connect_async(gateway->session, gateway->request, GATEWAY_ORIGIN, NULL, NULL, Gateway_connected_cb, (gpointer) gateway);

    return gateway;
}

void Gateway_send(Gateway *gateway, JsonNode *node){
    char *json_text = json_to_string(node, FALSE);
    soup_websocket_connection_send_text(gateway->connection, json_text);
    g_free(json_text);
}

void Gateway_close(Gateway *gateway){
    g_source_destroy(gateway->heartbeat_source);
    soup_websocket_connection_close(gateway->connection, SOUP_WEBSOCKET_CLOSE_NORMAL, NULL);
}

void Gateway_destroy(Gateway *gateway){
    g_free((gpointer) gateway->token);
    g_object_unref(gateway->session);
    g_object_unref(gateway->request);
    g_object_unref(gateway->connection);
    g_source_unref(gateway->heartbeat_source);
    json_node_free(gateway->heartbeat_node);
}
