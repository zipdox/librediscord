#include <glib.h>

// returns the size of the header
gssize read_rtp_header(
    unsigned char *buffer, int len,
    gboolean *padding, gboolean *extension, guint8 *cc,
    gboolean *marker, guint8 *pt, guint16 *sequence,
    guint32 *timestamp, guint32 *ssrc,
    guint32 *csrcs
);

// returns the size of the header written
gssize write_rtp_header(
    unsigned char *buffer,
    gboolean padding, gboolean extension, guint8 cc,
    gboolean marker, guint8 pt, guint16 sequence,
    guint32 timestamp, guint32 ssrc,
    guint32 *csrcs
);
