#include <rtaudio/rtaudio_c.h>
#include <glib.h>


typedef void (*AudioClient_data_cb)(float *out, float *in, float *processing, unsigned int n_frames, gpointer cb_data);

typedef struct {
    const char *api;
    const char *output;
    const char *input;
    unsigned int buffer_frames;
    char stereo;
    float *input_buffer;
    float *processing_buffer;
    rtaudio_t rtaudio;
    gboolean started;
    AudioClient_data_cb audio_cb;
    gpointer cb_data;
} AudioClient;

typedef struct {
    const char **inputs;
    unsigned int n_inputs;
    const char **outputs;
    unsigned int n_outputs;
} AudioDevices;

const char **AudioClient_get_supported_apis();

AudioClient *AudioClient_create(const char *api_name, AudioClient_data_cb audio_cb, gpointer cb_data);

AudioDevices *AudioClient_get_devices_for_api(const char *api_name);

void AudioDevices_clear(AudioDevices *devices);

gboolean AudioClient_connect(AudioClient *audio_client, const char *output_device, const char *input_device, unsigned int buffer_frames);

void AudioClient_destroy(AudioClient *audio_client);
