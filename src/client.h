#include <glib.h>

#include "gateway.h"
#include "voice_connection.h"
#include "audio.h"
#include "noisegate.h"
#include "dyncomp.h"

#define LOAD_PRIVATE_CHANNEL_TEMPLATE "{\"op\":13,\"d\":{\"channel_id\":\"\"}}"
#define VOICE_STATE_TEMPLATE "{\"op\":4,\"d\":{}}"

typedef void (*DiscordClient_ready_cb)(const char *username, const char *discriminator, const char *user_id, const char *avatar, gpointer cb_data);
typedef void (*DiscordClient_closed_cb)(gboolean auth_failed, gpointer cb_data);

typedef void (*DiscordClient_user_cb)(const char *username, const char *discriminator, const char *id, const char *avatar, gpointer cb_data);

typedef void (*DiscordClient_guild_cb)(const char *name, const char *id, const char *icon, gpointer cb_data);
typedef void (*DiscordClient_guild_channel_cb)(const char *guild_id, const char *id, const char *name, gpointer cb_data);

// type is 'a' for avatar and 'c' for channel_icon
typedef void (*DiscordClient_private_cb)(const char *name, const char *id, const char *icon_id, const char *icon, char type, gpointer cb_data);

typedef void (*DiscordClient_voice_status_cb)(const char *text, int progress, gpointer cb_data);

typedef void (*DiscordClient_voice_speaking_cb)(gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2], gpointer cb_data);

typedef void (*DiscordClient_voice_state_update)(
    const char *username, const char *discriminator, const char *avatar,
    const char *user_id, const char *session_id, const char *guild_id, const char *channel_id,
    gboolean self_video, gboolean self_mute, gboolean self_deaf, gboolean mute, gboolean deaf,
    gpointer cb_data
);

typedef void (*DiscordClient_alert_cb)(const char *alert, gpointer cb_data);

typedef struct {
    Gateway *gateway;
    const char *username;
    const char *discriminator;
    const char *user_id;
    const char *avatar;
    const char *session_id;
    GPtrArray *loaded_private_channels;

    DiscordClient_ready_cb ready_cb;
    DiscordClient_closed_cb closed_cb;

    DiscordClient_user_cb user_cb;

    DiscordClient_guild_cb guild_cb;
    DiscordClient_guild_channel_cb guild_channel_cb;

    DiscordClient_private_cb private_cb;

    DiscordClient_voice_state_update voice_state_cb;

    DiscordClient_alert_cb alert_cb;

    DiscordClient_voice_status_cb voice_status_cb;

    DiscordClient_voice_speaking_cb speaking_cb;

    JsonNode *voice_state_node;
    VoiceConnection *voice_connection;
    const char *voice_guild_id;
    const char *voice_channel_id;
    gboolean muted;
    gboolean deafened;
    gboolean video;

    GMutex voice_mutex;
    gboolean gate_enable;
    Noisegate *noisegate;
    gboolean dyncomp_enable;
    Dyncomp dyncomp;
    gboolean monitor;
    gboolean speaking;

    GSettings *settings;

    AudioClient *audio_client;

    gpointer cb_data;
} DiscordClient;

DiscordClient *DiscordClient_create(
    const char *token,
    DiscordClient_ready_cb ready_cb, DiscordClient_closed_cb closed_cb,
    DiscordClient_user_cb user_cb,
    DiscordClient_guild_cb guild_cb, DiscordClient_guild_channel_cb guild_channel_cb,
    DiscordClient_private_cb private_cb,
    DiscordClient_voice_state_update voice_state_cb,
    DiscordClient_alert_cb alert_cb, DiscordClient_voice_status_cb voice_status_cb,
    DiscordClient_voice_speaking_cb speaking_cb,
    GSettings *settings,
    gpointer cb_data
);

void DiscordClient_update_audio(DiscordClient *client);

void DiscordClient_mute_deafen_video(DiscordClient *client, gboolean muted, gboolean deafened, gboolean video);

void DiscordClient_set_channel(DiscordClient *client, const char *guild_id, const char *channel_id);

void DiscordClient_set_user_volume(DiscordClient *client, const char *user_id, float volume);

void DiscordClient_load_private_channel(DiscordClient *client, const char *id);

void DiscordClient_close(DiscordClient *client);
