#include <math.h>

#include "client.h"

static inline void guilds_channels(DiscordClient *client, const char *guild_id, JsonArray *channels_arr){
    guint channels_len = json_array_get_length(channels_arr);
    JsonObject *channel_obj;
    const char *id, *name;
    gint64 type;

    for(guint i = 0; i < channels_len; i++){
        channel_obj = json_array_get_object_element(channels_arr, i);
        type = json_object_get_int_member(channel_obj, "type");
        if(type != 2) continue;
        id = json_object_get_string_member(channel_obj, "id");
        name = json_object_get_string_member(channel_obj, "name");
        client->guild_channel_cb(guild_id, id, name, client->cb_data);
    }
}

static inline void READY_guilds(DiscordClient *client, JsonArray *guilds_arr){
    JsonObject *guild_obj;
    JsonArray *channels_arr;
    const char *name, *id, *icon;
    guint len = json_array_get_length(guilds_arr);
    for(guint i = 0; i < len; i++){
        guild_obj = json_array_get_object_element(guilds_arr, i);
        name = json_object_get_string_member(guild_obj, "name");
        id = json_object_get_string_member(guild_obj, "id");
        icon = json_object_get_string_member(guild_obj, "icon");
        client->guild_cb(name, id, icon, client->cb_data);

        channels_arr = json_object_get_array_member(guild_obj, "channels");
        guilds_channels(client, id, channels_arr);
    }
}

static inline JsonObject *get_user_from_array(JsonArray *users, const char *match_id){
    JsonObject *user;
    guint num_users = json_array_get_length(users);
    const char *user_id;
    for(guint i = 0; i < num_users; i++){
        user = json_array_get_object_element(users, i);
        user_id = json_object_get_string_member(user, "id");
        if(g_strcmp0(user_id, match_id) == 0)
            return user;
    }
    return NULL;
}

static inline void READY_users(DiscordClient *client, JsonArray *users){
    JsonObject *user;
    guint num_users = json_array_get_length(users);
    const char *username, *discriminator, *id, *avatar;
    for(guint i = 0; i < num_users; i++){
        user = json_array_get_object_element(users, i);
        username = json_object_get_string_member(user, "username");
        discriminator = json_object_get_string_member(user, "discriminator");
        id = json_object_get_string_member(user, "id");
        avatar = json_object_get_string_member(user, "avatar");
        client->user_cb(username, discriminator, id, avatar, client->cb_data);
    }
}

static inline char *make_group_name(JsonArray *recipient_ids, JsonArray *users){
    guint num_recipients = json_array_get_length(recipient_ids);
    const char **recipient_names = g_new0(const char*, num_recipients + 1);
    JsonObject *recipient_obj;
    const char *recipient_id;
    for(guint i = 0; i < num_recipients; i++){
        recipient_id = json_array_get_string_element(recipient_ids, i);
        recipient_obj = get_user_from_array(users, recipient_id);
        recipient_names[i] = json_object_get_string_member(recipient_obj, "username");
    }
    char *joined = g_strjoinv(", ", (char**) recipient_names);
    g_free(recipient_names);
    return joined;
}

typedef struct {
    const char *name, *id, *icon_id, *icon, *recipient_id;
    char type;
    guint64 last_message_id;
} PrivateChannel;

static gint PrivateChannel_compare(gconstpointer a, gconstpointer b, gpointer user_data){
    const PrivateChannel *ch_a = a;
    const PrivateChannel *ch_b = b;
    if(ch_a->last_message_id > ch_b->last_message_id){
        return -1;
    }else if(ch_a->last_message_id < ch_b->last_message_id){
        return 1;
    }
    return 0;
}

static inline void READY_private_channels(DiscordClient *client, JsonArray *private_channels_arr, JsonArray *users){
    JsonObject *private_channel_obj;
    JsonArray *recipient_ids;
    JsonObject *recipient_obj;
    guint len = json_array_get_length(private_channels_arr);
    PrivateChannel *sorted_channels = g_new0(PrivateChannel, len);
    for(guint i = 0; i < len; i++){
        private_channel_obj = json_array_get_object_element(private_channels_arr, i);
        sorted_channels[i].name = NULL;
        recipient_ids = json_object_get_array_member(private_channel_obj, "recipient_ids");

        sorted_channels[i].id = json_object_get_string_member(private_channel_obj, "id");

        const char *last_message_id = json_object_get_string_member_with_default(private_channel_obj, "last_message_id", "0");
        sorted_channels[i].last_message_id = g_ascii_strtoull(last_message_id, NULL, 10);
        sorted_channels[i].icon_id = sorted_channels[i].id;
        sorted_channels[i].icon = NULL;

        sorted_channels[i].type = (json_object_get_int_member(private_channel_obj, "type") == 3) ? 'c' : 'a';

        if(sorted_channels[i].type == 'c'){
            if(json_object_has_member(private_channel_obj, "icon"))
                sorted_channels[i].icon = json_object_get_string_member(private_channel_obj, "icon");

            if(json_object_has_member(private_channel_obj, "name")){
                sorted_channels[i].name = json_object_get_string_member(private_channel_obj, "name");
                if(sorted_channels[i].name){
                    sorted_channels[i].name = g_strdup(sorted_channels[i].name);
                }else
                    sorted_channels[i].name = make_group_name(recipient_ids, users);
            }else
                sorted_channels[i].name = make_group_name(recipient_ids, users);
        }else{
            sorted_channels[i].recipient_id = json_array_get_string_element(recipient_ids, 0);
            sorted_channels[i].icon_id = sorted_channels[i].recipient_id;
            recipient_obj = get_user_from_array(users, sorted_channels[i].recipient_id);

            sorted_channels[i].icon = json_object_get_string_member(recipient_obj, "avatar");
            sorted_channels[i].name = g_strdup(json_object_get_string_member(recipient_obj, "username"));
        }
    }

    g_qsort_with_data((gconstpointer) sorted_channels, len, sizeof(PrivateChannel), PrivateChannel_compare, NULL);
    
    for(guint i = 0; i < len; i++){
        client->private_cb(
            sorted_channels[i].name,
            sorted_channels[i].id,
            sorted_channels[i].icon_id,
            sorted_channels[i].icon,
            sorted_channels[i].type,
            client->cb_data
        );
    }
    g_free(sorted_channels);
}

static inline void READY(DiscordClient *client, JsonObject *data_obj){
    JsonObject *user = json_object_get_object_member(data_obj, "user");
    const char *username = json_object_get_string_member(user, "username");
    const char *discriminator = json_object_get_string_member(user, "discriminator");
    const char *user_id = json_object_get_string_member(user, "id");
    const char *avatar = json_object_get_string_member(user, "avatar");
    const char *session_id = json_object_get_string_member(data_obj, "session_id");

    client->username = g_strdup(username);
    client->discriminator = g_strdup(discriminator);
    client->user_id = g_strdup(user_id);
    client->avatar = g_strdup(avatar);
    client->session_id = g_strdup(session_id);
    
    client->ready_cb(username, discriminator, user_id, avatar, client->cb_data);

    JsonArray *guilds_arr = json_object_get_array_member(data_obj, "guilds");
    READY_guilds(client, guilds_arr);
    
    JsonArray *users = json_object_get_array_member(data_obj, "users");
    READY_users(client, users);

    JsonArray *private_channels_arr = json_object_get_array_member(data_obj, "private_channels");
    READY_private_channels(client, private_channels_arr, users);
}

static inline void READY_SUPPLEMENTAL(DiscordClient *client, JsonObject *data_obj){
    JsonArray *guilds_arr = json_object_get_array_member(data_obj, "guilds");
    guint len = json_array_get_length(guilds_arr);
    JsonObject *guild_obj, *voice_state_obj;
    JsonArray *voice_states_arr;
    guint voice_states_len;
    const char *user_id, *session_id, *guild_id, *channel_id;
    gboolean self_video, self_mute, self_deaf, mute, deaf;
    for(guint i = 0; i < len; i++){
        guild_obj = json_array_get_object_element(guilds_arr, i);
        voice_states_arr = json_object_get_array_member(guild_obj, "voice_states");
        voice_states_len = json_array_get_length(voice_states_arr);
        if(voice_states_len == 0) continue;
        guild_id = json_object_get_string_member(guild_obj, "id");
        for(guint j = 0; j < voice_states_len; j++){
            voice_state_obj = json_array_get_object_element(voice_states_arr, j);
            user_id = json_object_get_string_member(voice_state_obj, "user_id");
            session_id = json_object_get_string_member(voice_state_obj, "session_id");
            channel_id = json_object_get_string_member(voice_state_obj, "channel_id");
            self_video = json_object_get_boolean_member(voice_state_obj, "self_video");
            self_mute = json_object_get_boolean_member(voice_state_obj, "self_mute");
            self_deaf = json_object_get_boolean_member(voice_state_obj, "self_deaf");
            mute = json_object_get_boolean_member(voice_state_obj, "mute");
            deaf = json_object_get_boolean_member(voice_state_obj, "deaf");
            client->voice_state_cb(NULL, NULL, NULL, user_id, session_id, guild_id, channel_id, self_video, self_mute, self_deaf, mute, deaf, client->cb_data);
        }
    }
}

static inline void VOICE_STATE_UPDATE(DiscordClient *client, JsonObject *voice_state_obj){
    JsonObject *member_obj, *user_obj;
    const char *username = NULL, *discriminator = NULL, *avatar = NULL, *user_id, *session_id, *guild_id, *channel_id;
    gboolean self_video, self_mute, self_deaf, mute, deaf;
    user_id = json_object_get_string_member(voice_state_obj, "user_id");
    session_id = json_object_get_string_member(voice_state_obj, "session_id");
    guild_id = json_object_get_string_member(voice_state_obj, "guild_id");
    channel_id = json_object_get_string_member(voice_state_obj, "channel_id");
    self_video = json_object_get_boolean_member(voice_state_obj, "self_video");
    self_mute = json_object_get_boolean_member(voice_state_obj, "self_mute");
    self_deaf = json_object_get_boolean_member(voice_state_obj, "self_deaf");
    mute = json_object_get_boolean_member(voice_state_obj, "mute");
    deaf = json_object_get_boolean_member(voice_state_obj, "deaf");
    if(json_object_has_member(voice_state_obj, "member")){
        member_obj = json_object_get_object_member(voice_state_obj, "member");
        user_obj = json_object_get_object_member(member_obj, "user");
        username = json_object_get_string_member(user_obj, "username");
        discriminator = json_object_get_string_member(user_obj, "discriminator");
        avatar = json_object_get_string_member(user_obj, "avatar");
    }
    client->voice_state_cb(username, discriminator, avatar, user_id, session_id, guild_id, channel_id, self_video, self_mute, self_deaf, mute, deaf, client->cb_data);
    
    if(g_strcmp0(user_id, client->user_id) == 0){
        if(g_strcmp0(session_id, client->session_id) == 0){
            if(client->voice_guild_id) g_free((gpointer) client->voice_guild_id);
            client->voice_guild_id = guild_id ? g_strdup(guild_id) : NULL;
            if(client->voice_channel_id) g_free((gpointer) client->voice_channel_id);
            client->voice_channel_id = channel_id ? g_strdup(channel_id) : NULL;
            if(client->voice_guild_id == NULL && client->voice_channel_id == NULL && client->voice_connection){
                g_mutex_lock(&client->voice_mutex);
                VoiceConnection_close(client->voice_connection);
                client->voice_connection = NULL;
                g_mutex_unlock(&client->voice_mutex);
            }
        }else{
            if(client->voice_connection){
                g_mutex_lock(&client->voice_mutex);
                VoiceConnection_close(client->voice_connection);
                client->voice_connection = NULL;
                if(client->voice_guild_id) g_free((gpointer) client->voice_guild_id);
                if(client->voice_channel_id) g_free((gpointer) client->voice_channel_id);
                g_mutex_unlock(&client->voice_mutex);
            }
        }
    }
}

static inline void CALL_CREATE(DiscordClient *client, JsonObject *data_obj){
    JsonArray *voice_states_arr = json_object_get_array_member(data_obj, "voice_states");
    JsonObject *voice_state_obj;
    guint voice_states_len = json_array_get_length(voice_states_arr);
    const char *user_id, *session_id, *channel_id;
    gboolean self_video, self_mute, self_deaf, mute, deaf;
    for(guint i = 0; i < voice_states_len; i++){
        voice_state_obj = json_array_get_object_element(voice_states_arr, i);
        user_id = json_object_get_string_member(voice_state_obj, "user_id");
        session_id = json_object_get_string_member(voice_state_obj, "session_id");
        channel_id = json_object_get_string_member(voice_state_obj, "channel_id");
        self_video = json_object_get_boolean_member(voice_state_obj, "self_video");
        self_mute = json_object_get_boolean_member(voice_state_obj, "self_mute");
        self_deaf = json_object_get_boolean_member(voice_state_obj, "self_deaf");
        mute = json_object_get_boolean_member(voice_state_obj, "mute");
        deaf = json_object_get_boolean_member(voice_state_obj, "deaf");
        client->voice_state_cb(NULL, NULL, NULL, user_id, session_id, NULL, channel_id, self_video, self_mute, self_deaf, mute, deaf, client->cb_data);
    }
}

static void voice_closed(gpointer voice_ptr, gpointer cb_data){
    DiscordClient *client = cb_data;
    if(client->voice_connection == voice_ptr) client->voice_connection = NULL;
}

const char *status_messages[] = {"Connecting to socket...", "Connected"};
static void voice_stage_cb(int stage, gpointer cb_data){
    DiscordClient *client = cb_data;

    client->voice_status_cb(status_messages[stage - 1], stage, client->cb_data);
}

static void voice_speaking_cb(gboolean private_channel, guint64 server_id, guint64 user_id, float volume[2], gpointer cb_data){
    DiscordClient *client = cb_data;
    client->speaking_cb(private_channel, server_id, user_id, volume, client->cb_data);
}

static inline void VOICE_SERVER_UPDATE(DiscordClient *client, JsonObject *data_obj){
    if(client->voice_connection){
        g_mutex_lock(&client->voice_mutex);
        VoiceConnection_close(client->voice_connection);
        client->voice_connection = NULL;
        g_mutex_unlock(&client->voice_mutex);
    }

    const char *token = json_object_get_string_member(data_obj, "token");

    const char *server_id = json_object_get_string_member(data_obj, "guild_id");
    gboolean private_channel = FALSE;
    if(!server_id){
        server_id = json_object_get_string_member(data_obj, "channel_id");
        private_channel = TRUE;
    }

    const char *endpoint = json_object_get_string_member(data_obj, "endpoint");

    guint bitrate = g_settings_get_uint(client->settings, "bitrate");
    guint frame_size = g_settings_get_uint(client->settings, "frame-size");

    VoiceConnection *voice_connection = VoiceConnection_create(endpoint, server_id, client->user_id, client->session_id, token, voice_stage_cb, voice_closed, voice_speaking_cb, private_channel, &client->voice_mutex, bitrate, frame_size, client);
    client->speaking = FALSE;

    g_mutex_lock(&client->voice_mutex);
    client->voice_connection = voice_connection;
    g_mutex_unlock(&client->voice_mutex);
}

static void msg_cb(gint64 opcode, JsonObject *data_obj, const char *type, gpointer cb_data){
    DiscordClient *client = cb_data;
    if(g_strcmp0(type, "READY") == 0) READY(client, data_obj);
    if(g_strcmp0(type, "READY_SUPPLEMENTAL") == 0) READY_SUPPLEMENTAL(client, data_obj);
    if(g_strcmp0(type, "VOICE_STATE_UPDATE") == 0) VOICE_STATE_UPDATE(client, data_obj);
    if(g_strcmp0(type, "CALL_CREATE") == 0) CALL_CREATE(client, data_obj);
    if(g_strcmp0(type, "VOICE_SERVER_UPDATE") == 0) VOICE_SERVER_UPDATE(client, data_obj);
}

static void gateway_closed_cb(int code, gpointer cb_data){
    DiscordClient *client = cb_data;
    Gateway_destroy(client->gateway);
    client->closed_cb((code == 4004), client->cb_data);

    guint len = client->loaded_private_channels->len;
    gpointer loaded_channel;
    for(guint i = 0; i < len; i++){
        loaded_channel = g_ptr_array_index(client->loaded_private_channels, i);
        g_free(loaded_channel);
    }
    g_ptr_array_free(client->loaded_private_channels, TRUE);

    json_node_free(client->voice_state_node);

    g_free((gpointer) client->username);
    g_free((gpointer) client->discriminator);
    g_free((gpointer) client->user_id);
    g_free((gpointer) client->avatar);
    g_free((gpointer) client->session_id);

    if(client->audio_client) AudioClient_destroy(client->audio_client);
    Noisegate_destroy(client->noisegate);

    g_free(client);
}

static void DiscordClient_process_audio(float *out, float *in, float *processing, unsigned int n_frames, gpointer cb_data){
    DiscordClient *client = cb_data;
    gboolean sound = TRUE;

    if(client->gate_enable){
        sound = Noisegate_process(client->noisegate, processing, in, n_frames);
    }else
        memcpy(processing, in, sizeof(float) * 2 * n_frames);

    if(client->dyncomp_enable)
        Dyncomp_process(&client->dyncomp, n_frames, processing);
    if(client->monitor)
        memcpy(out, processing, sizeof(float) * 2 * n_frames);
    else
        memset(out, 0, 2 * n_frames * sizeof(float));

    g_mutex_lock(&client->voice_mutex);

    if(client->voice_connection) if(client->voice_connection->ready){
        if(!client->muted && sound){
            if(!client->speaking){
                client->speaking = TRUE;
                VoiceConnection_speaking(client->voice_connection, TRUE);
            }
            
            VoiceConnection_send_samples(client->voice_connection, processing, n_frames);
        }else{
            if(client->speaking){
                client->speaking = FALSE;
                VoiceConnection_flush_samples(client->voice_connection);
                VoiceConnection_speaking(client->voice_connection, FALSE);
            }
        }
        VoiceConnection_get_samples(client->voice_connection, out, n_frames, client->deafened ? 0.0 : 1.0);
    }

    g_mutex_unlock(&client->voice_mutex);
}

DiscordClient *DiscordClient_create(
    const char *token,
    DiscordClient_ready_cb ready_cb, DiscordClient_closed_cb closed_cb,
    DiscordClient_user_cb user_cb,
    DiscordClient_guild_cb guild_cb, DiscordClient_guild_channel_cb guild_channel_cb,
    DiscordClient_private_cb private_cb,
    DiscordClient_voice_state_update voice_state_cb,
    DiscordClient_alert_cb alert_cb, DiscordClient_voice_status_cb voice_status_cb,
    DiscordClient_voice_speaking_cb speaking_cb,
    GSettings *settings,
    gpointer cb_data
){
    DiscordClient *client = g_new0(DiscordClient, 1);
    client->ready_cb = ready_cb;
    client->closed_cb = closed_cb;
    client->user_cb = user_cb;
    client->guild_cb = guild_cb;
    client->guild_channel_cb = guild_channel_cb;
    client->private_cb = private_cb;
    client->voice_state_cb = voice_state_cb;
    client->alert_cb = alert_cb;
    client->voice_status_cb = voice_status_cb;
    client->speaking_cb = speaking_cb;
    client->cb_data = cb_data;
    client->gateway = Gateway_create(token, msg_cb, gateway_closed_cb, client);
    client->loaded_private_channels = g_ptr_array_new();
    client->settings = settings;

    client->voice_state_node = json_from_string(VOICE_STATE_TEMPLATE, NULL);

    const char *audio_api = g_settings_get_string(client->settings, "audio-api");
    client->audio_client = AudioClient_create(audio_api, DiscordClient_process_audio, client);
    g_free((gpointer) audio_api);

    double threshold = g_settings_get_double(settings, "noisegate-threshold");
    double hold = g_settings_get_double(settings, "noisegate-hold");
    double decay = g_settings_get_double(settings, "noisegate-decay");

    client->noisegate = Noisegate_create(threshold, hold, decay);
    client->gate_enable = g_settings_get_boolean(settings, "noisegate-enable");
    Dyncomp_init(&client->dyncomp, 48000, 2);
    client->dyncomp_enable = g_settings_get_boolean(settings, "compressor-enable");

    if(client->audio_client){
        DiscordClient_update_audio(client);
    }else
        alert_cb("Unable to initialize the audio system.\nCheck the audio section in the settings.", cb_data);

    return client;
}

void DiscordClient_update_audio(DiscordClient *client){
    double threshold = g_settings_get_double(client->settings, "noisegate-threshold");
    double hold = g_settings_get_double(client->settings, "noisegate-hold");
    double decay = g_settings_get_double(client->settings, "noisegate-decay");
    Noisegate_set_params(client->noisegate, threshold, hold, decay);
    client->gate_enable = g_settings_get_boolean(client->settings, "noisegate-enable");

    client->dyncomp_enable = g_settings_get_boolean(client->settings, "compressor-enable");
    double ratio = g_settings_get_double(client->settings, "compressor-ratio");
    double gain = g_settings_get_double(client->settings, "compressor-gain");
    threshold = g_settings_get_double(client->settings, "compressor-threshold");
    double attack = g_settings_get_double(client->settings, "compressor-attack");
    double release = g_settings_get_double(client->settings, "compressor-release");
    Dyncomp_set_ratio(&client->dyncomp, ratio);
    Dyncomp_set_inputgain(&client->dyncomp, gain);
    Dyncomp_set_threshold(&client->dyncomp, threshold);
    Dyncomp_set_attack(&client->dyncomp, attack);
    Dyncomp_set_release(&client->dyncomp, release);

    const char *audio_api = g_settings_get_string(client->settings, "audio-api");
    if(!client->audio_client){
        client->audio_client = AudioClient_create(audio_api, DiscordClient_process_audio, client);
    }else if(g_strcmp0(audio_api, client->audio_client->api) != 0){
        if(client->audio_client) AudioClient_destroy(client->audio_client);
        client->audio_client = AudioClient_create(audio_api, DiscordClient_process_audio, client);
    }

    if(!client->audio_client){
        client->alert_cb("Unable to initialize the audio system.\nCheck the audio section in the settings.", client->cb_data);
        return g_free((gpointer) audio_api);
    }

    const char *output_device = g_settings_get_string(client->settings, "audio-output-device");
    const char *input_device = g_settings_get_string(client->settings, "audio-input-device");
    unsigned int buffer_frames = g_settings_get_uint(client->settings, "buffer-size");

    gboolean connect_success;
    if((client->audio_client->buffer_frames != buffer_frames && buffer_frames != 0) || g_strcmp0(output_device, client->audio_client->output) != 0 || g_strcmp0(input_device, client->audio_client->input) != 0){
        connect_success = AudioClient_connect(client->audio_client, output_device, input_device, buffer_frames);
        if(!connect_success)
            client->alert_cb("Unable to connect the audio device(s). Set them in the settings.", client->cb_data);
    }

    const char *stereo_mode = g_settings_get_string(client->settings, "stereo-mode");
    client->audio_client->stereo = *stereo_mode;

    g_free((gpointer) audio_api);
    g_free((gpointer) output_device);
    g_free((gpointer) input_device);
    g_free((gpointer) stereo_mode);

    guint bitrate, frame_size;
    if(client->voice_connection){
        bitrate = g_settings_get_uint(client->settings, "bitrate");
        frame_size = g_settings_get_uint(client->settings, "frame-size");
        VoiceConnection_set_birate_frame_size(client->voice_connection, bitrate, frame_size);
    }

    client->monitor = g_settings_get_boolean(client->settings, "monitor");
}

static void DiscordClient_send_voice_state(DiscordClient *client){
    JsonObject *root_obj = json_node_get_object(client->voice_state_node);
    JsonObject *data_obj = json_object_get_object_member(root_obj, "d");

    json_object_set_boolean_member(data_obj, "self_mute", client->muted);
    json_object_set_boolean_member(data_obj, "self_deaf", client->deafened);
    json_object_set_boolean_member(data_obj, "self_video", client->video);

    if(client->voice_guild_id)
        json_object_set_string_member(data_obj, "guild_id", client->voice_guild_id);
    else
        json_object_set_null_member(data_obj, "guild_id");
    if(client->voice_channel_id)
        json_object_set_string_member(data_obj, "channel_id", client->voice_channel_id);
    else
        json_object_set_null_member(data_obj, "channel_id");

    Gateway_send(client->gateway, client->voice_state_node);
}

void DiscordClient_mute_deafen_video(DiscordClient *client, gboolean muted, gboolean deafened, gboolean video){
    if(client->muted == muted && client->deafened == deafened) return;
    client->muted = muted;
    client->deafened = deafened;
    DiscordClient_send_voice_state(client);
}

void DiscordClient_set_channel(DiscordClient *client, const char *guild_id, const char *channel_id){
    if(g_strcmp0(client->voice_guild_id, guild_id) == 0 && g_strcmp0(client->voice_channel_id, channel_id) == 0) return;

    if(client->voice_guild_id) g_free((gpointer) client->voice_guild_id);
    client->voice_guild_id = guild_id ? g_strdup(guild_id) : NULL;
    if(client->voice_channel_id) g_free((gpointer) client->voice_channel_id);
    client->voice_channel_id = channel_id ? g_strdup(channel_id) : NULL;
    DiscordClient_send_voice_state(client);
    client->voice_status_cb("Connecting to gateway...", 0, client->cb_data);
}

void DiscordClient_set_user_volume(DiscordClient *client, const char *user_id, float volume){
    if(!client->voice_connection) return;
    float calculated = powf(volume/100.0, 2);
    g_mutex_lock(&client->voice_mutex);
    guint64 uid = g_ascii_strtoull(user_id, NULL, 10);
    VoiceReceiver_set_volume(client->voice_connection->receiver, uid, calculated);
    g_mutex_unlock(&client->voice_mutex);
}

static inline gboolean check_private_channel_loaded(GPtrArray *loaded_channels, const char *id){
    guint len = loaded_channels->len;
    const char *loaded_channel;
    for(guint i = 0; i < len; i++){
        loaded_channel = g_ptr_array_index(loaded_channels, i);
        if(g_strcmp0(loaded_channel, id) == 0) return TRUE;
    }
    return FALSE;
}

void DiscordClient_load_private_channel(DiscordClient *client, const char *id){
    if(check_private_channel_loaded(client->loaded_private_channels, id)) return;
    const char *id_dup = g_strdup(id);
    g_ptr_array_add(client->loaded_private_channels, (gpointer) id_dup);
    JsonNode *update_node = json_from_string(LOAD_PRIVATE_CHANNEL_TEMPLATE, NULL);
    JsonObject *update_obj = json_node_get_object(update_node);
    JsonObject *data_obj = json_object_get_object_member(update_obj, "d");
    json_object_set_string_member(data_obj, "channel_id", id_dup);
    Gateway_send(client->gateway, update_node);
    json_node_free(update_node);
}

void DiscordClient_close(DiscordClient *client){
    Gateway_close(client->gateway);
}
