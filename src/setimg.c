#include "setimg.h"

static void set_img_cb_anim(GObject *source_object, GAsyncResult *res, gpointer user_data){
    GtkImage *image = user_data;

    GError *error = NULL;
    GdkPixbufAnimation *buf = gdk_pixbuf_animation_new_from_stream_finish(res, &error);
    g_object_unref(source_object);
    if(!buf){
        g_print("Failed to load image: %s\n", error->message);
        g_error_free(error);
        return;
    }

    gtk_image_set_from_animation(image, buf);
    g_object_unref(buf);
}

static void set_img_cb(GObject *source_object, GAsyncResult *res, gpointer user_data){
    GtkImage *image = user_data;

    GError *error = NULL;
    GdkPixbuf *buf = gdk_pixbuf_new_from_stream_finish(res, &error);
    g_object_unref(source_object);
    if(!buf){
        g_print("Failed to load image: %s\n", error->message);
        g_error_free(error);
        return;
    }

    gtk_image_set_from_pixbuf(image, buf);
    g_object_unref(buf);
}

static void set_img_cache_cb(GInputStream *img_stream, gboolean animated, gpointer user_data){
    if(animated){
        gdk_pixbuf_animation_new_from_stream_async(img_stream, NULL, set_img_cb_anim, user_data);
    }else{
        gdk_pixbuf_new_from_stream_async(img_stream, NULL, set_img_cb, user_data);
    }
}

void set_img(ImgCache *cache, GtkImage *img, char type, const char *id, const char *image, unsigned int size){
    if(!image){
        GdkPixbuf *buf = gdk_pixbuf_new_from_resource_at_scale(type == 'a' ? "/user.png" : "/group.png", size, size, FALSE, NULL);
        gtk_image_set_from_pixbuf(img, buf);
        g_object_unref(buf);
        return;
    }
    ImgCache_fetch(cache, type, id, image, size, set_img_cache_cb, img);
}
