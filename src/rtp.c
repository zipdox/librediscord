#include <glib.h>

// returns the size of the header
gssize read_rtp_header(
    unsigned char *buffer, int len,
    gboolean *padding, gboolean *extension, guint8 *cc,
    gboolean *marker, guint8 *pt, guint16 *sequence,
    guint32 *timestamp, guint32 *ssrc,
    guint32 *csrcs
){
    guint8 version = buffer[0] >> 6;
    if(version != 2) return 0;
    *padding = (buffer[0] >> 5) & 1;
    *extension = (buffer[0] >> 4) & 1;
    *cc = (buffer[0] & 0x0F);
    if((4 * (*cc) + 12) >= len) return 0;
    *marker = buffer[1] >> 7;
    *pt = (buffer[1] & 0x7F);

    *sequence = (buffer[2] << 8) | buffer[3];
    *timestamp = (buffer[4] << 24) | (buffer[5] << 16) | (buffer[6] << 8) | buffer[7];
    *ssrc = (buffer[8] << 24) | (buffer[9] << 16) | (buffer[10] << 8) | buffer[11];

    for(guint8 i = 0; i < *cc; i++){
        csrcs[i] = (buffer[4 * i + 12] << 24) | (buffer[4 * i + 13] << 16) | (buffer[4 * i + 14] << 8) | buffer[4 * i + 15];
    }

    return 4 * (*cc) + 12;
}

// returns the size of the header written
gssize write_rtp_header(
    unsigned char *buffer,
    gboolean padding, gboolean extension, guint8 cc,
    gboolean marker, guint8 pt, guint16 sequence,
    guint32 timestamp, guint32 ssrc,
    guint32 *csrcs
){
    buffer[0] = 0x80 | (padding << 5) | (extension << 4) | (cc & 0x0F);
    buffer[1] = (marker << 7) | (pt & 0x7F);

    buffer[2] = sequence >> 8  & 0xFF;
    buffer[3] = sequence & 0xFF;

    buffer[4] = timestamp >> 24 & 0xFF;
    buffer[5] = timestamp >> 16 & 0xFF;
    buffer[6] = timestamp >> 8 & 0xFF;
    buffer[7] = timestamp & 0xFF;

    buffer[8] = ssrc >> 24 & 0xFF;
    buffer[9] = ssrc >> 16 & 0xFF;
    buffer[10] = ssrc >> 8 & 0xFF;
    buffer[11] = ssrc & 0xFF;

    if(csrcs) for(guint8 i = 0; i < cc; i++){
        buffer[4 * i + 12] = csrcs[i] >> 24 & 0xFF;
        buffer[4 * i + 13] = csrcs[i] >> 16 & 0xFF;
        buffer[4 * i + 14] = csrcs[i] >> 8 & 0xFF;
        buffer[4 * i + 15] = csrcs[i] & 0xFF;
    }

    return 4 * cc + 12;
}
