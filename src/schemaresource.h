#include <glib.h>

typedef struct _GvdbTable GvdbTable;

typedef struct _GSettingsSchemaSource GSettingsSchemaSource;

GSettingsSchemaSource *
g_settings_schema_source_from_resource ( const char             *resource,
                                         GSettingsSchemaSource  *parent,
                                         gboolean                trusted,
                                         GError                **error);
