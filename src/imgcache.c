#include "imgcache.h"
#include "libsoup/soup-session.h"

ImgCache *ImgCache_create(const char *cache_dir){
    ImgCache *cache = g_new0(ImgCache, 1);

#ifdef USE_CACHE
    cache->cache = soup_cache_new(cache_dir, SOUP_CACHE_SINGLE_USER);
    soup_cache_set_max_size(cache->cache, 1 << 30);
    soup_cache_load(cache->cache);
    cache->session = soup_session_new_with_options(SOUP_SESSION_ADD_FEATURE, cache->cache, SOUP_SESSION_USER_AGENT, USER_AGENT, NULL);
#else
    cache->session = soup_session_new_with_options(SOUP_SESSION_USER_AGENT, USER_AGENT, NULL);
#endif

    return cache;
}

typedef struct {
    ImgCache *cache;
    ImgCache_cb callback;
    gpointer user_data;
    gboolean animated;
} ImgCacheCBData;

static void response_callback(GObject *source_object, GAsyncResult *res, gpointer user_data){
    ImgCacheCBData *data = user_data;

    GError *error = NULL;
    GInputStream *read_stream = soup_session_send_finish((SoupSession*) source_object, res, &error);
    if(read_stream == NULL){
        g_print("Error getting image: %s\n", error->message);
        g_error_free(error);
        g_object_unref(source_object);
        return;
    }

    data->callback(read_stream, data->animated, data->user_data);
    g_free(data);
}

void ImgCache_fetch(ImgCache *cache, char type, const char *id, const char *image, unsigned int size, ImgCache_cb callback, gpointer user_data){
    ImgCacheCBData *data = g_new0(ImgCacheCBData, 1);
    data->cache = cache;
    data->callback = callback;
    data->user_data = user_data;
    data->animated = g_str_has_prefix(image, "a_");
    const char *ext = data->animated ? "gif" : "png";
    const char *url;
    switch(type){
        case 'a':
            url = g_strdup_printf("https://cdn.discordapp.com/avatars/%s/%s.%s?size=%u", id, image, ext, size);
            break;
        case 'c':
            url = g_strdup_printf("https://cdn.discordapp.com/channel-icons/%s/%s.%s?size=%u", id, image, ext, size);
            break;
        case 'i':
            url = g_strdup_printf("https://cdn.discordapp.com/icons/%s/%s.%s?size=%u", id, image, ext, size);
            break;
        default:
            return;
    }
    SoupMessage *msg = soup_message_new(SOUP_METHOD_GET, url);
    g_free((gpointer) url);
    soup_session_send_async(cache->session, msg, NULL, response_callback, data);
}

void ImgCache_destroy(ImgCache *cache){
#ifdef USE_CACHE
    soup_cache_flush(cache->cache);
    soup_cache_dump(cache->cache);
    g_object_unref(cache->cache);
#endif
    g_object_unref(cache->session);
    g_free(cache);
}
