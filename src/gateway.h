#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

#include "constants.h"

#define IDENTITY_TEMPLATE "{ \"op\": 2, \"d\": { \"capabilities\": 1021, \"properties\": { \"os\": \"Linux\", \"browser\": \"Discord Client\", \"release_channel\": \"stable\", \"client_version\": \"0.0.20\", \"os_arch\": \"x64\", \"system_locale\": \"en-US\" } } }"
#define HEARTBEAT_TEMPLATE "{ \"op\": 1, \"d\": 0 }"
#define VOICE_STATE_TEMPLATE "{\"op\":4,\"d\":{}}"

typedef void (*Gateway_message_cb)(gint64 opcode, JsonObject *data_obj, const char *type, gpointer cb_data);

typedef void (*Gateway_closed_cb)(int code, gpointer cb_data);

typedef struct {
    const char *token;
    const char *session_id;
    guint64 seq;
    gpointer cb_data;
    SoupSession *session;
    SoupMessage *request;
    SoupWebsocketConnection *connection;
    Gateway_message_cb message_cb;
    Gateway_closed_cb closed_cb;
    guint64 heartbeat_interval;
    GSource *heartbeat_source;
    JsonNode *heartbeat_node;
} Gateway;

Gateway *Gateway_create(
    const char *token,
    Gateway_message_cb message_cb, Gateway_closed_cb closed_cb, gpointer cb_data
);

void Gateway_send(Gateway *gateway, JsonNode *node);

void Gateway_close(Gateway *gateway);

void Gateway_destroy(Gateway *gateway);
