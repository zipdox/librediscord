#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dyncomp.h"

void Dyncomp_reset (Dyncomp* self)
{
	self->za1  = 0.f;
	self->zr1  = 0.f;
	self->zr2  = 0.f;
	self->rms  = 0.f;
	self->gmin = 0.f;
	self->gmax = 0.f;
	self->newg = true;
}

void Dyncomp_set_ratio (Dyncomp* self, float r)
{
	self->p_rat = 0.5f * r;
}

void Dyncomp_set_inputgain (Dyncomp* self, float g)
{
	if (g == self->l_ign) {
		return;
	}
	self->l_ign = g;
#ifdef __USE_GNU
	self->p_ign = exp10f (0.05f * g);
#else
	self->p_ign = powf (10.0f, 0.05f * g);
#endif
}

void Dyncomp_set_threshold (Dyncomp* self, float t)
{
	if (t == self->l_thr) {
		return;
	}
	self->l_thr = t;
	/* Note that this is signal-power, hence .5 * 10^(x/10) */
#ifdef __USE_GNU
	self->p_thr = 0.5f * exp10f (0.1f * t);
#else
	self->p_thr = 0.5f * powf (10.0f, 0.1f * t);
#endif
}

void Dyncomp_set_hold (Dyncomp* self, bool hold)
{
	self->hold = hold;
}

void Dyncomp_set_attack (Dyncomp* self, float a)
{
	if (a == self->t_att) {
		return;
	}
	self->t_att = a;
	self->w_att = 0.5f / (self->sample_rate * a);
}

void Dyncomp_set_release (Dyncomp* self, float r)
{
	if (r == self->t_rel) {
		return;
	}
	self->t_rel = r;
	self->w_rel = 3.5f / (self->sample_rate * r);
}

void Dyncomp_get_gain (Dyncomp* self, float* gmin, float* gmax, float* rms)
{
	*gmin = self->gmin * 8.68589f; /* 20 / log(10) */
	*gmax = self->gmax * 8.68589f;
	if (self->rms > 1e-8f) {
		*rms = 10.f * log10f (2.f * self->rms);
	} else {
		*rms = -80;
	}
	self->newg = true;
}

void Dyncomp_init (Dyncomp* self, float sample_rate, uint32_t n_channels)
{
	self->sample_rate = sample_rate;
	self->n_channels  = n_channels;
	self->norm_input  = 1.f / n_channels;

	self->ratio = 0.f;
	self->p_rat = 0.f;

	self->igain = 1.f;
	self->p_ign = 1.f;
	self->l_ign = 0.f;

	self->p_thr = 0.05f;
	self->l_thr = -10.f;

	self->hold = false;

	self->t_att = 0.f;
	self->t_rel = 0.f;

	self->w_rms = 5.f / sample_rate;
	self->w_lpf = 160.f / sample_rate;

	Dyncomp_set_attack (self, 0.01f);
	Dyncomp_set_release (self, 0.03f);
	Dyncomp_reset (self);
}

void Dyncomp_process (Dyncomp* self, uint32_t n_samples, float *samples)
{
	float gmin, gmax;

	/* reset min/max gain report */
	if (self->newg) {
		gmax       = -100.0f;
		gmin       = 100.0f;
		self->newg = false;
	} else {
		gmax = self->gmax;
		gmin = self->gmin;
	}

	/* interpolate input gain */
	float       g  = self->igain;
	const float g1 = self->p_ign;
	float       dg = g1 - g;
	if (fabsf (dg) < 1e-5f || (g > 1.f && fabsf (dg) < 1e-3f)) {
		g  = g1;
		dg = 0;
	}

	/* interpolate ratio */
	float       r  = self->ratio;
	const float r1 = self->p_rat;
	float       dr = r1 - r;
	if (fabsf (dr) < 1e-5f) {
		r  = r1;
		dr = 0;
	}

	/* localize variables */
	float za1 = self->za1;
	float zr1 = self->zr1;
	float zr2 = self->zr2;

	float rms = self->rms;

	const float w_rms = self->w_rms;
	const float w_lpf = self->w_lpf;
	const float w_att = self->w_att;
	const float w_rel = self->w_rel;
	const float p_thr = self->p_thr;

	const float p_hold = self->hold ? 2.f * p_thr : 0.f;

	const uint32_t nc  = self->n_channels;
	const float    n_1 = self->norm_input;

	for (uint32_t j = 0; j < n_samples; ++j) {
		/* update input gain */
		if (dg != 0) {
			g += w_lpf * (g1 - g);
		}

		/* Input/Key RMS */
		float v = 0;
		for (uint32_t i = 0; i < nc; ++i) {
			const float x = g * samples[j * nc + i];
			v += x * x;
		}

		v *= n_1; // normalize *= 1 / (number of channels)

		/* slow moving RMS, used for GUI level meter display */
		rms += w_rms * (v - rms); // TODO: consider reporting range; 5ms integrate, 50ms min/max readout

		/* calculate signal power relative to threshold, LPF using attack time constant */
		za1 += w_att * (p_thr + v - za1);

		/* hold release */
		const bool hold = 0 != isless (za1, p_hold);

		/* Note: za1 >= p_thr; so zr1, zr2 can't become denormal */
		if (isless (zr1, za1)) {
			zr1 = za1;
		} else if (!hold) {
			zr1 -= w_rel * zr1;
		}

		if (isless (zr2, za1)) {
			zr2 = za1;
		} else if (!hold) {
			zr2 += w_rel * (zr1 - zr2);
		}

		/* update ratio */
		if (dr != 0) {
			r += w_lpf * (r1 - r);
		}

		/* Note: expf (a * logf (b)) == powf (b, a);
		 * however powf() is significantly slower
		 *
		 * Effective gain is  (zr2) ^ (-ratio).
		 *
		 * with 0 <= ratio <= 0.5 and
		 * zr2 being low-pass (attack/release) filtered square of the key-signal.
		 */

		float pg = -r * logf (20.0f * zr2);

		/* store min/max gain in dB, report to UI */
		gmax = fmaxf (gmax, pg);
		gmin = fminf (gmin, pg);

		pg = g * expf (pg);

		/* apply gain factor to all channels */
		for (uint32_t i = 0; i < nc; ++i) {
			samples[j * nc + i] = pg * samples[j * nc + i];
		}
	}

	/* copy back variables */
	self->igain = g;
	self->ratio = r;

	if (!isfinite (za1)) {
		self->za1  = 0.f;
		self->zr1  = 0.f;
		self->zr2  = 0.f;
		self->newg = true; /* reset gmin/gmax next cycle */
	} else {
		self->za1  = za1;
		self->zr1  = zr1;
		self->zr2  = zr2;
		self->gmax = gmax;
		self->gmin = gmin;
	}

	if (!isfinite (rms)) {
		self->rms = 0.f;
	} else if (rms > 10) {
		self->rms = 10; // 20dBFS
	} else {
		self->rms = rms + 1e-12; // + denormal protection
	}
}