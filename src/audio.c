#include "audio.h"

const char **AudioClient_get_supported_apis(){
    unsigned int num_apis = rtaudio_get_num_compiled_apis();
    const char **api_names = g_new0(const char*, num_apis + 1);
    const rtaudio_api_t *supported_apis = rtaudio_compiled_api();
    for(unsigned int i = 0; i < num_apis; i++){
        api_names[i] = rtaudio_api_name(supported_apis[i]);
    }
    return api_names;
}

AudioClient *AudioClient_create(const char *api_name, AudioClient_data_cb audio_cb, gpointer cb_data){
    AudioClient *audio_client = g_new0(AudioClient, 1);
    rtaudio_api_t chosen_api = rtaudio_compiled_api_by_name(api_name);
    if(chosen_api == RTAUDIO_API_UNSPECIFIED){
        g_free(audio_client);
        return NULL;
    }

    audio_client->rtaudio = rtaudio_create(chosen_api);
    audio_client->audio_cb = audio_cb;
    audio_client->cb_data = cb_data;

    audio_client->api = g_strdup(api_name);

    return audio_client;
}

static inline gboolean AudioClient_device_supports_48k(rtaudio_device_info_t *device_info){
    int sample_rate;
    for(int i = 0; i < NUM_SAMPLE_RATES; i++){
        sample_rate = device_info->sample_rates[i];
        if(sample_rate == 48000) return TRUE;
    }
    return FALSE;
}

#ifdef RTAUDIO_BELOW_6_0
static inline int AudioClient_get_device_id(rtaudio_t *audio, const char *device_name){
    int num_devices = rtaudio_device_count(*audio);

    rtaudio_device_info_t device_info;
    for(int i = 0; i < num_devices; i++){
        device_info = rtaudio_get_device_info(*audio, i);
        if(!AudioClient_device_supports_48k(&device_info)) continue;
        if(g_strcmp0(device_name, device_info.name) == 0) return i;
    }
    
    return -1;
}

#else
static inline unsigned int AudioClient_get_device_id(rtaudio_t *audio, const char *device_name){
    int num_devices = rtaudio_device_count(*audio);

    rtaudio_device_info_t device_info;
    for(int i = 0; i < num_devices; i++){
        unsigned int id = rtaudio_get_device_id(*audio, i);
        device_info = rtaudio_get_device_info(*audio, id);
        if(!AudioClient_device_supports_48k(&device_info)) continue;
        if(g_strcmp0(device_name, device_info.name) == 0) return id;
    }
    
    return 0;
}
#endif

AudioDevices *AudioClient_get_devices_for_api(const char *api_name){
    rtaudio_api_t chosen_api = rtaudio_compiled_api_by_name(api_name);
    if(chosen_api == RTAUDIO_API_UNSPECIFIED) return NULL;
    rtaudio_t rtaudio = rtaudio_create(chosen_api);

    AudioDevices *devices = g_new0(AudioDevices, 1);
    int num_devices = rtaudio_device_count(rtaudio);
    devices->outputs = g_new0(const char*, num_devices);
    devices->inputs = g_new0(const char*, num_devices);

    rtaudio_device_info_t device_info;
    for(int i = 0; i < num_devices; i++){
#ifdef RTAUDIO_BELOW_6_0
        device_info = rtaudio_get_device_info(rtaudio, i);
#else
        unsigned int id = rtaudio_get_device_id(rtaudio, i);
        device_info = rtaudio_get_device_info(rtaudio, id);
#endif
        if(!AudioClient_device_supports_48k(&device_info)) continue;
        if(device_info.output_channels >= 2){
            devices->outputs[devices->n_outputs] = g_strdup(device_info.name);
            devices->n_outputs++;
        }
        if(device_info.input_channels >= 2){
            devices->inputs[devices->n_inputs] = g_strdup(device_info.name);
            devices->n_inputs++;
        }
    }

    rtaudio_destroy(rtaudio);

    return devices;
}

void AudioDevices_clear(AudioDevices *devices){
    for(int i = 0; i < devices->n_outputs; i++)
        g_free((gpointer) devices->outputs[i]);
    for(int i = 0; i < devices->n_inputs; i++)
        g_free((gpointer) devices->inputs[i]);
    g_free(devices->outputs);
    g_free(devices->inputs);
    g_free(devices);
}

static int AudioClient_rtaudio_cb(void *out, void *in, unsigned int n_frames, double stream_time, rtaudio_stream_status_t status, void *userdata){
    AudioClient *audio_client = userdata;
    float *output = out;
    float *input = in;

    char stereo = audio_client->stereo;
    switch(stereo){
        case 'M':
            for(unsigned int i = 0; i < n_frames; i++){
                audio_client->input_buffer[i * 2] = audio_client->input_buffer[i * 2 + 1] = (input[i * 2] + input[i * 2 + 1]) / 2.0;
            }
            break;
        case 'L':
            for(unsigned int i = 0; i < n_frames; i++){
                audio_client->input_buffer[i * 2] = audio_client->input_buffer[i * 2 + 1] = input[i * 2];
            }
            break;
        case 'R':
            for(unsigned int i = 0; i < n_frames; i++){
                audio_client->input_buffer[i * 2] = audio_client->input_buffer[i * 2 + 1] = input[i * 2 + 1];
            }
            break;
        default:
            memcpy(audio_client->input_buffer, input, sizeof(float) * 2 * n_frames);
            break;
    }

    audio_client->audio_cb(output, audio_client->input_buffer, audio_client->processing_buffer, n_frames, audio_client->cb_data);

    return 0;
}

static void AudioClient_rtaudio_err(rtaudio_error_t err, const char *msg){
    g_print("RtAudio Error: %s\n", msg);
}

gboolean AudioClient_connect(AudioClient *audio_client, const char *output_device, const char *input_device, unsigned int buffer_frames){
    if(audio_client->started){
        rtaudio_stop_stream(audio_client->rtaudio);
        rtaudio_close_stream(audio_client->rtaudio);
        audio_client->started = FALSE;
    }

#ifdef RTAUDIO_BELOW_6_0
    int output_device_id = AudioClient_get_device_id(&audio_client->rtaudio, output_device);
    if(output_device_id < 0) return FALSE;
    int input_device_id = AudioClient_get_device_id(&audio_client->rtaudio, input_device);
    if(input_device_id < 0) return FALSE;
#else
    unsigned int output_device_id = AudioClient_get_device_id(&audio_client->rtaudio, output_device);
    if(output_device_id == 0) return FALSE;
    unsigned int input_device_id = AudioClient_get_device_id(&audio_client->rtaudio, input_device);
    if(input_device_id == 0) return FALSE;
#endif

    rtaudio_stream_parameters_t output_stream_params = {output_device_id, 2, 0};
    rtaudio_stream_parameters_t input_stream_params = {input_device_id, 2, 0};
    rtaudio_stream_options_t stream_opts = {0, 0, 0, "LibreDiscord"};

    audio_client->buffer_frames = buffer_frames;

    int open_error = rtaudio_open_stream(audio_client->rtaudio, &output_stream_params, &input_stream_params, RTAUDIO_FORMAT_FLOAT32, 48000, &audio_client->buffer_frames, AudioClient_rtaudio_cb, audio_client, &stream_opts, AudioClient_rtaudio_err);

#ifdef RTAUDIO_BELOW_6_0
    if(open_error >= RTAUDIO_ERROR_UNSPECIFIED){
#else
    if(open_error >= RTAUDIO_ERROR_UNKNOWN){
#endif
        rtaudio_close_stream(audio_client->rtaudio);
        return FALSE;
    }

    if(audio_client->input_buffer) g_free(audio_client->input_buffer);
    if(audio_client->processing_buffer) g_free(audio_client->processing_buffer);
    audio_client->input_buffer = g_new0(float, audio_client->buffer_frames * 2);
    audio_client->processing_buffer = g_new0(float, audio_client->buffer_frames * 2);

    rtaudio_start_stream(audio_client->rtaudio);
    audio_client->started = TRUE;

    g_free((gpointer) audio_client->output);
    audio_client->output = g_strdup(output_device);
    g_free((gpointer) audio_client->input);
    audio_client->input = g_strdup(input_device);

    return TRUE;
}

void AudioClient_destroy(AudioClient *audio_client){
    if(audio_client->started){
        rtaudio_stop_stream(audio_client->rtaudio);
        rtaudio_close_stream(audio_client->rtaudio);
    }
    rtaudio_destroy(audio_client->rtaudio);
    g_free((gpointer) audio_client->api);
    g_free((gpointer) audio_client->output);
    g_free((gpointer) audio_client->input);
    g_free(audio_client->input_buffer);
    g_free(audio_client->processing_buffer);
    g_free(audio_client);
}
