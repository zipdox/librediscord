#include <glib.h>
#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

#include "constants.h"

#define VOICE_HEARTBEAT_TEMPLATE "{ \"op\": 3, \"d\": 0 }"
#define VOICE_IDENTITY_TEMPLATE "{ \"op\": 0, \"d\": {} }"
#define SELECT_PROTOCOL_TEMPLATE "{ \"op\": 1, \"d\": { \"protocol\": \"udp\", \"data\": {\"mode\": \"xsalsa20_poly1305\" } } }"

typedef void (*VoiceGateway_message_cb)(gint64 opcode, JsonObject *data_obj, gpointer cb_data);

typedef void (*VoiceGateway_closed_cb)(gpointer cb_data);

typedef struct {
    const char *server_id;
    const char *user_id;
    const char *session_id;
    const char *token;
    gpointer cb_data;
    SoupSession *session;
    SoupMessage *request;
    SoupWebsocketConnection *connection;
    VoiceGateway_message_cb message_cb;
    VoiceGateway_closed_cb closed_cb;
    GPtrArray *voice_receivers;
    guint64 heartbeat_interval;
    GSource *heartbeat_source;
    JsonNode *heartbeat_node;
} VoiceGateway;

VoiceGateway *VoiceGateway_create(
    const char *endpoint, const char *server_id, const char *user_id, const char *session_id, const char *token,
    VoiceGateway_message_cb message_cb, VoiceGateway_closed_cb closed_cb, gpointer cb_data
);

void VoiceGateway_send(VoiceGateway *voice_gateway, JsonNode *transmission);

void VoiceGateway_close(VoiceGateway *voice_gateway);

void VoiceGateway_destroy(VoiceGateway *voice_gateway);
