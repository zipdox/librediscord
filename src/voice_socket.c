#include <gio/gio.h>

#include "voice_socket.h"

static gboolean VoiceSocket_received_cb(GSocket *socket, GIOCondition condition, gpointer user_data){
    VoiceSocket *voice_socket = user_data;
    GError *error = NULL;
    gssize received = g_socket_receive_from(voice_socket->socket, &voice_socket->address, (char*) voice_socket->buffer, 65536, NULL, &error);
    if(received <= 0){
        g_print("Error receiving UDP data: %s\n", error->message);
        g_error_free(error);
        return G_SOURCE_CONTINUE;
    }
    voice_socket->data_cb(voice_socket->buffer, received, voice_socket->cb_data);
    return G_SOURCE_CONTINUE;
}

VoiceSocket* VoiceSocket_create(const char *address, guint port, VoiceSocket_data_cb data_cb, gpointer cb_data){
    VoiceSocket *voice_socket = g_new0(VoiceSocket, 1);
    voice_socket->data_cb = data_cb;
    voice_socket->address = g_inet_socket_address_new_from_string(address, port);
    voice_socket->family = g_socket_address_get_family(G_SOCKET_ADDRESS(voice_socket->address));
    voice_socket->cb_data = cb_data;

    GError *error = NULL;
    voice_socket->socket = g_socket_new(voice_socket->family, G_SOCKET_TYPE_DATAGRAM, G_SOCKET_PROTOCOL_UDP, &error);
    if(voice_socket == NULL){
        g_print("Error creating UDP socket: %s\n", error->message);
        g_error_free(error);
        g_object_unref(voice_socket->address);
        g_free(voice_socket);
        return NULL;
    }

    g_socket_set_blocking(voice_socket->socket, FALSE);
    voice_socket->source = g_socket_create_source(voice_socket->socket, G_IO_IN, NULL);
    g_source_attach(voice_socket->source, NULL);
    g_source_set_callback(voice_socket->source, G_SOURCE_FUNC(VoiceSocket_received_cb), (gpointer) voice_socket, NULL);

    return voice_socket;
}

void VoiceSocket_send(VoiceSocket* voice_socket, unsigned char *data, gsize size){
    g_socket_send_to(voice_socket->socket, voice_socket->address, (const char*) data, size, NULL, NULL);
}


void VoiceSocket_destroy(VoiceSocket* voice_socket){
    g_source_destroy(voice_socket->source);
    g_socket_close(voice_socket->socket, NULL);
    g_object_unref(voice_socket->socket);
    g_source_unref(voice_socket->source);
    g_object_unref(voice_socket->address);
    g_free(voice_socket);
}
