#include <gio/gio.h>
#include <gtk/gtk.h>

#include "client.h"
#include "state_manager.h"

typedef enum {
    ICON_CAMERA,
    ICON_CAMERA_OFF,
    ICON_SCREEN,
    ICON_HEADPHONES,
    ICON_HEADPHONES_MUTED,
    ICON_MICROPHONE,
    ICON_MICROPHONE_MUTED,
    ICON_HANG_UP,
    ICON_SETTINGS,
    ICON_INFO,
    ICON_TOTAL
} IconName;

typedef struct {
    GtkComboBox *audio_api;
    GtkWidget *audio_input_device;
    GtkWidget *audio_output_device;
    GtkComboBox *stereo_mode;
    GtkWidget *monitor_enable;
    GtkComboBox *buffer_size;

    GtkWidget *noisegate_enable;
    GtkAdjustment *gate_threshold_adjustment;
    GtkAdjustment *gate_hold_adjustment;
    GtkAdjustment *gate_decay_adjustment;

    GtkWidget *compressor_enable;
    GtkAdjustment *compressor_gain_adjustment;
    GtkAdjustment *compressor_threshold_adjustment;
    GtkAdjustment *compressor_ratio_adjustment;
    GtkAdjustment *compressor_attack_adjustment;
    GtkAdjustment *compressor_release_adjustment;

    GtkComboBox *bitrate;
    GtkComboBox *frame_size;
} GUISettings;

typedef struct {
    GdkPixbuf *icon_bufs[ICON_TOTAL];
    GSettings *settings;
    ImgCache *cache;
    GPtrArray *accounts;
    gboolean loading_accounts;
    guint selected_account;
    const char *token;
    const char *switch_token;
    gboolean add_account; // add the account to the secret service after logging in

    DiscordClient *client;

    GtkBuilder *builder;
    GtkWidget *window;

    GtkPaned *master_pane;
    GtkWidget *default_channel_box;

    GtkWidget *channels_viewport;
    GtkListBox *directs_list;
    GtkListBox *guilds_list;

    GtkWidget *accounts_dialog;
    GtkListBox *accounts_list;
    GtkWidget *account_find_dialog;
    GtkEntry *token_entry;
    GtkWidget *login_dialog;
    GtkEntry *email_entry;
    GtkEntry *password_entry;
    GtkWidget *logout_dialog;
    GtkWidget *mfa_dialog;
    GtkEntry *mfa_entry;
    const char *logout_token;
    const char *mfa_ticket;

    GtkWidget *settings_dialog;

    GtkWidget *about_dialog;

    GtkImage *deafen_button_icon;
    GtkImage *mute_button_icon;
    GtkImage *camera_button_icon;

    GtkWidget *voice_status_box;
    GtkLabel *voice_status_label;
    GtkLabel *voice_channel_label;

    GtkImage *profile_picture;
    GtkLabel *username_label;

    GUISettings *settings_widgets;

    StateManager *state_manager;

    const char *initial_user_id;
    gboolean close;

    gboolean muted;
    gboolean deafened;
    gboolean video;
} GUI;

GUI *GUI_create(GSettings *settings, int *argc, char ***argv, const char *cache_dir);

void GUI_run(GUI *gui);
