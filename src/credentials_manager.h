#include <glib.h>

typedef struct {
    const char *username;
    const char *discriminator;
    const char *user_id;
    const char *avatar;
    const char *token;
} DiscordCredentials;

typedef void (*DiscordCredentials_list_cb)(GPtrArray *accounts, gpointer cb_data);

typedef void (*DiscordCredentials_add_cb)(gboolean success, gpointer cb_data);

void DiscordCredentials_list(DiscordCredentials_list_cb callback, gpointer cb_data);

void DiscordCredentials_list_clear(GPtrArray *accounts);

// void DiscordCredentials_add(char *username, char *user_id, char *avatar, char *token, DiscordCredentials_add_cb callback, gpointer cb_data);
void DiscordCredentials_add(const char *username, const char *discriminator, const char *user_id, const char *avatar, const char *token);

void DiscordCredentials_remove(GPtrArray *accounts, guint index);
