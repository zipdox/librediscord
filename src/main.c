/*
 * LibreDiscord, a voice and video client for Discord.
 * Copyright (C) 2022  Zipdox
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#define G_SETTINGS_ENABLE_BACKEND
#include <gio/gsettingsbackend.h>

#include "schemaresource.h"
#include "gui.h"

#define CONFIG_FILENAME "librediscord.conf"

const char *get_settings_path(){
    const char *config_dir = g_get_user_config_dir();
    const char *settings_path = g_build_filename(config_dir, CONFIG_FILENAME, NULL);
    gint made_settings_dir = g_mkdir_with_parents(config_dir, 700);
    if(made_settings_dir != 0) return NULL;
    return settings_path;
}

const char *cache_dir;
int create_cache_dir(){
    const char *user_cache_dir = g_get_user_cache_dir();
    cache_dir = g_build_filename(user_cache_dir, "librediscord", NULL);
    return g_mkdir_with_parents(cache_dir, 0700);
}

int main(int argc, char **argv){
    const char *settings_path = get_settings_path();
    if(!settings_path){
        g_error("Failed to create settings directory");
        return 1;
    }
    if(create_cache_dir() != 0){
        g_error("Failed to create cache directory");
        return 1;
    }

    GSettingsSchemaSource *schema_source = g_settings_schema_source_from_resource("/gschemas.compiled", NULL, TRUE, NULL);
    GSettingsSchema *schema = g_settings_schema_source_lookup(schema_source, "net.librediscord", FALSE);
    GSettingsBackend *backend = g_keyfile_settings_backend_new(settings_path, "/", "librediscord");
    g_free((gpointer) settings_path);
    GSettings *settings = g_settings_new_full(schema, backend, NULL);

    GUI *gui = GUI_create(settings, &argc, &argv, cache_dir);
    GUI_run(gui);

    return 0;
}
