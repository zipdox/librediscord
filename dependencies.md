## Build tools
- gcc
- make
- pkg-config

## Build Dependencies
Debian / Ubuntu | Fedora | Arch | MacPorts | MSYS2
---|---|---|---|---
libgtk-3-dev | gtk3-devel | gtk3 | gtk3 -x11 +quartz | gtk3:u
libsoup2.4-dev | libsoup-devel | libsoup | libsoup | libsoup:u
libjson-glib-dev | json-glib-devel | json-glib | json-glib | json-glib:u
libleveldb-dev | leveldb-devel | leveldb | leveldb | leveldb:u
librtaudio-dev | rtaudio-devel | rtaudio | rtaudio | rtaudio:u
libopus-dev | opus-devel | opus | libopus | opus:u
libsodium-dev | libsodium-devel | libsodium | libsodium | libsodium:u
libsecret-1-dev | libsecret-devel | libsecret | libsecret | libsecret:u
