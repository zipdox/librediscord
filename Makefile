CC=gcc
FLAGS=`pkg-config --cflags gtk+-3.0 libsoup-2.4 rtaudio json-glib-1.0 opus libsodium libsecret-1`
LIBS=`pkg-config --libs gtk+-3.0 libsoup-2.4 rtaudio json-glib-1.0 opus libsodium libsecret-1` -lleveldb -lm
PREFIX=/usr
BUILD_DIR=build
SRCS = $(shell find ./src/*.c)
OBJS = $(patsubst %.c, $(BUILD_DIR)/%.o, $(SRCS))
OPTS=-O3
ifdef CACHE
OPTS+= -D USE_CACHE
endif

RTAUDIO_BELOW_6_0 := $(shell pkg-config --atleast-version=6.0.0 rtaudio && echo no || echo yes)
ifeq ($(RTAUDIO_BELOW_6_0), yes)
	OPTS+= -D RTAUDIO_BELOW_6_0
endif

all: $(BUILD_DIR)/librediscord

$(BUILD_DIR)/assets/: assets/
	mkdir -p $(dir $@)
	cp -r assets/* $(dir $@)
$(BUILD_DIR)/assets/gschemas.compiled: $(BUILD_DIR)/assets/
	glib-compile-schemas $(dir $@)
$(BUILD_DIR)/resources.c: resources.xml $(BUILD_DIR)/assets/gschemas.compiled
	glib-compile-resources --sourcedir=$(BUILD_DIR)/assets/ $< --target=$@ --generate-source
$(BUILD_DIR)/resources.o: $(BUILD_DIR)/resources.c
	$(CC) -c -o $@ $^ $(FLAGS) $(OPTS)

$(BUILD_DIR)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) -c -o $@ $(FLAGS) $< -Wall $(OPTS)

$(BUILD_DIR)/librediscord: $(OBJS) $(BUILD_DIR)/resources.o
	$(CC) -o $@ $^ $(LIBS) $(OPTS)

clean:
	rm -rf build

uninstall:
	rm -f $(PREFIX)/share/applications/librediscord.desktop
	rm -f $(PREFIX)/share/pixmaps/librediscord.png
	rm -f $(PREFIX)/share/pixmaps/librediscord.svg
	rm -f $(PREFIX)/bin/librediscord

install: uninstall
	cp librediscord.desktop $(PREFIX)/share/applications/librediscord.desktop
	cp assets/icon.svg $(PREFIX)/share/pixmaps/librediscord.svg
	cp $(BUILD_DIR)/librediscord $(PREFIX)/bin/librediscord

run:
	./build/librediscord
