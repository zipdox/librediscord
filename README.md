<img src="assets/icon.svg" width="200" height="200">

# LibreDiscord

LibreDiscord is a free and open source voice client for Discord written in C using GTK3 and GLib.

![screenshot](screenshot.png)

[![invite](https://discordapp.com/api/guilds/1219326752371904522/embed.png?style=banner2)](https://discord.gg/ajuseV7sfz)

# WIP
This project is currently a work in progress. Right now it only supports audio tranmission and reception. Video isn't implemented yet.

# FAQ

### Will LibreDiscord support text chat?
It is not my intention to support it. Discord's text chat is a lot of work to implement and outside the intended scope of this project.

### Will there be a native Windows build?
Not unless someone else figures out how to do it. I'm all ears.

### Will there be video support?
That is planned. Right now I'm more focussed on [discord-jack-voiceengine](https://gitlab.com/zipdox/discord-jack-voiceengine/), which has it.

# Features
- Lightweight
- Wide audio API support: ALSA, PulseAudio, OSS, JACK, CoreAudio, WASAPI, ASIO, DirectSound
- Input channel configuration (stereo, dual mono, left only, right only)
- Low-latency monitoring
- Configurable buffer size
- Dynamic range compressor
- Noise gate
- Stereo transmission
- Deafen without muting
- Configurable codec frame size
- Configurable bitrate (bypasses channel bitrate setting)

# System requirements
- Any computer that's not a shitbox
- Operating system
  - GNU/Linux (any distro that can install the dependencies)
  - Windows with MSYS2
  - MacOS with MacPorts
- Audio device with stereo input and output
- Internet connection (preferably faster than dial-up)

# Logging in
You can open the account manager by clicking on the avatar image in the bottom left. There are two methods of logging in. You can either log in using email and password (and 2FA), or you can add a token manually. LibreDiscord can also find your token if you're logged into the official Discord desktop app.

# Configuration
LibreDiscord is intented to provide high quality audio, therefore it requires some configuration. After opening the settings you will need to select what audio backend you want to use with LibreDiscord, the options will depend on your operating system. Then you'll need to select what input and output devices to use. Note that LibreDiscord will only show you devices that support 48kHz sample rate and have at least two input and output channels. If your device doesn't show up, you might have to change your sample rate to 48kHz in your system settings (or JACK / ASIO configuration). You'll also need to select an audio buffer size. Please note that if you're using ASIO or JACK, you need to leave the buffer size to auto as ASIO and JACK govern their own buffer size.

You'll also have to enable and configure the noise gate if you don't want to be speaking constantly.

Also select a good bit rate if you don't want to sound like a JPEG.

# Building
LibreDiscord is built using Make.

> Note: If you want support for caching images, use `make -j CACHE=1`. This is disabled by default due to a bug in libsoup that causes a crash if you have a lot of private channels or guilds due to the amount of simultaneous cache saves.

### GNU/Linux
Install the [dependencies](dependencies.md) for your distro and run `make -j`.

You can install and uninstall LibreDiscord system-wide using `sudo make install` and `sudo make uninstall`, or run it directly with `make run`.

### Windows
On Windows, currently the only working method to build LibreDiscord is using MSYS2. Use the UCRT64 shell. Install the [dependencies](dependencies.md) for MSYS2 using [pacboy](https://www.msys2.org/docs/package-naming/#avoiding-writing-long-package-names). Build it with `make -j`. You can run it with `make run`.

Installing is not supported on Windows as the MSYS2 UCRT64 environment is necessary to run LibreDiscord. 

### MacOS
Install the [dependencies](dependencies.md) using MacPorts. Additionally, make sure you install `pkgconfig`, `gmake` and `gcc13` (or later).

Then you should be able to build LibreDiscord with `gmake -j`. You can run it with `gmake run`.

Installing is not supported on MacOS.

# License
LibreDiscord is licensed under the GNU General Public License version 3.

[LICENSE](LICENSE.md)

# License notices
Obligatory legal notices for used code.

- [x42 compressor](https://github.com/x42/darc.lv2) Copyright (C) 2018,2019 Robin Gareus
- [Numix icon theme](https://github.com/numixproject/numix-icon-theme)
- [Paper Icon Theme](https://github.com/snwh/paper-icon-theme/)
- [Papirus Icon Theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
